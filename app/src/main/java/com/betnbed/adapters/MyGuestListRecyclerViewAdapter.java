package com.betnbed.adapters;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.betnbed.Heap;
import com.betnbed.R;
import com.betnbed.RequestConstructor;
import com.betnbed.activity.MyHostActivity;
import com.betnbed.controller.SettingsController;
import com.betnbed.interfaces.MovingToTab;
import com.betnbed.interfaces.SocketInterface;
import com.betnbed.model.Request;
import com.betnbed.service.NetworkService;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MyGuestListRecyclerViewAdapter extends MyHostRecycleViewAdapter<MyGuestListRecyclerViewAdapter.ViewHolder> {

    MovingToTab movingToTab;

    public MyGuestListRecyclerViewAdapter(MovingToTab movingToTab) {
        this.movingToTab = movingToTab;

    }

    @Override
    public MyGuestListRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        super.onCreateViewHolder(parent,viewType);

        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.list_view_myguest, parent, false));
    }

    @Override
    public void onBindViewHolder(final MyGuestListRecyclerViewAdapter.ViewHolder h, int position) {
        super.onBindViewHolder(h, position);

        final Request q = contents.get(h.getAdapterPosition());

        ///////////////////////////////////////////////////////////////////////////////////////////////

        h.accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAcceptRequestDialog(h.getAdapterPosition());
            }
        });



        //h.reject.setVisibility(View.VISIBLE);

        if (q.getSenderId() == SettingsController.getAccoutUserId()) {
            h.accept.setVisibility(View.GONE);
            h.reject.setText("Скрыть");
            h.reject.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    hideMyRequest(h.getAdapterPosition());
                }
            });
            //h.reject.setVisibility(View.GONE);
        } else {
            h.accept.setVisibility(View.VISIBLE);
            h.reject.setText("Отклонить");
            h.reject.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showRejectRequestDialog(h.getAdapterPosition());
                }
            });
            //h.reject.setVisibility(View.VISIBLE);
        }
    }

    static class ViewHolder extends MyHostRecycleViewAdapter.ViewHolder {

        @BindView(R.id.more) Button accept;
        @BindView(R.id.reject) Button reject;

        private ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    private void showAcceptRequestDialog(final int position) {
        View myView = LayoutInflater.from(context).inflate(R.layout.dialog_custom_invite, null);

        final EditText et = (EditText) myView.findViewById(R.id.editText);
        et.setText(SettingsController.getAdditionalInfoWhenInviteTourist());
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setView(myView)
                .setCancelable(false)
                .setIcon(R.drawable.png_agreement_green)
                .setTitle("Описание приглашения")
                .setPositiveButton("Пригласить", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (Heap.accountHotel.requestHostController.filtered.get(position).isSendId()) {
                            NetworkService.requestController.sendRequest(RequestConstructor.HInvite(Heap.accountHotel.requestHostController.filtered.get(position).getSenderId(), Heap.accountHotel.requestHostController.filtered.get(position).getRequestId(), et.getText().toString()), "GInviteReceived", new SocketInterface() {
                                @Override
                                public void onSuccess(String request) {
                                    try {

                                        Heap.accountHotel.requestHostController.filtered.get(position).setSendId(true);
                                        Heap.accountHotel.requestHostController.filtered.get(position).setInviteId(new JSONObject(request).getJSONArray("data").getInt(2));
                                        Heap.accountHotel.requestHostController.withInvite.add(0, Heap.accountHotel.requestHostController.filtered.get(position));

                                        Heap.accountHotel.requestHostController.deleteRequestFromWithOutInviteAndFiltered(Heap.accountHotel.requestHostController.filtered.get(position).getSenderId(), Heap.accountHotel.requestHostController.filtered.get(position).getRequestId());

                                        notifyItemRangeChanged(0, getItemCount());
                                        notifyDataSetChanged();

                                        movingToTab.moveTo(1);
                                        //LocalBroadcastManager.getInstance(context).sendBroadcast(new Intent(MyHostFragment.tagOpenPage).putExtra("OPEN_PAGE", 1));
                                        LocalBroadcastManager.getInstance(context).sendBroadcast(new Intent(MyHostActivity.tagRefreshFragment).putExtra("FRAGMENT",1));
                                        Toast.makeText(context, "Приглашение отправленно", Toast.LENGTH_SHORT).show();

                                    } catch (JSONException e) {
                                        Log.v("TAG", "Some error");
                                    }
                                }

                                @Override
                                public void onError(int error) {
                                    switch (error) {
                                        case 24:
                                            Toast.makeText(context, "На данный запрос уже отправленно приглашение", Toast.LENGTH_SHORT).show();
                                            break;
                                        case 67:
                                            Toast.makeText(context, context.getString(R.string.error67), Toast.LENGTH_SHORT).show();
                                            break;
                                    }
                                }
                            });
                        }
                    }
                })
                .setNeutralButton("Отменить", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        AlertDialog ad = builder.create();
        ad.show();
    }

    private void showRejectRequestDialog(final int position) {

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setView(null)
                .setCancelable(true)
                .setTitle("Отклонить заявку?")
                .setPositiveButton("Отклонить", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (Heap.accountHotel.requestHostController.filtered.get(position).isSendId()) {
                            NetworkService.requestController.sendRequest(RequestConstructor.HIgnoreRequest(Heap.accountHotel.requestHostController.filtered.get(position).getSenderId(), Heap.accountHotel.requestHostController.filtered.get(position).getRequestId()), "GIgnoreRequest", new SocketInterface() {
                                @Override
                                public void onSuccess(String request) {

                                    Heap.accountHotel.requestHostController.deleteRequestFromWithOutInviteAndFiltered(Heap.accountHotel.requestHostController.filtered.get(position).getSenderId(), Heap.accountHotel.requestHostController.filtered.get(position).getRequestId());

                                    notifyItemRangeChanged(0, getItemCount());
                                    notifyDataSetChanged();
                                    Toast.makeText(context, "Запрос удален", Toast.LENGTH_SHORT).show();
                                }

                                @Override
                                public void onError(int error) {
                                    switch (error) {
                                        case 67:
                                            Toast.makeText(context, context.getString(R.string.error67), Toast.LENGTH_SHORT).show();
                                            break;
                                    }
                                }
                            });
                        }
                    }
                })
                .setNeutralButton("Нет", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        AlertDialog ad = builder.create();
        ad.show();
    }

    private void hideMyRequest(final int position) {

        NetworkService.requestController.sendRequest(RequestConstructor.HIgnoreRequest(Heap.accountHotel.requestHostController.filtered.get(position).getSenderId(), Heap.accountHotel.requestHostController.filtered.get(position).getRequestId()), "GIgnoreRequest", new SocketInterface() {
            @Override
            public void onSuccess(String request) {

                Heap.accountHotel.requestHostController.deleteRequestFromWithOutInviteAndFiltered(Heap.accountHotel.requestHostController.filtered.get(position).getSenderId(), Heap.accountHotel.requestHostController.filtered.get(position).getRequestId());

                notifyItemRangeChanged(0, getItemCount());
                notifyDataSetChanged();
            }

            @Override
            public void onError(int error) {
            }
        });
    }

}

