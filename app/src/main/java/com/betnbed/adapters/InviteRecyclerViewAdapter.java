package com.betnbed.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.graphics.drawable.VectorDrawableCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.betnbed.Heap;
import com.betnbed.R;
import com.betnbed.RequestConstructor;
import com.betnbed.activity.profile.ProfileHotelActivity;
import com.betnbed.controller.RequestController;
import com.betnbed.interfaces.RefreshHeader;
import com.betnbed.interfaces.SocketInterface;
import com.betnbed.model.Hotel;
import com.betnbed.model.Invite;
import com.betnbed.service.NetworkService;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class InviteRecyclerViewAdapter extends RecyclerView.Adapter<InviteRecyclerViewAdapter.ViewHolder> {

    private ArrayList<Invite> contents;
    private Context context;
    private RefreshHeader refreshHeader;
    private int requestId;

    public InviteRecyclerViewAdapter(int requestId, RefreshHeader refreshHeader) {
        this.requestId = requestId;
        contents = Heap.accountUser.getRequestTouristController().getRequests().get(this.requestId).getInvites();
        this.refreshHeader = refreshHeader;
    }


    @Override
    public int getItemCount() {
        return contents.size();
    }

    @Override
    public InviteRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();

        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.list_view_user_invitation, parent, false));
    }


    @Override
    public void onBindViewHolder(final InviteRecyclerViewAdapter.ViewHolder holder, final int position) {

        final int currentPosition = holder.getAdapterPosition();

        holder.title.setText(contents.get(currentPosition).getTitle());
        holder.city.setText(contents.get(currentPosition).getAddress());


        ////////////////////////////////////////////////////////////////////////////////////////////
        if (contents.get(currentPosition).getDeleteReason() != null) {

            holder.progressBar.getIndeterminateDrawable().setColorFilter(Color.parseColor("#00a0f0"), PorterDuff.Mode.MULTIPLY);

            holder.agree.setBackgroundColor(Color.parseColor("#9e9b9c"));
            holder.agree.setTextColor(Color.WHITE);
            holder.agree.setText("Убрать");
            holder.more.setVisibility(View.GONE);
            holder.helpInfo.setVisibility(View.VISIBLE);
            holder.helpInfo.setText("Отель отозвал приглашение " + (contents.get(currentPosition).getDeleteReason().equals("") ? "" : ("по причине:\n" + contents.get(currentPosition).getDeleteReason())));
            holder.agree.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    contents.remove(currentPosition);
                    notifyItemRangeChanged(0, getItemCount());
                    notifyDataSetChanged();
                }
            });
        } else {
            holder.more.setVisibility(View.VISIBLE);
            holder.agree.setBackgroundColor(Color.parseColor("#2cc990"));
            holder.agree.setTextColor(Color.WHITE);
            holder.agree.setText("Принять");

            if (!contents.get(position).getComment().equals("")) {
                holder.helpInfo.setText("Сообщение от " + contents.get(currentPosition).getTitle() + ":\n" + contents.get(position).getComment());
            } else holder.helpInfo.setVisibility(View.GONE);

            if (!Heap.accountUser.getRequestTouristController().getRequests().get(requestId).isActive())
                holder.agree.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        AlertDialog.Builder builder = new AlertDialog.Builder(context);
                        builder.setTitle("Принятие приглашения")
                                .setMessage("Вы принимаете приглашение от " + contents.get(position).getTitle() + ".\n\nДля вашего удобства мы отклоним приглашения от других отелей.")
                                .setIcon(R.drawable.ic_check_blue_24dp)
                                .setPositiveButton("Да",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                NetworkService.requestController.sendRequest(RequestConstructor.CInviteStatus(contents.get(position).getRequestId(), contents.get(position).getInviteId(), 2), "SInviteStatus", new SocketInterface() {
                                                    @Override
                                                    public void onSuccess(String request) {
                                                        Heap.accountUser.getRequestTouristController().getRequests().get(requestId).setActive(true);

                                                        Invite invite = contents.get(position);
                                                        Heap.accountUser.getRequestTouristController().getRequests().get(requestId).getInvites().clear();
                                                        Heap.accountUser.getRequestTouristController().getRequests().get(requestId).getInvites().add(invite);

                                                        notifyItemRangeChanged(0, getItemCount());
                                                        notifyDataSetChanged();
                                                        refreshHeader.refreshHeader();
                                                        Heap.accountUser.getRequestTouristController().getRequestObserver().onNewRequest();
                                                        //LocalBroadcastManager.getInstance(context).sendBroadcast(new Intent(TouristRequestListActivity.tagNewRequest));
                                                        holder.agree.setVisibility(View.GONE);
                                                    }

                                                    @Override
                                                    public void onError(int error) {
                                                        Toast.makeText(context, "Возникла ошибка при принятии приглашения", Toast.LENGTH_LONG).show();
                                                    }
                                                });
                                            }
                                        })
                                .setNeutralButton("Нет",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                dialog.dismiss();
                                            }
                                        });
                        AlertDialog alert = builder.create();
                        alert.show();

                    }
                });
            else holder.agree.setVisibility(View.GONE);
        }

        if (contents.get(position).getPhoto().length() > 0)
            Glide.with(context)
                    .load(contents.get(position).getPhoto())
                    .centerCrop()
                    .listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            holder.progressBar.setVisibility(View.GONE);
                            return false;
                        }
                    }).into(holder.image);
        else {
            holder.progressBar.setVisibility(View.GONE);
            Glide.with(context).load(((contents.get(position).getType() & Hotel.HOST_TYPE) == Hotel.HOST_TYPE) ? R.drawable.png_home_blue : R.drawable.png_hotel_blue).into(holder.image);
        }

        if((contents.get(position).getType() & Hotel.HOSTEL_TYPE) == Hotel.HOSTEL_TYPE)
            Glide.with(context).load(R.drawable.png_hostel_blue).into(holder.hostType);
        else if((contents.get(position).getType() & Hotel.HOTEL_TYPE) == Hotel.HOTEL_TYPE)
            Glide.with(context).load(R.drawable.png_hotel_blue).into(holder.hostType);
        else Glide.with(context).load(R.drawable.png_home_blue).into(holder.hostType);

        //Glide.with(context).load(contents.get(position).getType() == 0 ? R.drawable.png_home_blue : R.drawable.png_hotel_blue).into(holder.hostType);

        holder.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showHotel(contents.get(position).getHotelId(), contents.get(position).getType());
            }
        });
        holder.more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showHotel(contents.get(position).getHotelId(), contents.get(position).getType());

            }
        });



        if (!Heap.accountUser.getRequestTouristController().getRequests().get(requestId).isActive()) {
            //app:srcCompat="@drawable/ic_delete_gray_24dp"
            Drawable deleteIcon = VectorDrawableCompat.create(context.getResources(), R.drawable.ic_delete_gray_24dp, context.getTheme());
            holder.delete.setImageDrawable(deleteIcon);
            holder.delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showDeleteDialog(position);
                }
            });
        } else holder.delete.setVisibility(View.GONE);
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.progress_bar) ProgressBar progressBar;
        @BindView(R.id.agree) TextView agree;
        @BindView(R.id.more) TextView more;
        @BindView(R.id.image) ImageView image;
        @BindView(R.id.title) TextView title;
        @BindView(R.id.city) TextView city;
        @BindView(R.id.menu) ImageView delete;
        @BindView(R.id.hostType) ImageView hostType;
        @BindView(R.id.additionalInfo) TextView helpInfo;

        private ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    private void showDeleteDialog(final int position) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Удаление приглашения")
                .setMessage("Вы уверенны что хотие удалить приглашение?")
                .setIcon(R.drawable.ic_delete_blue_24dp)
                .setPositiveButton("Да",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                NetworkService.requestController.sendRequest(RequestConstructor.CInviteStatus(contents.get(position).getRequestId(), contents.get(position).getInviteId(), 3), "SInviteStatus", new SocketInterface() {
                                    @Override
                                    public void onSuccess(String request) {
                                        contents.remove(position);
                                        notifyItemRangeChanged(0, getItemCount());
                                        notifyDataSetChanged();
                                    }

                                    @Override
                                    public void onError(int error) {
                                        Toast.makeText(context, "Возникла ошибка при отмене пришлашения", Toast.LENGTH_SHORT).show();
                                    }
                                });
                            }

                        }).setNegativeButton("Нет",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        builder.create().show();
    }

    private void showHotel(final int idHotel, final int type) {

        new Thread(new Runnable() {
            @Override
            public void run() {

                NetworkService.requestController.sendRequest(RequestConstructor.CGetProfile(((type & Hotel.HOST_TYPE) == Hotel.HOST_TYPE) ? 2 : 3, idHotel), "SGetProfile", new SocketInterface() {
                    @Override
                    public void onSuccess(String request) {
                        //int idImage = 0;
                        String jsonArray = "";
                        final ArrayList<String> images = new ArrayList<>();
                        try {
                            //idImage = new JSONObject(request).getJSONArray("data").getInt(0);

                            jsonArray = new JSONObject(request).getJSONArray("data").toString();
                            //intent.putExtra("jsonArray", new JSONObject(request).getJSONArray("data").toString());
                        } catch (JSONException e) {
                            return;
                        }

                        NetworkService.requestController.sendRequest(RequestConstructor.CGetImages(((type & Hotel.HOST_TYPE) == Hotel.HOST_TYPE) ? 2 : 3, idHotel), "SGetImages", new SocketInterface() {
                            @Override
                            public void onSuccess(String request) {
                                images.addAll(RequestController.getLinkImagesWithOutId(request));
                            }

                            @Override
                            public void onError(int error) {
                            }
                        });

                        Intent intent = new Intent(context, ProfileHotelActivity.class);
                        intent.putExtra("isAnotherHotel", true);
                        intent.putExtra("jsonArray", jsonArray);
                        intent.putExtra("images", images);

                        context.startActivity(intent);

                        //ProfileHotelFragment fragment = ProfileHotelFragment.newInstance(true, images, jsonArray);
                        //((AppCompatActivity)context).getSupportFragmentManager().beginTransaction().addToBackStack("").add(R.id.frame, fragment).commit();
                    }

                    @Override
                    public void onError(int error) {

                    }
                });
            }

        }).start();
    }

    public ArrayList<Invite> getContents() {
        return contents;
    }

    public void setContents(ArrayList<Invite> contents) {
        this.contents = contents;
        notifyItemRangeChanged(0, contents.size());
        notifyDataSetChanged();
    }
}