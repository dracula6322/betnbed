package com.betnbed.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.betnbed.R;
import com.betnbed.model.Image;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.util.List;

public class ImageAdapter extends BaseAdapter {

    private final LayoutInflater inflater;
    private final Context context;
    private List<Image> content;
    public String avatar;



    public ImageAdapter(Context context,List<Image> content, String avatar) {
        this.context = context;
        this.content = content;
        this.avatar = avatar;
        inflater = LayoutInflater.from(context);

    }

    @Override
    public int getCount() {
        return content == null ? 0 : content.size();
    }

    @Override
    public String getItem(int position) {
        return content.get(position).getUrl();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        View view = convertView;
        if (view == null) {
            view = inflater.inflate(R.layout.item_grid_image, parent, false);
            holder = new ViewHolder();
            holder.imageView = (ImageView) view.findViewById(R.id.image);
            holder.avatar = (ImageView) view.findViewById(R.id.avatar);
            holder.progressBar = (ProgressBar) view.findViewById(R.id.progress_bar);
            holder.progressBar.getIndeterminateDrawable().setColorFilter(Color.parseColor("#00a0f0"), PorterDuff.Mode.MULTIPLY);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }


        if (avatar.equals(content.get(position).getUrl())) {
            holder.avatar.setVisibility(View.VISIBLE);
        } else holder.avatar.setVisibility(View.GONE);

        Glide.with(context)
                .load(content.get(position).getUrl())
                .centerCrop()
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        holder.progressBar.setVisibility(View.GONE);
                        return false;
                    }
                })
                .into(holder.imageView);

        return view;
    }

    public List<Image> getContent() {
        return content;
    }

    public void setContent(List<Image> content) {
        this.content = content;
        notifyDataSetInvalidated();
        notifyDataSetChanged();
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
        notifyDataSetInvalidated();
        notifyDataSetChanged();
    }

    class ViewHolder {
        ProgressBar progressBar;
        ImageView imageView;
        ImageView avatar;

    }

}

