package com.betnbed.adapters;

import android.content.DialogInterface;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.betnbed.Heap;
import com.betnbed.R;
import com.betnbed.RequestConstructor;
import com.betnbed.interfaces.SocketInterface;
import com.betnbed.model.Request;
import com.betnbed.service.NetworkService;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MyGuestWaitingListRecyclerViewAdapter extends MyHostRecycleViewAdapter<MyGuestWaitingListRecyclerViewAdapter.ViewHolder> {


    private final ArrayList<Request> contents;

    public MyGuestWaitingListRecyclerViewAdapter() {
        this.contents = Heap.accountHotel.requestHostController.withInvite;
    }

    /*@Override
    public int getItemCount() {
        return contents.size();
    }*/

    @Override
    public MyGuestWaitingListRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.list_view_waiting, parent, false));
    }

    @Override
    public void onBindViewHolder(final MyGuestWaitingListRecyclerViewAdapter.ViewHolder h, final int position) {
        super.onBindViewHolder(h, h.getAdapterPosition());

        final Request q = contents.get(h.getAdapterPosition());

        ////////////////////////////////////////////////////////////////////////////////////////////


        if (q.getReasonInActive() > 0) {
            h.header.setBackgroundColor(Color.parseColor("#9e9b9c"));
            h.more.setBackgroundColor(Color.parseColor("#9e9b9c"));
            h.more.setTextColor(Color.WHITE);
            h.more.setText("Убрать");
            h.more.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    NetworkService.requestController.sendRequest(RequestConstructor.HIgnoreRequest(q.getSenderId(), q.getRequestId()),"GIgnoreRequest", new SocketInterface() {
                        @Override
                        public void onSuccess(String request) {
                            contents.remove(position);
                            notifyItemRangeChanged(0, getItemCount());
                            notifyDataSetChanged();
                        }

                        @Override
                        public void onError(int error) {

                        }
                    });

                }
            });
            h.additionalInfo.setVisibility(View.VISIBLE);
            switch (q.getReasonInActive()) {
                case Request.TOURIST_DELETE_REQUEST:
                    h.additionalInfo.setText("Уважаемый " + Heap.accountHotel.getTitle() + ", гость самостоятельно удалил свою заявку.");
                    break;
                case Request.TOURIST_AGREED_ANOTHER_INVITE:
                    h.additionalInfo.setText("Уважаемый " + Heap.accountHotel.getTitle() + ", спасибо за присланное приглашение, мною было принято приглашение другого отеля.");
                    break;
                case Request.TOURIST_DELETE_OUR_INVITE:
                    h.additionalInfo.setText("Уважаемый " + Heap.accountHotel.getTitle() + ", " + q.getFirstName() + " " + q.getSecondName() + " не готов принять Ваше приглашение.");
                    break;
            }


        } else {
            h.header.setBackgroundColor(Color.parseColor("#fcb941"));
            h.more.setBackground(context.getResources().getDrawable(R.drawable.edittext_gray));
            h.more.setText("Отозвать приглашение");
            h.more.setTextColor(Color.parseColor("#ff9e9b9c"));
            h.more.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showRejectRequestDialog(position);
                }
            });
        }


    }

    static class ViewHolder extends MyHostRecycleViewAdapter.ViewHolder {

        @BindView(R.id.more) TextView more;
        @BindView(R.id.header) LinearLayout header;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }

    private void showRejectRequestDialog(final int position){
        View myView = LayoutInflater.from(context).inflate(R.layout.dialog_custom_invite, null);
        final EditText et = (EditText) myView.findViewById(R.id.editText);
        et.setHint("Укажите причину отзыва приглашения");
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setView(myView)
                .setCancelable(true)
                .setIcon(R.drawable.png_agreement_green)
                .setTitle("Отозвать приглашение?")
                .setPositiveButton("Отозвать", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        NetworkService.requestController.sendRequest(RequestConstructor.HDeleteInvite(contents.get(position).getSenderId(), contents.get(position).getRequestId(), contents.get(position).getInviteId(), et.getText().toString()),"GInviteDeleted", new SocketInterface() {
                            @Override
                            public void onSuccess(String request) {
                                contents.remove(position);
                                notifyItemRangeChanged(0, getItemCount());
                                notifyDataSetChanged();
                            }

                            @Override
                            public void onError(int error) {
                                switch (error) {
                                    case 67:
                                        Toast.makeText(context, context.getString(R.string.error67), Toast.LENGTH_SHORT).show();
                                        break;
                                }
                            }
                        });
                    }
                })
                .setNeutralButton("Отменить", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        AlertDialog ad = builder.create();
        ad.show();
    }


}
