package com.betnbed.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.betnbed.R;
import com.betnbed.RequestConstructor;
import com.betnbed.controller.MessageController;
import com.betnbed.controller.SettingsController;
import com.betnbed.interfaces.SocketInterface;
import com.betnbed.model.Message;
import com.betnbed.service.NetworkService;
import com.bumptech.glide.Glide;

import org.json.JSONArray;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MessageRecyclerViewAdapter extends RecyclerView.Adapter<MessageRecyclerViewAdapter.ViewHolder> {

    private final SimpleDateFormat simpleDateFormat;

    private final int type;
    private final List<Message> contents;
    private final Context context;

    public MessageRecyclerViewAdapter(List<Message> contents, int type, Context context) {

        this.simpleDateFormat = new SimpleDateFormat("dd MMM yyyy HH:mm", Locale.getDefault());
        //this.simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

        this.contents = contents;
        this.type = type;
        this.context = context;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return contents.size();
    }

    @Override
    public MessageRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.list_view_message, parent, false));
    }


    @Override
    public void onBindViewHolder(final MessageRecyclerViewAdapter.ViewHolder holder, final int position) {

        Glide.with(context).load(type == 1 ? R.drawable.png_message_from_admin_orange : R.drawable.notification_message_png).into(holder.icon);

        holder.message.setText(contents.get(position).getMessage());
        String stringBuilder = "От Betnbed ";
        stringBuilder += simpleDateFormat.format((long) contents.get(position).getTime() * 1000);
        holder.additional.setText(stringBuilder);
        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("Удалить сообщение")
                        .setMessage("Вы уверенны что хотите удалить данное сообщение?")
                        .setIcon(R.drawable.ic_delete_gray_24dp)
                        .setPositiveButton("Да", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                NetworkService.requestController.sendRequest(RequestConstructor.CDeleteMessages(new JSONArray().put(contents.get(position).getId())), "SDeleteMessages", new SocketInterface() {
                                    @Override public void onSuccess(String request) {
                                        MessageController.removeFromDatabase(SettingsController.getAccoutUserId(), contents.get(position).getId());
                                        contents.remove(position);
                                        notifyItemRangeChanged(0, contents.size());
                                        notifyDataSetChanged();
                                    }

                                    @Override public void onError(int error) {}
                                });


                            }
                        })
                        .setNeutralButton("Нет", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();
            }
        });
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.message) TextView message;
        @BindView(R.id.additional) TextView additional;
        @BindView(R.id.delete) ImageView delete;
        @BindView(R.id.icon) ImageView icon;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}