package com.betnbed.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.graphics.drawable.VectorDrawableCompat;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.betnbed.Application;
import com.betnbed.R;
import com.betnbed.RequestConstructor;
import com.betnbed.SC;
import com.betnbed.activity.InvitesActivity;
import com.betnbed.interfaces.SocketInterface;
import com.betnbed.model.Request;
import com.betnbed.service.NetworkService;
import com.bumptech.glide.Glide;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RequestRecyclerViewAdapter extends RecyclerView.Adapter<RequestRecyclerViewAdapter.ViewHolder> {

    private SparseArray<Request> contents;
    private Context context;

    public RequestRecyclerViewAdapter(SparseArray<Request> contents) {
        this.contents = contents;

    }

    @Override
    public int getItemCount() {
        return contents.size();
    }

    @Override
    public RequestRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.list_view_tourist_request, parent, false));
    }

    @Override
    public void onBindViewHolder(final RequestRecyclerViewAdapter.ViewHolder holder, final int position) {

        final Request request = contents.valueAt(holder.getAdapterPosition());

        if (request.getFullPlaceName().isEmpty()) {
            Places.GeoDataApi.getPlaceById(Application.mGoogleApiClient, request.getPlace())
                    .setResultCallback(new ResultCallback<PlaceBuffer>() {
                        @Override
                        public void onResult(@NonNull PlaceBuffer places) {
                            if (places.getStatus().isSuccess() && places.getCount() > 0) {
                                request.setFullPlaceName(places.get(0).getAddress().toString());
                                notifyDataSetChanged();
                            }
                            places.release();
                        }
                    });
        }
        holder.place.setText(request.getFullPlaceName());
        holder.hours.setText(request.getHours());
        holder.arrival.setText(SC.df.format(request.getArrival() * 1000));
        holder.departure.setText(SC.df.format(request.getDeparture() * 1000));
        holder.money.setText(String.valueOf(request.getMoney()) + " " + request.getCurrency());
        holder.money_type.setText(context.getResources().getStringArray(R.array.paymentMethod)[request.getMoneyType()]);
        holder.people.setText(String.valueOf(request.getPeople()) + " " + "чел.");


        if (!request.getSpecial().isEmpty()) {
            holder.special.setVisibility(View.VISIBLE);
            holder.special.setText("Мои особенные пожелания:" + "\n" + request.getSpecial());
        } else
            holder.special.setVisibility(View.GONE);

        if (request.isActive())
            holder.header.setBackgroundColor(Color.parseColor("#2cc990"));
        else holder.header.setBackgroundColor(Color.parseColor("#00a0f0"));

        Glide.with(context).load(SC.wayToPayIdImage[request.getMoneyType()]).into(holder.image_money_type);

        if (!request.isActive()) {

            holder.invites.setText("   Приглашения" + " " + "(" + request.getInvites().size() + ")   ");
            holder.l1.setBackground(context.getResources().getDrawable(R.drawable.edittext_bg));
            holder.l2.setBackground(context.getResources().getDrawable(R.drawable.edittext_bg));


            Drawable menuIcon = VectorDrawableCompat.create(context.getResources(), R.drawable.ic_menu_white_24dp, context.getTheme());
            holder.popup.setVisibility(View.VISIBLE);
            holder.popup.setImageDrawable(menuIcon);
            holder.popup.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    PopupMenu popup = new PopupMenu(context, v);
                    popup.inflate(R.menu.popup);
                    popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            if (item.getItemId() == R.id.tourist_request_delete) {
                                NetworkService.requestController.sendRequest(RequestConstructor.CDeleteRequest(request.getRequestId()), "SRequestDeleted", new SocketInterface() {
                                    @Override
                                    public void onSuccess(String request) {
                                        contents.removeAt(holder.getAdapterPosition());
                                        notifyItemRemoved(holder.getAdapterPosition());
                                        notifyItemRangeChanged(0, contents.size());
                                        Toast.makeText(context, "Запрос удален", Toast.LENGTH_SHORT).show();
                                    }

                                    @Override
                                    public void onError(int error) {
                                        switch (error) {
                                            case 101:
                                                Toast.makeText(context, context.getString(R.string.errorIsEthernetNotAvailable), Toast.LENGTH_SHORT).show();
                                                break;
                                        }
                                    }
                                });
                            }
                            return false;
                        }
                    });
                    popup.show();
                }
            });
        } else {

            holder.invites.setText("   Посмотреть приглашение   ");
            holder.l1.setBackground(context.getResources().getDrawable(R.drawable.edittext_green_bg));
            holder.l2.setBackground(context.getResources().getDrawable(R.drawable.edittext_green_bg));
            holder.popup.setVisibility(View.GONE);

        }

        holder.invites.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, InvitesActivity.class);
                intent.putExtra("requestId", request.getRequestId());
                context.startActivity(intent);
                //InvitesFragment fragment = InvitesFragment.getInstance(request.getRequestId());
                //FragmentManager fragmentManager = ((AppCompatActivity) context).getSupportFragmentManager();
                //fragmentManager.beginTransaction().hide(fragmentManager.findFragmentByTag("trlf")).addToBackStack("trlf").add(R.id.frame, fragment).commit();
            }
        });

    }

    public SparseArray<Request> getContents() {
        return contents;
    }

    public void setContents(SparseArray<Request> contents) {
        this.contents = contents;
        notifyItemRangeChanged(0, contents.size());
        notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.arrival) TextView arrival;
        @BindView(R.id.departure) TextView departure;
        @BindView(R.id.money) TextView money;
        @BindView(R.id.money_type) TextView money_type;
        @BindView(R.id.people) TextView people;
        @BindView(R.id.special) TextView special;
        @BindView(R.id.place) TextView place;
        @BindView(R.id.hours) TextView hours;
        @BindView(R.id.menu) ImageView popup;
        @BindView(R.id.image_money_type) ImageView image_money_type;
        @BindView(R.id.invites) Button invites;
        @BindView(R.id.header) RelativeLayout header;
        @BindView(R.id.l1) View l1;
        @BindView(R.id.l2) View l2;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}