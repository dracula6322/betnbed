package com.betnbed.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.betnbed.Heap;
import com.betnbed.R;
import com.betnbed.RequestConstructor;
import com.betnbed.SC;
import com.betnbed.activity.profile.ProfileTouristActivity;
import com.betnbed.controller.RequestController;
import com.betnbed.interfaces.SocketInterface;
import com.betnbed.model.Hotel;
import com.betnbed.model.Request;
import com.betnbed.service.NetworkService;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.bumptech.glide.request.target.Target;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public abstract class MyHostRecycleViewAdapter<VH extends MyHostRecycleViewAdapter.ViewHolder> extends RecyclerView.Adapter<VH> {

    ArrayList<Request> contents;
    Context context;
    BitmapImageViewTarget cycleBitmapImageViewTarget;

    @Override public VH onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        return null;
    }

    @Override public void onBindViewHolder(final VH h, int position) {

        cycleBitmapImageViewTarget = new BitmapImageViewTarget(h.image) {

            @Override
            protected void setResource(Bitmap resource) {
                RoundedBitmapDrawable circularBitmapDrawable = RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                circularBitmapDrawable.setCircular(true);
                h.image.setImageDrawable(circularBitmapDrawable);
            }
        };

        final Request q = contents.get(h.getAdapterPosition());

        h.progressBar.getIndeterminateDrawable().setColorFilter(Color.parseColor("#00a0f0"), PorterDuff.Mode.MULTIPLY);
        h.inDate.setText(SC.df.format(q.getArrival() * 1000));
        h.outDate.setText(SC.df.format(q.getDeparture() * 1000));
        h.hours.setText(q.getHours());
        h.howManyPeople.setText(q.getPeople() + " " + "чел.");
        h.wayToPay.setText(context.getResources().getStringArray(R.array.paymentMethodShort)[q.getMoneyType()]);
        h.money.setText(q.getMoney() + " " + q.getCurrency());
        h.name.setText((q.getFirstName().isEmpty() && q.getSecondName().isEmpty()) ? "Имя не указано" : (q.getFirstName() + " " + q.getSecondName()));
        h.l1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shotTourist(q.getSenderId());
            }
        });

        Glide.with(context).load(SC.wayToPayIdImage[q.getMoneyType()]).centerCrop().into(h.wayToPayImage);
        if (!q.getImage().isEmpty())
            Glide.with(context)
                    .load(q.getImage())
                    .asBitmap()
                    .error(R.drawable.avatar_default)
                    .listener(new RequestListener<String, Bitmap>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<Bitmap> target, boolean isFirstResource) {

                            h.progressBar.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Bitmap resource, String model, Target<Bitmap> target, boolean isFromMemoryCache, boolean isFirstResource) {

                            h.progressBar.setVisibility(View.GONE);
                            return false;
                        }
                    })
                    .centerCrop()
                    .into(cycleBitmapImageViewTarget);
        else {
            h.progressBar.setVisibility(View.GONE);
            Glide.with(context).load(R.drawable.avatar_default).error(R.drawable.avatar_default).centerCrop().into(h.image);
        }

        if (q.getSpecial().length() > 0) {
            h.additionalInfo.setVisibility(View.VISIBLE);
            h.additionalInfo.setText("Сообщение от " + q.getFirstName() + " " + q.getSecondName() + ":\n" + q.getSpecial());
        } else h.additionalInfo.setVisibility(View.GONE);

        if (Heap.accountHotel.isHotel()) {

            if (Heap.accountHotel.getType() == (Hotel.HOSTEL_TYPE | Hotel.HOTEL_TYPE)) {
                h.hotelTypeDesc.setVisibility(View.VISIBLE);
                if ((q.getHotelType() & Hotel.HOTEL_TYPE) == Hotel.HOTEL_TYPE)
                    h.hotelTypeDesc.setText("ищет отдельный номер");
                if ((q.getHotelType() & Hotel.HOSTEL_TYPE) == Hotel.HOSTEL_TYPE)
                    h.hotelTypeDesc.setText("ищет место в хостеле");
                if (((q.getHotelType() & Hotel.HOTEL_TYPE) == Hotel.HOTEL_TYPE) && ((q.getHotelType() & Hotel.HOSTEL_TYPE) == Hotel.HOSTEL_TYPE))
                    h.hotelTypeDesc.setText("ищет отдельный номер или место в хостеле");


            } else h.hotelTypeDesc.setVisibility(View.GONE);

        } else h.hotelTypeDesc.setVisibility(View.GONE);

    }

    @Override public int getItemCount() {
        return contents == null ? 0 : contents.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.hotelTypeDesc) TextView hotelTypeDesc;
        @BindView(R.id.progress_bar) ProgressBar progressBar;
        @BindView(R.id.inDate) TextView inDate;
        @BindView(R.id.outDate) TextView outDate;
        @BindView(R.id.howManyPeople) TextView howManyPeople;
        @BindView(R.id.wayToPay) TextView wayToPay;
        @BindView(R.id.money) TextView money;
        @BindView(R.id.hours) TextView hours;
        @BindView(R.id.image) ImageView image;
        @BindView(R.id.name) TextView name;
        @BindView(R.id.way_to_pay_image) ImageView wayToPayImage;
        @BindView(R.id.l1) LinearLayout l1;
        @BindView(R.id.additionalInfo) TextView additionalInfo;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }


    void shotTourist(final int id) {
        NetworkService.requestController.sendRequest(RequestConstructor.CGetTourist(id), "SGetTourist", new SocketInterface() {
            @Override
            public void onSuccess(String request) {
                int idImage = 0;
                String jsonArray = "";
                final ArrayList<String> images = new ArrayList<>();
                try {
                    idImage = new JSONObject(request).getJSONArray("data").getInt(0);

                    jsonArray = new JSONObject(request).getJSONArray("data").toString();
                } catch (JSONException e) {
                    Log.v("TAG", e.toString());
                }

                NetworkService.requestController.sendRequest(RequestConstructor.CGetImages(1, idImage), "SGetImages", new SocketInterface() {
                    @Override
                    public void onSuccess(String request) {
                        images.addAll(RequestController.getLinkImagesWithOutId(request));
                    }

                    @Override
                    public void onError(int error) {

                    }
                });

                Intent intent = new Intent(context, ProfileTouristActivity.class);
                intent.putExtra("isAnotherTourist", true);
                intent.putExtra("jsonArray", jsonArray);
                intent.putExtra("images", images);

                context.startActivity(intent);

                //ProfileTouristFragment fragment = ProfileTouristFragment.newInstance(true, images, jsonArray);
                //((AppCompatActivity)context).getSupportFragmentManager().beginTransaction().addToBackStack("").add(R.id.frame, fragment).commit();
            }

            @Override
            public void onError(int error) {

            }
        });
    }

    public ArrayList<Request> getContents() {
        return contents;
    }

    public void setContents(ArrayList<Request> contents) {
        this.contents = contents;
        notifyItemRangeChanged(0, contents.size());
        notifyDataSetChanged();
    }
}
