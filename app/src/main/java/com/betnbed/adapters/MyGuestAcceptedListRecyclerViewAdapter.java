package com.betnbed.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.betnbed.R;
import com.betnbed.model.Request;

import butterknife.BindView;
import butterknife.ButterKnife;

public class
MyGuestAcceptedListRecyclerViewAdapter extends MyHostRecycleViewAdapter<MyGuestAcceptedListRecyclerViewAdapter.ViewHolder> {


    public MyGuestAcceptedListRecyclerViewAdapter() {
    }

    @Override
    public MyGuestAcceptedListRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        super.onCreateViewHolder(parent, viewType);
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.list_view_accepted, parent, false));
    }


    @Override
    public void onBindViewHolder(final MyGuestAcceptedListRecyclerViewAdapter.ViewHolder h, final int position) {
        super.onBindViewHolder(h, position);

        final Request q = contents.get(position);

        ////////////////////////////////////////////////////////////////////////////////////////////

        h.more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shotTourist(q.getSenderId());
            }
        });

    }

    static class ViewHolder extends MyHostRecycleViewAdapter.ViewHolder {
        @BindView(R.id.more) TextView more;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
