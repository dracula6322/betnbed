package com.betnbed;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.betnbed.interfaces.SocketInterface;
import com.betnbed.service.NetworkService;

import java.util.List;
import java.util.regex.Pattern;

public class Utils {

    public static String toString(List<Integer> a) {
        String b[] = new String[a.size()];
        for(int i=0;i<a.size();i++)
            b[i] = String.valueOf(a.get(i).toString());
        return toString(b);
    }

    public static String toString(int[] a) {
        if (a == null)
            return "null";
        int iMax = a.length - 1;
        if (iMax == -1)
            return "[]";

        StringBuilder b = new StringBuilder();
        b.append('[');
        for (int i = 0; ; i++) {
            b.append(a[i]);
            if (i == iMax)
                return b.append(']').toString();
            b.append(",");
        }
    }

    public static String toString(String[] a) {
        if (a == null)
            return "null";
        int iMax = a.length - 1;
        if (iMax == -1)
            return "[]";

        StringBuilder b = new StringBuilder();
        b.append('[');
        for (int i = 0; ; i++) {
            b.append(a[i]);
            if (i == iMax)
                return b.append(']').toString();
            b.append(",");
        }



    }

    public static boolean checkEmail(String email) {
        return !Pattern.matches("^([a-zA-Z0-9._+-]+)@([a-zA-Z0-9._+-]+)[.]([a-zA-Z]{2,5})$", email);
    }

    static public void showUpdateGMSDialog(final Context context) {

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Устаревший Google Play Service ")
                .setMessage("Для успешного использования приложения Betnbed необходимо обновить Google Play Service")
                .setCancelable(false)
                .setIcon(R.drawable.ic_warning_black_48dp)
                .setPositiveButton("Перейти в Google Play", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.setData(Uri.parse("market://details?id=com.google.android.gms"));
                        context.startActivity(intent);
                    }
                })
                .setNeutralButton("Отмена", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                    }
                });
        builder.create().show();
    }

    public static void showForgotPasswordDialog(final Context context) {
        LinearLayout linearLayout = new LinearLayout(context);
        linearLayout.setFocusableInTouchMode(true);
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        linearLayout.setPadding(16, 32, 32, 16);
        linearLayout.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));


        final TextInputLayout emailInputLayout = new TextInputLayout(context);


        final EditText email = new EditText(context);
        email.setHint("Введите email");
        email.setHintTextColor(Color.parseColor("#61000000"));
        email.setTextColor(Color.BLACK);
        email.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);

        email.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                emailInputLayout.setError("");

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        emailInputLayout.addView(email);
        emailInputLayout.setErrorEnabled(true);
        linearLayout.addView(emailInputLayout);

        AlertDialog.Builder builder = new AlertDialog.Builder(context)
                .setTitle("Сброс пароля")
                .setIcon(R.drawable.png_change_password_gray)
                .setPositiveButton("Сбросить пароль", null)
                .setNeutralButton("Отмена", null)
                .setCancelable(false)
                .setView(linearLayout);
        final AlertDialog ad = builder.create();
        ad.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialogInterface) {
                ad.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        String userEmail = emailInputLayout.getEditText().getText().toString().trim();
                        if (checkEmail(userEmail)) {
                            emailInputLayout.setError("Не верный формат email");
                            return;
                        }

                        NetworkService.requestController.sendRequest(RequestConstructor.CPassReset(emailInputLayout.getEditText().getText().toString()), "SResetEmailSend", new SocketInterface() {
                            @Override
                            public void onSuccess(String request) {
                                dialogInterface.dismiss();
                                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                                builder.setTitle("Смена пароля")
                                        .setMessage("На указанный вами email отправлено письмо с инструкциями по восстановлению пароля")
                                        .setCancelable(true)
                                        .setIcon(R.drawable.png_change_password_gray)
                                        .setPositiveButton("Ok", null);

                                builder.create().show();
                            }

                            @Override
                            public void onError(int error) {
                                switch (error) {
                                    case 10:
                                        emailInputLayout.setError("Данный email не найден");
                                        break;
                                    default:
                                        Toast.makeText(context, "Произошла ошибка " + error, Toast.LENGTH_SHORT).show();
                                        break;
                                }
                            }
                        });
                    }
                });
                ad.getButton(AlertDialog.BUTTON_NEUTRAL).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialogInterface.dismiss();
                    }
                });

            }
        });
        ad.show();
    }
}
