package com.betnbed.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.view.ViewPager;

import com.betnbed.R;
import com.betnbed.adapters.ViewPagerAdapter;
import com.betnbed.controller.MessageController;
import com.betnbed.customView.BetnbedActivity;
import com.betnbed.loader.MessageLoader;
import com.betnbed.model.Message;
import com.betnbed.service.NotificationService;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MessageActivity extends BetnbedActivity implements LoaderManager.LoaderCallbacks<List<Message>> {

    static final int LOADER_TIME_ID = 1;

    Loader loader;
    MessagePartFragment first;
    MessagePartFragment second;

    @BindView(R.id.viewpager) public ViewPager viewPager;
    @BindView(R.id.tabs) TabLayout tabLayout;

    @Override public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message);

        first = MessagePartFragment.newInstance(2, "Здесь будут видны уведомления");
        second = MessagePartFragment.newInstance(1, "Здесь будут видны сообщения от Betnbed");

        loader = getSupportLoaderManager().initLoader(LOADER_TIME_ID, null, this);
        MessageController.setContentObserver(loader.new ForceLoadContentObserver());
        loader.forceLoad();

        ButterKnife.bind(this);
        initNavigationView();
        initToolbar("Уведомления");
        initView();

    }

    @Override public void onResume() {
        super.onResume();

        NotificationService.setCountUnreadMessage(0);
        NotificationService.hideNewMessageNotiications();

    }

    @Override public void onDestroy() {
        super.onDestroy();
        MessageController.setContentObserver(null);
    }

    @Override protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        viewPager.setCurrentItem(intent.getIntExtra("page", 0));
    }

    @Override public Loader<List<Message>> onCreateLoader(int id, Bundle args) {
        return new MessageLoader(this);
    }

    @Override public void onLoadFinished(Loader<List<Message>> loader, List<Message> data) {
        first.setMessages(data);
        second.setMessages(data);

    }

    @Override public void onLoaderReset(Loader<List<Message>> loader) {}

    private void initView() {
        initTabLayout();
    }

    private void initTabLayout() {

        final ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(first, "Уведомления");
        adapter.addFragment(second, "Сообщения");


        viewPager.setAdapter(adapter);
        viewPager.setCurrentItem(getIntent().getIntExtra("page", 0));

        tabLayout.setupWithViewPager(viewPager);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
                toolbar.setTitle(adapter.getPageTitle(tab.getPosition()));
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {}

            @Override
            public void onTabReselected(TabLayout.Tab tab) {}
        });
    }

}
