package com.betnbed.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;

import com.betnbed.R;
import com.betnbed.adapters.ViewPagerAdapter;
import com.betnbed.customView.BetnbedActivity;
import com.betnbed.fragment.CreateRequestFragment;
import com.betnbed.fragment.RequestsListFragment;
import com.betnbed.interfaces.MovingToTab;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TouristRequestListActivity extends BetnbedActivity {

    @BindView(R.id.viewpager) ViewPager viewPager;
    @BindView(R.id.tabs) TabLayout tabLayout;

    @Override public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_title);
        /*if (!SettingsController.isFirstLaunchTourist())
            getFragmentManager().beginTransaction().hide(this).addToBackStack("").add(R.id.frame, TutorialSettingsFragment.newInstance(0)).commit();*/

        /*if (getIntent().getExtras().getInt(ARGUMENT_GSM_ERROR, 0) != 0)
            Utils.showUpdateGMSDialog(this);*/

        ButterKnife.bind(this);

        initNavigationView();
        initToolbar("Betnbed");
        initView();


    }

    @Override protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        viewPager.setCurrentItem(intent.getIntExtra("page", 0));
    }

    private void initView() {initTabLayout();}

    private void initTabLayout() {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        RequestsListFragment requestsListFragment = RequestsListFragment.newInstance();
        CreateRequestFragment createRequestFragment = CreateRequestFragment.newInstance();

        MovingToTab movingToTab = new MovingToTab() {
            @Override public void moveTo(int position) {
                viewPager.setCurrentItem(position);
            }
        };

        requestsListFragment.setMovingToTab(movingToTab);
        createRequestFragment.setMovingToTab(movingToTab);

        adapter.addFragment(requestsListFragment, "Мои приглашения");
        adapter.addFragment(createRequestFragment, "Создать поездку");


        viewPager.setAdapter(adapter);
        viewPager.setCurrentItem(getIntent().getIntExtra("page", 0));

        tabLayout.setupWithViewPager(viewPager);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {}

            @Override
            public void onTabReselected(TabLayout.Tab tab) {}
        });
    }

    /*@Override public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN) {

            BaseModel.BaseBuilder baseBuilder = new BaseModel.BaseBuilder()
                    .internet(1)
                    .breakfast(1)
                    .parking(1)
                    .media(1)
                    .shower(1)
                    .kitchen(1)
                    .pets(1)
                    .services(1)
                    .moneyType(1)
                    .hotelType(1);

            final Request.Builder request = new Request.Builder()
                    .catsAndDogs(baseBuilder)
                    .senderId(22)
                    .requestId(33)
                    .place("23")
                    .arrival(1491970600)
                    .departure(1493674160)
                    .money(22)
                    .currency("RUB")
                    .people(1)
                    .special("")
                    .mask(64)
                    .inviteId(1)
                    .special("допольнитьельняа фывфывфыв фывыфвфв фв фыв фыв фыв фыв фы")
                    .user(new User.Builder().firstName("1").lastName("21").photo("").build());


            new Thread(new Runnable() {
                @Override public void run() {
                    try {
                        Thread.sleep(5000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    Log.v("TAG", "We add request");

                    Heap.accountUser.getRequestTouristController().addRequest(request.build());

                }
            }).start();


        }
        return super.onKeyDown(keyCode, event);
    }*/

}
