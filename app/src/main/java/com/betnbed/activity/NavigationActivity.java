package com.betnbed.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.betnbed.Application;
import com.betnbed.Heap;
import com.betnbed.R;
import com.betnbed.RequestSender;
import com.betnbed.activity.login.LoginHotelActivity;
import com.betnbed.activity.login.LoginStartActivity;
import com.betnbed.activity.login.LoginTourisActivity;
import com.betnbed.fragment.SettingsFragment;
import com.betnbed.service.NetworkService;
import com.betnbed.service.NotificationService;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.Places;

public class NavigationActivity extends AppCompatActivity {

    static public final String tagLoginIsEnd = "NavigationFragment_LoginIsEnd";


    boolean weMustKill;
    LoginIsEnd loginIsEnd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_navigation);

        weMustKill = false;

        startService(new Intent(this, NetworkService.class));

        Application.mGoogleApiClient = new GoogleApiClient
                .Builder(this)
                .addApi(Places.GEO_DATA_API)
                .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                    @Override
                    public void onConnected(@Nullable Bundle bundle) {
                        Log.v("TAG", "Google onConnected ");
                    }

                    @Override
                    public void onConnectionSuspended(int i) {
                        Log.v("TAG", "Google onConnectionSuspended ");
                    }


                })
                .addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
                        //gmsError = connectionResult.getErrorCode();
                        Log.v("TAG", "Google onConnectionFailed " + connectionResult.getErrorCode());
                    }
                })
                .build();
        Application.mGoogleApiClient.connect();


        loginIsEnd = new LoginIsEnd();
        LocalBroadcastManager.getInstance(this).registerReceiver(loginIsEnd, new IntentFilter(tagLoginIsEnd));

    }

    @Override protected void onResume() {
        super.onResume();
        if (weMustKill) {
            finish();
            stopService(new Intent(this, NetworkService.class));
        }
    }

    @Override protected void onPause() {
        super.onPause();
        weMustKill = true;
    }

    @Override protected void onStop() {
        super.onStop();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(loginIsEnd);
    }

    @Override public void onDestroy() {
        super.onDestroy();
        finish();
        stopService(new Intent(this, NetworkService.class));
    }


    @Override protected void onNewIntent(Intent newIntent) {
        super.onNewIntent(newIntent);

        weMustKill = false;

        Log.v("LC", "onNewIntent " + newIntent.toString());
        if (newIntent.getAction() != null) {

            String action = newIntent.getAction();

            if (action.equals(SettingsFragment.LOGOUT_FROM_SETTINGS)) {
                Intent intent = new Intent(NavigationActivity.this, LoginStartActivity.class);
                startActivity(intent);
                return;
            }

            if (action.equals(LoginHotelActivity.SUCCES_HOTEL_LOGIN)) {
                Intent intent = new Intent(this, MyHostActivity.class);
                startActivity(intent);
                return;
            }

            if (action.equals(LoginTourisActivity.SUCCES_TOURIST_LOGIN)) {
                Intent intent = new Intent(this, TouristRequestListActivity.class);
                startActivity(intent);
                return;
            }
        }


        int openPage = newIntent.getIntExtra("openPage", 0);

        if (openPage == NotificationService.NEW_MESSAGE) {
            Intent intent = new Intent(this, MessageActivity.class);
            startActivity(intent);
        }
        if (openPage == NotificationService.NEW_INVITE) {
            Intent intent = new Intent(this, TouristRequestListActivity.class);
            startActivity(intent);
            //getSupportFragmentManager().popBackStack("trlf", FragmentManager.POP_BACK_STACK_INCLUSIVE);
            //LocalBroadcastManager.getInstance(this).sendBroadcast(new Intent(MyHostFragment.tagOpenPage).putExtra("OPEN_PAGE", 0));
        }

        if (openPage == NotificationService.OUR_INVITE_ACCEPT) {
            int page = 2;
            Intent intent = new Intent(this, MyHostActivity.class);
            startActivity(intent);
            /*if (SettingsController.isHotel()) {
                getSupportFragmentManager().popBackStack("mhf", FragmentManager.POP_BACK_STACK_INCLUSIVE);
                LocalBroadcastManager.getInstance(this).sendBroadcast(new Intent(MyHostFragment.tagOpenPage).putExtra("OPEN_PAGE", page));
            } else {
                getSupportFragmentManager().popBackStack("trlf", FragmentManager.POP_BACK_STACK_INCLUSIVE);
                Fragment fragment = MyHostFragment.newInstance(page, false);
                getSupportFragmentManager().beginTransaction().hide(getSupportFragmentManager().findFragmentByTag("trlf")).addToBackStack("trlf").add(R.id.frame, fragment, "mhf").commit();
            }*/
        }

    }

    public class LoginIsEnd extends BroadcastReceiver {
        @Override public void onReceive(Context context, Intent broadcastIntent) {

            int state = broadcastIntent.getIntExtra("state", 0);

            if (state == RequestSender.WITH_LOGIN_HOTEL) Heap.initHotelProfile(3, context);

            if (state == RequestSender.WITH_LOGIN_TOURIST) {
                Heap.initUserProfile();
                Heap.initHotelProfile(2, context);
            }

            if (state == RequestSender.OLD_VERSION) {
                Intent intent = new Intent(NavigationActivity.this, LoadApplicationActivity.class);
                startActivity(intent);
                return;
            }

            if (state == RequestSender.WITH_OUT_LOGIN || state == RequestSender.DISCONNECTED) {
                Intent intent = new Intent(NavigationActivity.this, LoginStartActivity.class);
                startActivity(intent);
                return;
            }


            if (state == RequestSender.WITH_LOGIN_HOTEL || state == RequestSender.WITH_LOGIN_TOURIST) {

                boolean isTourist = state != RequestSender.WITH_LOGIN_HOTEL;
                Intent newIntent;

                int page = getIntent().getIntExtra("openPage", 0);
                Log.v("TAG", "Page = " + page);

                switch (page) {

                    case 0:
                        newIntent = new Intent(context, isTourist ? TouristRequestListActivity.class : MyHostActivity.class);
                        startActivity(newIntent);
                        return;
                    case NotificationService.NEW_INVITE:
                        newIntent = new Intent(context, TouristRequestListActivity.class);
                        startActivity(newIntent);
                        return;
                    case NotificationService.NEW_MESSAGE:
                        newIntent = new Intent(context, MessageActivity.class);
                        startActivity(newIntent);
                        return;
                    case NotificationService.OUR_INVITE_ACCEPT:
                        newIntent = new Intent(context, MyHostActivity.class);
                        startActivity(newIntent);
                        return;

                }
                throw new RuntimeException("Some proble with page in requestController");
            }
            throw new RuntimeException("Some proble with state in requestController " + state);
        }
    }


}
