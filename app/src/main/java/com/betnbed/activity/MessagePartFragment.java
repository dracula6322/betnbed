package com.betnbed.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.betnbed.R;
import com.betnbed.adapters.MessageRecyclerViewAdapter;
import com.betnbed.model.Message;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MessagePartFragment extends Fragment {

    public MessageRecyclerViewAdapter adapter;
    ArrayList<Message> messages = new ArrayList<>();
    int type;
    @BindView(R.id.message) public RecyclerView rv;
    @BindView(R.id.emptyLayout) public View emptyLayout;
    @BindView(R.id.emptyDesc) public TextView emptyDesc;
    @BindView(R.id.emptyImage) public ImageView emptyImage;

    static public MessagePartFragment newInstance(int type, String emptyDesc) {

        MessagePartFragment fragment = new MessagePartFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("type", type);
        bundle.putString("emptyDesc", emptyDesc);
        fragment.setArguments(bundle);

        return fragment;

    }

    @Override public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        type = getArguments().getInt("type");

        adapter = new MessageRecyclerViewAdapter(messages, type, getActivity());
        adapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onItemRangeChanged(int positionStart, int itemCount) {
                super.onItemRangeChanged(positionStart, itemCount);
                emptyLayout.setVisibility(itemCount == 0 ? View.VISIBLE : View.GONE);
            }
        });

    }

    @Nullable @Override public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_part_message, container, false);

        ButterKnife.bind(this, view);
        initView();
        return view;
    }


    private void refreshRecycleView() {
        if (adapter == null) return;

        adapter.notifyItemRangeChanged(0, adapter.getItemCount());
        adapter.notifyDataSetChanged();
    }

    public void setMessages(List<Message> newMessages) {

        int start = type == 1 ? 0 : 3;
        int end = type == 1 ? 3 : 10;

        messages.clear();
        for (Message message : newMessages)
            if (message.getType() > start && message.getType() < end)
                messages.add(message);

        refreshRecycleView();
    }

    private void initView() {

        Glide.with(this).load(type == 1 ? R.drawable.png_message_from_admin_gray : R.drawable.png_notification_gray).into(emptyImage);

        emptyDesc.setText(getArguments().getString("emptyDesc"));

        rv.setLayoutManager(new LinearLayoutManager(getActivity()));
        rv.setAdapter(adapter);

        refreshRecycleView();

    }

}
