package com.betnbed.activity.login;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.betnbed.Heap;
import com.betnbed.R;
import com.betnbed.RequestConstructor;
import com.betnbed.RequestSender;
import com.betnbed.activity.LoginActivity;
import com.betnbed.activity.NavigationActivity;
import com.betnbed.controller.SettingsController;
import com.betnbed.interfaces.SocketInterface;
import com.betnbed.service.NetworkService;
import com.vk.sdk.VKAccessToken;
import com.vk.sdk.VKCallback;
import com.vk.sdk.VKScope;
import com.vk.sdk.VKSdk;
import com.vk.sdk.VKServiceActivity;
import com.vk.sdk.api.VKError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginTourisActivity extends LoginActivity {

    @BindView(R.id.enter) TextView enter;

    @OnClick(R.id.vk) void vk() {
        /*String[] sMyScope = new String[]{};
        VKSdk.login(getActivity(), sMyScope);*/

        Intent intent = new Intent(this, VKServiceActivity.class);
        intent.putExtra("arg1", "Authorization");
        ArrayList scopes = new ArrayList<>();
        scopes.add(VKScope.OFFLINE);
        intent.putStringArrayListExtra("arg2", scopes);
        intent.putExtra("arg4", VKSdk.isCustomInitialize());
        startActivityForResult(intent, VKServiceActivity.VKServiceType.Authorization.getOuterCode());

    }

    @OnClick(R.id.registration) void registration() {
        openRegistrationAcitivity(LoginCreateAccountActivity.TOURIST_TYPE);
    }

    @OnClick(R.id.login) void login(View v) {
        final String userLogin = email.getEditText().getText().toString().trim();
        if (userLogin.isEmpty()) {
            email.setError(getString(R.string.email) + " не может буть пустым");
            return;
        }

        final String userPassword = password.getEditText().getText().toString().trim();
        if (userPassword.isEmpty()) {
            password.setError("Пароль не может буть пустым");
            return;
        }

        v.setEnabled(false);
        progressBarBetnbedLogin.setVisibility(View.VISIBLE);

        new Thread(new Runnable() {
            @Override
            public void run() {
                NetworkService.requestController.sendRequest(RequestConstructor.CLogin(userLogin, userPassword), "SLogin", new SocketInterface() {
                    @Override
                    public void onSuccess(String request) {
                        try {
                            JSONArray jsonArray = new JSONObject(request).getJSONArray("data");

                            if (jsonArray.getInt(4) == 2) {
                                handlerSetButtonEnable.sendEmptyMessage(102);

                                NetworkService.requestController.sendRequest(RequestConstructor.CLogout(), "SLogout", new SocketInterface() {
                                    @Override
                                    public void onSuccess(String request) {
                                    }

                                    @Override
                                    public void onError(int error) {
                                    }
                                });
                            } else login(userLogin, userPassword, jsonArray.getInt(0), jsonArray.getInt(1), jsonArray.getInt(2), jsonArray.getInt(3));
                        } catch (JSONException e) {
                            Log.v("TAG", e.toString());
                        }
                    }

                    @Override
                    public void onError(int error) {

                        if (error == 9) {
                            NetworkService.requestController.sendRequest(RequestConstructor.CLogout(), "SLogout", new SocketInterface() {
                                @Override
                                public void onSuccess(String request) {
                                    NetworkService.requestController.sendRequest(RequestConstructor.CLogin(userLogin, userPassword), "CLogin", new SocketInterface() {
                                        @Override
                                        public void onSuccess(String request) {
                                            try {
                                                JSONArray jsonArray = new JSONObject(request).getJSONArray("data");
                                                login(userLogin, userPassword, jsonArray.getInt(0), jsonArray.getInt(1), jsonArray.getInt(2), jsonArray.getInt(3));
                                            } catch (JSONException e) {
                                                Log.v("TAG", e.toString());
                                            }
                                        }

                                        @Override
                                        public void onError(int error) {
                                            handlerSetButtonEnable.sendEmptyMessage(error);
                                        }
                                    });
                                }

                                @Override
                                public void onError(int error) {
                                    handlerSetButtonEnable.sendEmptyMessage(error);
                                }
                            });

                        } else handlerSetButtonEnable.sendEmptyMessage(error);
                    }
                });
            }
        }).start();

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


        /*NetworkService.requestController.sendRequestWithObservable(RequestConstructor.CLogin(userLogin, userPassword), "SLogin")
                .flatMap(new Function<JSONObject, SingleSource<JSONObject>>() {
                    @Override public SingleSource<JSONObject> apply(@NonNull JSONObject jsonObject) throws Exception {
                        JSONArray jsonArray = jsonObject.getJSONArray("data");

                        if (jsonArray.getInt(4) == 2)
                            return Single.error(new BetnbedException(102));

                        return Single.just(jsonObject);
                    }
                })
                .doOnError(new Consumer<Throwable>() {
                    @Override public void accept(@NonNull Throwable throwable) throws Exception {
                        NetworkService.requestController.sendRequestWithObservable(RequestConstructor.CLogout(), "SLogout");
                    }
                })
                .doOnSuccess(new Consumer<JSONObject>() {
                    @Override public void accept(@NonNull JSONObject o) throws Exception {
                        JSONArray jsonArray = o.getJSONArray("data");
                        login(userLogin, userPassword, jsonArray.getInt(0), jsonArray.getInt(1), jsonArray.getInt(2), jsonArray.getInt(3));
                    }
                })
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<JSONObject>() {
                    @Override public void accept(@NonNull JSONObject o) throws Exception {

                    }
                }, new Consumer<Throwable>() {
                    @Override public void accept(@NonNull Throwable throwable) throws Exception {
                        if(throwable instanceof BetnbedException) {

                            int errorCode = ((BetnbedException) throwable).getErrorCode();
                            handlerSetButtonEnable.sendEmptyMessage(errorCode);
                        }
                    }
                });
*/

    }

    @Override public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_tourist);

        ButterKnife.bind(this);
        initView();

    }

    @Override protected void initView() {
        super.initView();

        registrationLink.setLinkTextColor(getResources().getColor(getIntent().getIntExtra("type", 0) == 1 ? R.color.betnbed_orange : R.color.betnbed_blue));
        forgetPassword.setLinkTextColor(getResources().getColor(getIntent().getIntExtra("type", 0) == 1 ? R.color.betnbed_orange : R.color.betnbed_blue));
        enter.setTextColor(getResources().getColor(getIntent().getIntExtra("type", 0) == 1 ? R.color.betnbed_orange : R.color.betnbed_blue));
        login.setBackgroundColor(getResources().getColor(getIntent().getIntExtra("type", 0) == 1 ? R.color.betnbed_orange : R.color.betnbed_blue));

        progressBarBetnbedLogin.getIndeterminateDrawable().setColorFilter(Color.parseColor("#00a0f0"), PorterDuff.Mode.MULTIPLY);
    }

    private void initLoginVK(final String userId, final String token) {
        NetworkService.requestController.sendRequest(RequestConstructor.CLogin(Integer.parseInt(userId), token), "SLogin", new SocketInterface() {
            @Override
            public void onSuccess(String request) {
                try {
                    JSONArray jsonArray = new JSONObject(request).getJSONArray("data");
                    SettingsController.setTagAccountUserId(jsonArray.getInt(0));
                    Heap.touristId = jsonArray.getInt(1);
                    Heap.hostId = jsonArray.getInt(2);
                    Heap.hotelId = jsonArray.getInt(3);

                } catch (JSONException e) {
                    Log.v("TAG", e.toString());
                }
                SettingsController.setIsLoginVKUserId(Integer.parseInt(userId));
                SettingsController.setIsLoginVKUserToken(token);
                SettingsController.setIsHotel(false);
                Heap.initUserProfile();
                Heap.initHotelProfile(2, LoginTourisActivity.this);
                NetworkService.requestController.setState(RequestSender.WITH_LOGIN_TOURIST);
                openTouristRequestListFragment();
            }

            @Override
            public void onError(int error) {
                switch (error) {
                    case 9:
                        email.setError("Вы уже залогинены, перезапустите приложение");
                        break;
                }
            }
        });

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (!VKSdk.onActivityResult(requestCode, resultCode, data, new VKCallback<VKAccessToken>() {
            @Override
            public void onResult(VKAccessToken res) {
                initLoginVK(res.userId, res.accessToken);

            }

            @Override
            public void onError(VKError error) {
                if (error.errorCode == VKError.VK_CANCELED)
                    return;
                Toast.makeText(LoginTourisActivity.this, "Возникла ошибка с сервером VK, пожалуйста попробуйте позже", Toast.LENGTH_SHORT).show();
            }
        })) {
        }

    }

    protected void login(String userLogin, String userPassword, int idAccount, int touristId, int hostId, int hotelId) {
        super.login(userLogin, userPassword, idAccount, touristId, hostId, hotelId);
        SettingsController.setIsHotel(false);
        Heap.initUserProfile();
        Heap.initHotelProfile(2, this);
        NetworkService.requestController.setState(RequestSender.WITH_LOGIN_TOURIST);

        openTouristRequestListFragment();

    }

    static public final String SUCCES_TOURIST_LOGIN = "SUCCES_TOURIST_LOGIN";


    private void openTouristRequestListFragment() {
        /*Intent intent = new Intent(this, TouristRequestListActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);*/

        Intent intent = new Intent(this, NavigationActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.setAction(SUCCES_TOURIST_LOGIN);
        startActivity(intent);


    }
}
