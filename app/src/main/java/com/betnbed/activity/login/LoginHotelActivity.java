package com.betnbed.activity.login;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.betnbed.Heap;
import com.betnbed.R;
import com.betnbed.RequestConstructor;
import com.betnbed.RequestSender;
import com.betnbed.activity.LoginActivity;
import com.betnbed.activity.NavigationActivity;
import com.betnbed.controller.SettingsController;
import com.betnbed.interfaces.SocketInterface;
import com.betnbed.service.NetworkService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginHotelActivity extends LoginActivity {

    @OnClick(R.id.registration) void registration() {
        openRegistrationAcitivity(LoginCreateAccountActivity.HOTEL_TYPE);
    }

    @OnClick(R.id.login) void login(View v) {
        final String userLogin = email.getEditText().getText().toString().trim();
        if (userLogin.isEmpty()) {
            email.setError(getString(R.string.email) + " не может буть пустым");
            return;
        }

        final String userPassword = password.getEditText().getText().toString().trim();
        if (userPassword.isEmpty()) {
            password.setError("Пароль не может буть пустым");
            return;
        }

        v.setEnabled(false);
        progressBarBetnbedLogin.setVisibility(View.VISIBLE);

        new Thread(new Runnable() {
            @Override
            public void run() {
                NetworkService.requestController.sendRequest(RequestConstructor.CLogin(userLogin, userPassword), "SLogin", new SocketInterface() {
                    @Override
                    public void onSuccess(String request) {

                        try {
                            JSONArray jsonArray = new JSONObject(request).getJSONArray("data");

                            if (jsonArray.getInt(4) == 1) {
                                handlerSetButtonEnable.sendEmptyMessage(103);

                                NetworkService.requestController.sendRequest(RequestConstructor.CLogout(), "SLogout", new SocketInterface() {
                                    @Override
                                    public void onSuccess(String request) {

                                    }

                                    @Override
                                    public void onError(int error) {

                                    }
                                });
                            } else login(userLogin, userPassword, jsonArray.getInt(0), jsonArray.getInt(1), jsonArray.getInt(2), jsonArray.getInt(3));
                        } catch (JSONException e) {
                            Log.v("TAG", e.toString());
                        }
                    }

                    @Override
                    public void onError(int error) {
                        if (error == 9) {
                            NetworkService.requestController.sendRequest(RequestConstructor.CLogout(), "SLogout", new SocketInterface() {
                                @Override
                                public void onSuccess(String request) {
                                    NetworkService.requestController.sendRequest(RequestConstructor.CLogin(userLogin, userPassword), "CLogin", new SocketInterface() {
                                        @Override
                                        public void onSuccess(String request) {
                                            try {
                                                JSONArray jsonArray = new JSONObject(request).getJSONArray("data");
                                                login(userLogin, userPassword, jsonArray.getInt(0), jsonArray.getInt(1), jsonArray.getInt(2), jsonArray.getInt(3));

                                            } catch (JSONException e) {
                                                Log.v("TAG", e.toString());
                                            }
                                        }

                                        @Override
                                        public void onError(int error) {
                                            handlerSetButtonEnable.sendEmptyMessage(error);
                                        }
                                    });
                                }

                                @Override
                                public void onError(int error) {
                                }
                            });
                        } else handlerSetButtonEnable.sendEmptyMessage(error);
                    }
                });
            }
        }).start();
    }

    @Override public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_hotel);

        ButterKnife.bind(this);
        initView();
    }

    @Override protected void initView() {
        super.initView();
        progressBarBetnbedLogin.getIndeterminateDrawable().setColorFilter(Color.parseColor("#00a0f0"), PorterDuff.Mode.MULTIPLY);

        forgetPassword.setLinkTextColor(Color.parseColor("#2cc990"));
    }

    static public final String SUCCES_HOTEL_LOGIN = "SUCCES_HOTEL_LOGIN";

    protected void login(String userLogin, String userPassword, int idAccount, int touristId, int hostId, int hotelId) {
        super.login(userLogin, userPassword, idAccount, touristId, hostId, hotelId);
        SettingsController.setIsHotel(true);
        Heap.initHotelProfile(3, this);
        NetworkService.requestController.setState(RequestSender.WITH_LOGIN_HOTEL);

        Intent intent = new Intent(this, NavigationActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.setAction(SUCCES_HOTEL_LOGIN);
        startActivity(intent);

    }

}