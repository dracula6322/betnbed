package com.betnbed.activity.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;

import com.betnbed.R;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginStartHostActivity extends AppCompatActivity {

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    @OnClick(R.id.mainLoginTourist) void tourist() {
        Intent intent = new Intent(this, LoginTourisActivity.class);
        intent.putExtra("type", 2);
        startActivity(intent);
        //LoginTouristFragment fragment = LoginTouristFragment.newInstance(2);
        //fragmentManager.beginTransaction().hide(this).addToBackStack("").add(R.id.frame, fragment).commit();
    }

    @OnClick(R.id.mainLoginHotel) void hotel() {
        Intent intent = new Intent(this, LoginHotelActivity.class);
        startActivity(intent);
        //LoginHotelFragment fragment = new LoginHotelFragment();
        //fragmentManager.beginTransaction().hide(this).addToBackStack("").add(R.id.frame, fragment).commit();
    }

    /*public static LoginStartHostActivity getInstance(int gsmError) {

        LoginStartHostActivity fragment = new LoginStartHostActivity();
        Bundle bundle = new Bundle();
        bundle.putInt(ARGUMENT_GSM_ERROR, gsmError);
        fragment.setArguments(bundle);
        return fragment;
    }*/

    @Override public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_start_main);

        ButterKnife.bind(this);
    }
}