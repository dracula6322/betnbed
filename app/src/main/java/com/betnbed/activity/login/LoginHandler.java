package com.betnbed.activity.login;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.support.design.widget.TextInputLayout;
import android.view.View;
import android.widget.Toast;

import com.betnbed.R;

import java.lang.ref.WeakReference;

public class LoginHandler extends Handler {

    private final WeakReference<View> weakReferenceProgressBar;
    private final WeakReference<View> weakReferenceButton;
    private final WeakReference<TextInputLayout> weakReferenceFirstInputLayout;
    private final WeakReference<TextInputLayout> weakReferenceSecondInputLayout;
    private final Context context;


    public LoginHandler(Context context, View progressBar, View button, TextInputLayout firstInputLayout, TextInputLayout secondInputLayout) {
        this.context = context;
        this.weakReferenceProgressBar = new WeakReference<>(progressBar);
        this.weakReferenceButton = new WeakReference<>(button);
        this.weakReferenceFirstInputLayout = new WeakReference<>(firstInputLayout);
        this.weakReferenceSecondInputLayout = new WeakReference<>(secondInputLayout);

    }

    @Override
    public void handleMessage(Message msg) {
        View progressBar = weakReferenceProgressBar.get();
        if (progressBar != null)
            progressBar.setVisibility(View.GONE);

        View button = weakReferenceButton.get();
        if (button != null)
            button.setEnabled(true);

        TextInputLayout firstTextInputLayout = weakReferenceFirstInputLayout.get();
        TextInputLayout secondTextInputLayout = weakReferenceSecondInputLayout.get();
        if (firstTextInputLayout != null && secondTextInputLayout != null)
            switch (msg.what) {
                case 8:
                    firstTextInputLayout.setError("Такой логин уже есть в базе");
                    break;
                case 10:
                    firstTextInputLayout.setError("Такой пользователь не зарегистрирован");
                    break;
                case 11:
                    secondTextInputLayout.setError("Неверный пароль");
                    break;
                case 33:
                    firstTextInputLayout.setError("Не верный формат " + context.getString(R.string.email) + " адреса");
                    break;
                case 57:
                    firstTextInputLayout.setError("Ваша учетная запись была заблокирована");
                    break;
                case 101:
                    Toast.makeText(context, context.getString(R.string.errorIsEthernetNotAvailable), Toast.LENGTH_SHORT).show();
                    break;
                case 102:
                    firstTextInputLayout.setError("Вы зарегистрированы как отель, пожалуйста, перейдите в окно входа отеля");
                    break;
                case 103:
                    firstTextInputLayout.setError("Вы зарегистрированы как турист, пожалуйста, перейдите в окно входа туриста");
                    break;
            }
    }
}
