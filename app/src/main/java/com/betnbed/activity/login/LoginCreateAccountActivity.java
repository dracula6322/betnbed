package com.betnbed.activity.login;


import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.betnbed.R;
import com.betnbed.RequestConstructor;
import com.betnbed.Utils;
import com.betnbed.interfaces.SocketInterface;
import com.betnbed.service.NetworkService;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;

public class LoginCreateAccountActivity extends AppCompatActivity {

    static public final int TOURIST_TYPE = 1;
    static public final int HOTEL_TYPE = 2;
    static public final int HOST_TYPE = 3;

    static public final int AGREEMENT_TYPE = 1;

    private LoginHandler handler;
    private int type;

    @BindView(R.id.createAccount) Button createAccount;
    @BindView(R.id.agree) CheckBox agree;
    @BindView(R.id.progress_bar) ProgressBar progressBar;
    @BindView(R.id.touristEmail) TextInputLayout login;
    @BindView(R.id.touristPassword) TextInputLayout password;
    @BindView(R.id.agreement) TextView agreement;

    @OnClick(R.id.agreement) void agreement() {
        Intent intent = new Intent(this, AgreementActivity.class);
        intent.putExtra("type", type);
        startActivityForResult(intent, AGREEMENT_TYPE);
        //AgreementFragment fragment = AgreementFragment.newInstance(AgreementFragment.AGREE_TYPE);
        //fragmentManager.beginTransaction().hide(this).addToBackStack("").add(R.id.frame, fragment).commit();
    }

    @OnClick(R.id.createAccount) void createAccount(View v) {
        final String userLogin = login.getEditText().getText().toString().trim();
        if (userLogin.isEmpty()) {
            login.setError("Email не может быть пустым");
            return;
        }

        final String userPassword = password.getEditText().getText().toString().trim();
        if (userPassword.isEmpty()) {
            password.setError("Пароль не может быть пустым");
            return;
        }

        if (Utils.checkEmail(userLogin)) {
            login.setError("Не верный формат " + getString(R.string.email) + " адреса");
            return;
        }

        v.setEnabled(false);
        progressBar.setVisibility(View.VISIBLE);


        new AsyncTask<Void, Void ,Integer>(){

            final Bundle[] bundle = new Bundle[1];
            final Integer[] result = new Integer[1];

            @Override protected Integer doInBackground(Void... params) {



                NetworkService.requestController.sendRequest(RequestConstructor.CReg(userLogin, userPassword, type), "SRegEmailSend", new SocketInterface() {
                    @Override
                    public void onSuccess(String request) {
                        bundle[0] = new Bundle();
                        bundle[0].putBoolean("isCreatedEmail", true);
                        bundle[0].putString("email", userLogin);
                        bundle[0].putString("password", userPassword);
                        result[0] = -1;
                    }

                    @Override
                    public void onError(int error) {
                        result[0] = error;

                    }
                });

                return result[0];
            }

            @Override protected void onPostExecute(Integer integer) {
                super.onPostExecute(integer);

                if(integer == -1){
                    Intent intent = new Intent();
                    intent.putExtra("isCreatedEmail", true);
                    intent.putExtra("email", userLogin);
                    intent.putExtra("password", userPassword);
                    setResult(RESULT_OK, intent);
                    finish();

                } else {
                    handler.sendEmptyMessage(integer);
                }

            }

        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

        new Thread(new Runnable() {
            @Override
            public void run() {

            }
        }).start();
    }

    @OnCheckedChanged(R.id.agree) void check(boolean enable) {
        createAccount.setEnabled(enable);
    }

    @Override public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_create_account);
        type = getIntent().getIntExtra("type", 0);

        ButterKnife.bind(this);
        initView();


        //fragmentManager = getFragmentManager();

        /*acceptAgreement = new AcceptAgreement() {
            @Override public void onAcceptAgreement(boolean result) {
                agree.setChecked(result);
            }
        };*/

        /*try {
            getResultFromAnotherFragment = (GetResultFromAnotherFragment) fragmentManager.findFragmentById(R.id.frame);
        } catch (ClassCastException e) {
            throw new ClassCastException(getFragmentManager().findFragmentByTag("create").toString() + " must implement SocketInterface");
        }*/

    }

    /*public static LoginCreateAccountActivity newInstance(int type) {
        LoginCreateAccountActivity fragment = new LoginCreateAccountActivity();
        Bundle args = new Bundle();
        args.putInt("type", type);
        fragment.setArguments(args);
        return fragment;
    }*/

    private void initView() {

        progressBar.getIndeterminateDrawable().setColorFilter(Color.parseColor("#00a0f0"), PorterDuff.Mode.MULTIPLY);

        if (login.getEditText() != null && password.getEditText() != null) {
            login.getEditText().addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    login.setError("");
                }

                @Override
                public void afterTextChanged(Editable editable) {
                }
            });

            password.getEditText().addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    password.setError("");
                }

                @Override
                public void afterTextChanged(Editable editable) {}
            });
        } else throw new RuntimeException("No view in edit text");

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
            agreement.setText(Html.fromHtml("Я согласен с уловиями " + "<a href=\"\">пользовательского соглашения</a> ", Html.FROM_HTML_MODE_LEGACY));
        else agreement.setText(Html.fromHtml("Я согласен с уловиями " + "<a href=\"\">пользовательского соглашения</a> "));
        agreement.setLinkTextColor(Color.parseColor("#fcb941"));

        handler = new LoginHandler(this, progressBar, createAccount, login, password);

    }

    @Override protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK){
            if(requestCode == AGREEMENT_TYPE)
                agree.setChecked(data.getBooleanExtra("agree", true));
        }
    }
}
