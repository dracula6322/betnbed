package com.betnbed.activity.login;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.betnbed.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class AgreementActivity extends AppCompatActivity {

    final public static int SHOW_TYPE = 0;
    final public static int AGREE_TYPE = 1;

    @BindView(R.id.more) View accept;

    @OnClick(R.id.more) void click() {
        Intent intent = new Intent();
        intent.putExtra("agree", true);
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agreement);

        ButterKnife.bind(this);
        initView();
    }

    private void initView() {
        accept.setVisibility(getIntent().getIntExtra("type", 0) == SHOW_TYPE ? View.GONE : View.VISIBLE);
    }
}
