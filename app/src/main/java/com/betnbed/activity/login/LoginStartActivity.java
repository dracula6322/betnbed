package com.betnbed.activity.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;

import com.betnbed.R;
import com.betnbed.activity.tutorial.TutorialFirstScreenActivity;
import com.betnbed.controller.SettingsController;
import com.betnbed.service.NetworkService;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginStartActivity extends AppCompatActivity {

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    @OnClick(R.id.mainLoginTourist) void tourist() {
        Intent intent = new Intent(this, LoginTourisActivity.class);
        intent.putExtra("type", 1);
        startActivity(intent);
    }

    @OnClick(R.id.mainLoginHotel) void hotel() {
        Intent intent = new Intent(this, LoginStartHostActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.help) void help() {
        showTutorialFirstScreenFragment();
    }

    /*public static LoginStartActivity getInstance(int gsmError) {

        LoginStartActivity fragment = new LoginStartActivity();
        Bundle bundle = new Bundle();
        bundle.putInt(ARGUMENT_GSM_ERROR, gsmError);
        fragment.setArguments(bundle);
        return fragment;
    }*/

    @Override public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_main);

        if (!SettingsController.isFirstLaunch()) {
            showTutorialFirstScreenFragment();
        }

        //if (getArguments().getInt(ARGUMENT_GSM_ERROR, 0) != 0)
            //Utils.showUpdateGMSDialog(getActivity());

        ButterKnife.bind(this);

    }

    private void showTutorialFirstScreenFragment(){
        Intent intent = new Intent(this, TutorialFirstScreenActivity.class);
        startActivity(intent);
        //TutorialFirstScreenFragment fragment = TutorialFirstScreenFragment.newInstance();
        //fragmentManager.beginTransaction().hide(this).addToBackStack("").add(R.id.frame, fragment).commit();
    }

    @Override public void onBackPressed() {
        Intent intent = new Intent(this, NetworkService.class);
        stopService(intent);
        super.onBackPressed();
        finish();
    }
}