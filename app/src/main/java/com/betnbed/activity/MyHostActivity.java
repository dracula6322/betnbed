package com.betnbed.activity;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import com.betnbed.Heap;
import com.betnbed.R;
import com.betnbed.SC;
import com.betnbed.adapters.MyGuestListRecyclerViewAdapter;
import com.betnbed.adapters.ViewPagerAdapter;
import com.betnbed.controller.SettingsController;
import com.betnbed.customView.BetnbedActivity;
import com.betnbed.fragment.MyGuestFragment;
import com.betnbed.interfaces.MovingToTab;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MyHostActivity extends BetnbedActivity {

    static public final String tagOpenPage = "com.betnbed.MY_HOST_ACTIVITY_OPEN_PAGE";
    static public final String tagRefreshFragment = "com.betnbed.MY_HOST_ACTIVITY_REFRESH_FRAGMENT";

    public final SimpleDateFormat filterDataFormat = new SimpleDateFormat("dd MMM yyyy_HH:mm", Locale.getDefault());

    private MyGuestFragment[] fragments;

    private MyHostActivityOpenPageBroadcastReceiver myHostActivityOpenPageBroadcastReceiver;
    private MyHostActivityRefreshFragment myHostActivityRefreshFragment;
    private int moneyType;
    private int currentTab;

    private boolean isCard = true;
    private boolean isMoney = true;

    private EditText additionalInfo;

    TextView inDate;
    TextView inTime;
    TextView outDate;
    TextView outTime;
    @BindView(R.id.fab) FloatingActionButton fab;
    @BindView(R.id.viewpager) ViewPager viewPager;
    @BindView(R.id.tabs) TabLayout tabLayout;

    @OnClick(R.id.fab) void showDialog() {
        showFilterDialog();
    }



    @Override public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_guest);
        /*if (getArguments().getBoolean("fromHotel", false) && !SettingsController.isFirstLaunchHotel())
            getFragmentManager().beginTransaction().hide(this).addToBackStack("").add(R.id.frame, TutorialSettingsFragment.newInstance(5)).commit();

        if (getArguments().getInt(ARGUMENT_GSM_ERROR, 0) != 0)
            Utils.showUpdateGMSDialog(getActivity());*/

        ButterKnife.bind(this);
        initNavigationView();
        initToolbar("Betnbed");
        initView();

    }

    @Override public void onResume() {
        super.onResume();

        myHostActivityOpenPageBroadcastReceiver = new MyHostActivityOpenPageBroadcastReceiver();
        LocalBroadcastManager.getInstance(this).registerReceiver(myHostActivityOpenPageBroadcastReceiver, new IntentFilter(tagOpenPage));

        myHostActivityRefreshFragment = new MyHostActivityRefreshFragment();
        LocalBroadcastManager.getInstance(this).registerReceiver(myHostActivityRefreshFragment, new IntentFilter(tagRefreshFragment));

        new Thread(new Runnable() {
            @Override
            public void run() {
                Heap.accountHotel.requestHostController.getActiveRequest();
            }
        }).start();

        Heap.accountHotel.requestHostController.disableFilter();

        moneyType = 0;

        fab.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor("#00a0f0")));
    }

    @Override public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(myHostActivityOpenPageBroadcastReceiver);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(myHostActivityRefreshFragment);
    }

    @Override protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        viewPager.setCurrentItem(intent.getIntExtra("page", 0));
    }

    private void initView() {
        initTabLayout();
    }

    private Dialog getDialog(int id) {
        switch (id) {
            case 1:
                return new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        Calendar calendar = new GregorianCalendar(year, monthOfYear, dayOfMonth);
                        inDate.setText(SC.dfShort.format(calendar.getTime()));
                        if (outDate.getText().equals("")) outDate.setText(SC.dfShort.format(calendar.getTime()));
                        if (inTime.getText().equals("")) inTime.setText("00:00");
                        if (outTime.getText().equals("")) outTime.setText("23:59");
                    }
                }, Calendar.getInstance().get(Calendar.YEAR), Calendar.getInstance().get(Calendar.MONTH), Calendar.getInstance().get(Calendar.DAY_OF_MONTH));
            case 2:
                return new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        Calendar calendar = new GregorianCalendar(year, monthOfYear, dayOfMonth);
                        outDate.setText(SC.dfShort.format(calendar.getTime()));
                        if (outTime.getText().equals("")) outTime.setText("23:59");
                    }
                }, Calendar.getInstance().get(Calendar.YEAR), Calendar.getInstance().get(Calendar.MONTH), Calendar.getInstance().get(Calendar.DAY_OF_MONTH));
            case 3:
                return new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        inTime.setText(hourOfDay + ":" + (minute < 10 ? '0' + String.valueOf(minute) : minute));
                    }
                }, Calendar.getInstance().get(Calendar.HOUR_OF_DAY), Calendar.getInstance().get(Calendar.MINUTE), true);
            case 4:
                return new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        outTime.setText(hourOfDay + ":" + (minute < 10 ? '0' + String.valueOf(minute) : minute));
                    }
                }, Calendar.getInstance().get(Calendar.HOUR_OF_DAY), Calendar.getInstance().get(Calendar.MINUTE), true);
            default:
                throw new StackOverflowError();
        }
    }


    private void initTabLayout() {

        fragments = new MyGuestFragment[3];
        for (int i = 0; i < fragments.length; i++)
            fragments[i] = MyGuestFragment.newInstance(i);

        MovingToTab movingToTab = new MovingToTab() {
            @Override public void moveTo(int position) {
                viewPager.setCurrentItem(position);
            }
        };
        fragments[0].setMovingToTab(movingToTab);
        fragments[1].setMovingToTab(movingToTab);
        fragments[2].setMovingToTab(movingToTab);

        fragments[0].adapter = new MyGuestListRecyclerViewAdapter(fragments[0].getMovingToTab());
        fragments[0].adapter.setContents(Heap.accountHotel.getRequestHostController().filtered);
        fragments[0].adapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {

            @Override
            public void onItemRangeChanged(int positionStart, int itemCount) {
                super.onItemRangeChanged(positionStart, itemCount);
                if (currentTab == 0)
                    fab.setVisibility(Heap.accountHotel.requestHostController.withOutInvite.size() == 0 ? View.GONE : View.VISIBLE);
                fragments[0].emptyLayout.setVisibility(itemCount == 0 ? View.VISIBLE : View.GONE);
                fragments[0].adapter.notifyDataSetChanged();
            }
        });

        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(fragments[0], "Все гости");
        adapter.addFragment(fragments[1], "Приглашены");
        adapter.addFragment(fragments[2], "Мои гости");

        viewPager.setAdapter(adapter);
        viewPager.setCurrentItem(getIntent().getIntExtra("page", 0));


        tabLayout.setupWithViewPager(viewPager);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                currentTab = tab.getPosition();
                viewPager.setCurrentItem(tab.getPosition());
                fab.setVisibility((tab.getPosition() == 0) && (Heap.accountHotel.requestHostController.withOutInvite.size() > 0) ? View.VISIBLE : View.GONE);


            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
    }

    private void showFilterDialog() {

        View v = getLayoutInflater().inflate(R.layout.fragment_myguest_setting, null);

        inDate = (TextView) v.findViewById(R.id.inDate);
        inTime = (TextView) v.findViewById(R.id.inTime);
        outDate = (TextView) v.findViewById(R.id.outDate);
        outTime = (TextView) v.findViewById(R.id.outTime);


        if (Heap.accountHotel.requestHostController.getArrival() == 0) {
            String split[] = filterDataFormat.format(Heap.accountHotel.requestHostController.getArrival() * 1000).split("_");
            inDate.setText(split[0]);
            inTime.setText(split[1]);

        } else {
            inDate.setText("");
            inTime.setText("");
        }

        if (Heap.accountHotel.requestHostController.getDeparture() == 0) {
            String split[] = filterDataFormat.format(Heap.accountHotel.requestHostController.getDeparture() * 1000).split("_");
            outDate.setText(split[0]);
            outTime.setText(split[1]);
        } else {
            outDate.setText("");
            outTime.setText("");
        }

        inDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getDialog(1).show();
            }
        });
        inTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getDialog(3).show();
            }
        });
        outDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getDialog(2).show();
            }
        });
        outTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getDialog(4).show();
            }
        });


        additionalInfo = (EditText) v.findViewById(R.id.myGuest_myHost_settings_additional_info);
        additionalInfo.setText(SettingsController.getAdditionalInfoWhenInviteTourist());


        final ViewSwitcher switcherCard = (ViewSwitcher) v.findViewById(R.id.cardSwitcher);
        switcherCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isCard = !isCard;
                switcherCard.showNext();
            }
        });

        final ViewSwitcher switcherMoney = (ViewSwitcher) v.findViewById(R.id.moneySwitcher);
        switcherMoney.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isMoney = !isMoney;
                switcherMoney.showNext();
            }
        });

        if (moneyType == 1) switcherMoney.showNext();
        if (moneyType == 2) switcherCard.showNext();

        AlertDialog.Builder b = new AlertDialog.Builder(this)
                .setView(v)
                .setTitle("Фильтр")
                .setPositiveButton("Применить", null)
                .setNeutralButton("Выйти", null)
                .setNegativeButton("Очистить", null);

        final AlertDialog dialog = b.create();
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog_) {
                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        SettingsController.setAdditionalInfoWhenInviteTourist(additionalInfo.getText().toString());

                        boolean error = false;

                        long arrivalTime = 0;
                        long departureTime = 0;
                        try {
                            arrivalTime = filterDataFormat.parse(inDate.getText().toString() + "_" + inTime.getText().toString()).getTime() / 1000;
                            departureTime = filterDataFormat.parse(outDate.getText().toString() + "_" + outTime.getText().toString()).getTime() / 1000;
                            if (departureTime <= arrivalTime) {
                                showMessage("Дата отъезда должна быть позже даты приезда");
                                error = true;
                            }

                        } catch (ParseException e) {
                            Log.v("TAG", e.toString());
                            error = true;
                        }
                        if (isCard && isMoney)
                            moneyType = 0;
                        if (isCard && !isMoney)
                            moneyType = 1;
                        if (!isCard && isMoney)
                            moneyType = 2;
                        if (!isCard && !isMoney) {
                            showMessage("Выберите тип оплаты");

                            error = true;
                        }


                        if (!error) {

                            Heap.accountHotel.requestHostController.applyFilter(arrivalTime, departureTime, moneyType);
                            fragments[0].adapter.notifyItemRangeChanged(0, Heap.accountHotel.requestHostController.filtered.size());
                            fragments[0].adapter.notifyDataSetChanged();
                            fab.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor("#fcb941")));
                            dialog.dismiss();
                        }

                    }
                });

                dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Heap.accountHotel.requestHostController.disableFilter();

                        additionalInfo.setText("");
                        SettingsController.setAdditionalInfoWhenInviteTourist(additionalInfo.getText().toString());

                        inDate.setHintTextColor(Color.parseColor("#00aff0"));
                        inTime.setHintTextColor(Color.parseColor("#00aff0"));
                        outDate.setHintTextColor(Color.parseColor("#00aff0"));
                        outTime.setHintTextColor(Color.parseColor("#00aff0"));

                        inDate.setText("");
                        inTime.setText("");
                        outDate.setText("");
                        outTime.setText("");

                        moneyType = 0;

                        fab.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor("#00a0f0")));

                        fragments[0].handler.sendEmptyMessage(0);
                    }
                });

                dialog.getButton(AlertDialog.BUTTON_NEUTRAL).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
            }
        });
        dialog.show();
    }

    private void showMessage(String text){
        Toast.makeText(this, text, Toast.LENGTH_LONG).show();
    }


    public class MyHostActivityOpenPageBroadcastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            viewPager.setCurrentItem(intent.getIntExtra("OPEN_PAGE", 0));
        }
    }

    public class MyHostActivityRefreshFragment extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (fragments[intent.getIntExtra("FRAGMENT", 0)].handler != null) {
                fragments[intent.getIntExtra("FRAGMENT", 0)].handler.sendEmptyMessage(0);
            }
        }
    }

    /*@Override public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN) {

            Request.Builder request = new Request.Builder()
                    .catsAndDogs(null)
                    .senderId(22)
                    .requestId(33)
                    .place("23")
                    .arrival(1491970600)
                    .departure(1493674160)
                    .money(22)
                    .currency("RUB")
                    .people(1)
                    .special("")
                    .mask(64)
                    .inviteId(1)
                    .special("допольнитьельняа фывфывфыв фывыфвфв фв фыв фыв фыв фыв фы")
                    .user(new User.Builder().firstName("1").lastName("21").photo("").build());


            Heap.accountHotel.getRequestHostController().addRequest(request.build());

            Heap.accountHotel.getRequestHostController().addRequest(request.mask(4).build());

            Heap.accountHotel.getRequestHostController().addRequest(request.mask(32).build());

            Heap.accountHotel.getRequestHostController().GNewRequest(request.mask(8).build());

            LocalBroadcastManager.getInstance(this).sendBroadcast(new Intent(MyHostActivity.tagRefreshFragment).putExtra("FRAGMENT", 0));
            LocalBroadcastManager.getInstance(this).sendBroadcast(new Intent(MyHostActivity.tagRefreshFragment).putExtra("FRAGMENT", 1));
            LocalBroadcastManager.getInstance(this).sendBroadcast(new Intent(MyHostActivity.tagRefreshFragment).putExtra("FRAGMENT", 2));

        }
        return super.onKeyDown(keyCode, event);
    }*/

}
