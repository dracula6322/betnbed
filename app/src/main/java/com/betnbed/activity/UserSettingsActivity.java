package com.betnbed.activity;

import android.os.Bundle;

import com.betnbed.R;
import com.betnbed.customView.ToolbarActivity;
import com.betnbed.fragment.SettingsFragment;

import butterknife.ButterKnife;

public class UserSettingsActivity extends ToolbarActivity {

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        ButterKnife.bind(this);

        getSupportFragmentManager().beginTransaction().replace(R.id.content, new SettingsFragment()).commit();
        initToolbar("Настройки");

    }
}
