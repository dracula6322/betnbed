package com.betnbed.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.betnbed.R;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoadApplicationActivity extends AppCompatActivity {

    @OnClick(R.id.button) void onClick(){
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + getPackageName())));
    }

    @Override protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_load_application);

        ButterKnife.bind(this);


    }
}
