package com.betnbed.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.betnbed.R;
import com.betnbed.customView.ToolbarActivity;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import butterknife.ButterKnife;

public class ShowHotelOrHostOnMapActivity extends ToolbarActivity {

    LatLng latLng;
    String title;
    String address;

    @Override public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_hotel_or_host_on_map);

        latLng = getIntent().getParcelableExtra("latLng");
        title = getIntent().getStringExtra("title");
        address = getIntent().getStringExtra("address");

        ButterKnife.bind(this);

        initToolbar("Карта");
        initView();

    }

    @Override public void onResume() {
        super.onResume();
        initView();
    }

    private void initView() {
        SupportMapFragment mapFragment = new SupportMapFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.map_fragment, mapFragment).commit();
        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {

                googleMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
                googleMap.getUiSettings().setMapToolbarEnabled(false);

                final Marker mark = googleMap.addMarker(new MarkerOptions().position(latLng).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_place_black_40_56)));
                googleMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
                    @Override
                    public View getInfoWindow(Marker marker) {
                        View view = getLayoutInflater().inflate(R.layout.marker_hotel_window, null);

                        ((TextView) view.findViewById(R.id.title)).setText(title);
                        ((TextView) view.findViewById(R.id.address)).setText(address);

                        return view;
                    }

                    @Override
                    public View getInfoContents(Marker marker) {
                        return null;
                    }
                });

                mark.showInfoWindow();
                googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 17));
            }
        });
    }

}
