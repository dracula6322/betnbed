package com.betnbed.activity.tutorial;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;

import com.betnbed.R;
import com.betnbed.customView.ToolbarActivity;
import com.betnbed.fragment.TutorialFirstFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TutorialFirstScreenActivity extends ToolbarActivity {

    @BindView(R.id.bottom_pages) ViewGroup bottomPages;
    @BindView(R.id.button) View button;
    @BindView(R.id.viewpager) ViewPager viewPager;

    @OnClick(R.id.button) void closeFragment(){
        onBackPressed();
    }

    @Override protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tutorial_first_screen);

        ButterKnife.bind(this);
        initToolbar("Добро пожаловать");
        initView();

    }

    private void initView() {

        final FirstTutorialAdapter adapter = new FirstTutorialAdapter(getSupportFragmentManager());
        TutorialFirstFragment firstScreen = TutorialFirstFragment.newInstance(1);
        TutorialFirstFragment secondScreen = TutorialFirstFragment.newInstance(2);
        TutorialFirstFragment thirdScreen = TutorialFirstFragment.newInstance(3);

        adapter.addFragment(firstScreen);
        adapter.addFragment(secondScreen);
        adapter.addFragment(thirdScreen);

        viewPager.setAdapter(adapter);
    }

    public class FirstTutorialAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();

        private FirstTutorialAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        private void addFragment(Fragment fragment) {
            mFragmentList.add(fragment);
        }


        @Override
        public void setPrimaryItem(ViewGroup container, int position, Object object) {
            super.setPrimaryItem(container, position, object);
            int count = bottomPages.getChildCount();
            for (int a = 0; a < count; a++) {
                View child = bottomPages.getChildAt(a);
                if (a == position) {
                    child.setBackgroundColor(getResources().getColor(R.color.betnbed_blue));
                } else {
                    child.setBackgroundColor(0xffbbbbbb);
                }
            }

            if (position == count - 1 && button.getVisibility() == View.GONE)
                button.setVisibility(View.VISIBLE);

        }
    }

}
