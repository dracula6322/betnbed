package com.betnbed.activity.tutorial;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;

import com.betnbed.R;
import com.betnbed.customView.ToolbarActivity;
import com.betnbed.fragment.TutorialFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TutorialSettingsActivity extends ToolbarActivity {

    @BindView(R.id.bottom_pages) ViewGroup bottomPages;
    @BindView(R.id.viewpager) ViewPager viewPager;
    private int delta;
    private int mass[];

    /*public static TutorialSettingsActivity newInstance(int delta) {
        TutorialSettingsActivity fragment = new TutorialSettingsActivity();
        Bundle bundle = new Bundle();
        bundle.putInt("delta", delta);
        fragment.setArguments(bundle);
        return fragment;
    }*/

    @Override public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tutorial_tourist_acitivty);
        delta = getIntent().getIntExtra("delta", 0); //5
        if (delta == 0)
            mass = new int[]{3, 5, 1, 2, 4};
        else mass = new int[]{11, 12, 13, 14, 15};

        ButterKnife.bind(this);

        initToolbar(delta == 0 ? "Как путешествовать" : "Как принимать гостей");
        initTabLayout();

    }

    private void initTabLayout() {


        FirstTutorialAdapter adapter = new FirstTutorialAdapter(getSupportFragmentManager());
        TutorialFragment firstScreen = new TutorialFragment();
        firstScreen.setType(mass[0], delta);
        TutorialFragment secondScreen = new TutorialFragment();
        secondScreen.setType(mass[1], delta + 1);
        TutorialFragment thirdScreen = new TutorialFragment();
        thirdScreen.setType(mass[2], delta + 2);
        TutorialFragment fourScreen = new TutorialFragment();
        fourScreen.setType(mass[3], delta + 3);
        TutorialFragment fiveScreen = new TutorialFragment();
        fiveScreen.setType(mass[4], delta + 4);

        adapter.addFragment(firstScreen);
        adapter.addFragment(secondScreen);
        adapter.addFragment(thirdScreen);
        adapter.addFragment(fourScreen);
        adapter.addFragment(fiveScreen);

        viewPager.setAdapter(adapter);
    }

    public class FirstTutorialAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();

        public FirstTutorialAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment) {
            mFragmentList.add(fragment);
        }


        @Override
        public void setPrimaryItem(ViewGroup container, int position, Object object) {
            super.setPrimaryItem(container, position, object);
            int count = bottomPages.getChildCount();
            for (int a = 0; a < count; a++) {
                View child = bottomPages.getChildAt(a);
                if (a == position) {
                    child.setBackgroundColor(getResources().getColor(R.color.betnbed_blue));
                } else {
                    child.setBackgroundColor(0xffbbbbbb);
                }
            }

        }
    }

}
