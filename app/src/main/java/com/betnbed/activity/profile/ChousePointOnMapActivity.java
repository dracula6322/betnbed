package com.betnbed.activity.profile;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.view.View;

import com.betnbed.R;
import com.betnbed.customView.ToolbarActivity;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ChousePointOnMapActivity extends ToolbarActivity {

    Marker marker;
    //ChouseMapPoint chouseMapPoint;


    @BindView(R.id.fab) FloatingActionButton fab;

    @Override protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chouse_point_on_map);
        ButterKnife.bind(this);

        initToolbar("Отметьте расположение отеля на карте");
        initView();

    }

    private void initView() {


        SupportMapFragment mapFragment = new SupportMapFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.mapp, mapFragment).commit();

        mapFragment.getMapAsync(new OnMapReadyCallback() {

            @Override
            public void onMapReady(final GoogleMap googleMap) {

                if (getIntent().getExtras() != null)
                    if (getIntent().getExtras().containsKey("latLng")) {
                        LatLng latLng = getIntent().getParcelableExtra("latLng");
                        marker = googleMap.addMarker(new MarkerOptions().position(latLng).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_place_black_40_56)));

                        float zoom = 0;
                        switch (getIntent().getIntExtra("type", 0)) {

                            case 1:
                                zoom = 17;
                                break;
                            case 2:
                                zoom = 13;
                                break;
                            case 3:
                                zoom = 8;
                                break;

                        }

                        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom));
                        fab.setVisibility(View.VISIBLE);

                    }
                googleMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
                googleMap.getUiSettings().setMapToolbarEnabled(false);
                googleMap.getUiSettings().setCompassEnabled(false);


                googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                    @Override
                    public void onMapClick(LatLng latLng) {
                        if (marker != null) marker.remove();

                        fab.setVisibility(View.VISIBLE);

                        marker = googleMap.addMarker(new MarkerOptions().position(latLng).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_place_black_40_56)));
                    }
                });

            }
        });

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.putExtra("latLng", marker.getPosition());
                //chouseMapPoint.onChouse(marker.getPosition());
                setResult(RESULT_OK, intent);
                finish();
            }
        });
    }

    /*public void setChouseMapPoint(ChouseMapPoint chouseMapPoint) {
        this.chouseMapPoint = chouseMapPoint;
    }*/
}
