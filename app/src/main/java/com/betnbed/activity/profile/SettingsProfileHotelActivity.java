package com.betnbed.activity.profile;

import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.betnbed.Heap;
import com.betnbed.R;
import com.betnbed.RequestConstructor;
import com.betnbed.adapters.ViewPagerAdapter;
import com.betnbed.controller.SettingsController;
import com.betnbed.customView.ToolbarActivity;
import com.betnbed.fragment.SettingsProfileEditText;
import com.betnbed.fragment.SettingsProfileHotelProfileSomeEditText;
import com.betnbed.fragment.SettingsProfileImageRequirements;
import com.betnbed.interfaces.SocketInterface;
import com.betnbed.service.NetworkService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SettingsProfileHotelActivity extends ToolbarActivity {

    @BindView(R.id.viewpager) ViewPager viewPager;
    @BindView(R.id.save) View save;
    @BindView(R.id.tabs) TabLayout tabLayout;

    @Override public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings_profile_hotel);

        ButterKnife.bind(this);

        initToolbar("Контакты и реквизиты");
        initView();
    }

    private void initView() {


        final ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        final SettingsProfileHotelProfileSomeEditText userInformation = new SettingsProfileHotelProfileSomeEditText();
        final SettingsProfileImageRequirements settingsProfileImageRequirements = new SettingsProfileImageRequirements();
        final SettingsProfileEditText first = SettingsProfileEditText.newInstance(11);
        final SettingsProfileEditText second = SettingsProfileEditText.newInstance(12);
        final SettingsProfileEditText third = SettingsProfileEditText.newInstance(13);
        final SettingsProfileEditText four = SettingsProfileEditText.newInstance(14);

        final String[] tabMass = {"Контакты и реквизиты", "Настройка услуг", "Отель и территория", "Окрестности", "Описание услуг", "Интересные факты"};

        adapter.addFragment(userInformation, "");
        adapter.addFragment(settingsProfileImageRequirements, "");
        adapter.addFragment(first, "");
        adapter.addFragment(second, "");
        adapter.addFragment(third, "");
        adapter.addFragment(four, "");
        viewPager.setAdapter(adapter);

        tabLayout.setBackgroundColor(Color.parseColor("#ffffffff"));
        tabLayout.setupWithViewPager(viewPager);

        int iconMass[] = {R.drawable.png_user_agreement_orange, R.drawable.png_settings_orange, R.drawable.png_hotel_and_territory, R.drawable.png_near_place, R.drawable.png_service_orange, R.drawable.png_interesting_fact_about_hotel};

        for (int i = 0; i < tabLayout.getTabCount(); i++)
            tabLayout.getTabAt(i).setIcon(iconMass[i]);


        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                viewPager.setCurrentItem(tab.getPosition());
                toolbar.setTitle(tabMass[tab.getPosition()]);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    JSONObject saveData = new JSONObject();

                    JSONObject data = userInformation.getSaveRequestSetting();
                    if (data.has("region")) saveData.put("region", data.getString("region"));
                    if (data.has("city")) saveData.put("city", data.getString("city"));
                    if (data.has("contacts")) saveData.put("contacts", data.getString("contacts"));
                    if (data.has("phone")) saveData.put("phone", data.getString("phone"));
                    if (data.has("street")) saveData.put("street", data.getString("street"));
                    if (data.has("house_number")) saveData.put("house_number", data.getString("house_number"));
                    if (data.has("title")) saveData.put("title", data.getString("title"));
                    if (data.has("vat")) saveData.put("vat", data.getString("vat"));
                    if (data.has("email")) saveData.put("email", data.getString("email"));
                    if (data.has("latitude")) saveData.put("latitude", data.getString("latitude"));
                    if (data.has("longitude")) saveData.put("longitude", data.getString("longitude"));

                    data = settingsProfileImageRequirements.getContent();
                    if (data.has("type")) saveData.put("type", data.getInt("type"));
                    if (data.has("internet")) saveData.put("internet", data.get("internet"));
                    if (data.has("breakfast")) saveData.put("breakfast", data.get("breakfast"));
                    if (data.has("parking")) saveData.put("parking", data.get("parking"));
                    if (data.has("media")) saveData.put("media", data.get("media"));
                    if (data.has("shower")) saveData.put("shower", data.get("shower"));
                    if (data.has("kitchen")) saveData.put("kitchen", data.get("kitchen"));
                    if (data.has("pets")) saveData.put("pets", data.get("pets"));
                    if (data.has("services")) saveData.put("services", data.get("services"));
                    if (data.has("money")) saveData.put("money", data.get("money"));


                    for (int i = 0; i < 4; i++) {
                        switch (i) {
                            case 0:
                                data = first.getContent();
                                break;
                            case 1:
                                data = second.getContent();
                                break;
                            case 2:
                                data = third.getContent();
                                break;
                            case 3:
                                data = first.getContent();
                                break;
                            default:
                                throw new RuntimeException("Type error");
                        }

                        if (data.has("interests")) saveData.put("interests", data.getString("interests"));
                        if (data.has("funny")) saveData.put("funny", data.getString("funny"));
                        if (data.has("skills")) saveData.put("skills", data.getString("skills"));
                        if (data.has("territory")) saveData.put("territory", data.getString("territory"));
                        if (data.has("neighborhood")) saveData.put("neighborhood", data.getString("neighborhood"));
                        if (data.has("functions")) saveData.put("functions", data.getString("functions"));
                        if (data.has("interesting")) saveData.put("interesting", data.getString("interesting"));

                    }

                    SocketInterface socketInterface = new SocketInterface() {
                        @Override
                        public void onSuccess(String request) {

                            try {
                                JSONArray jsonArray = new JSONObject(request).getJSONArray("data");
                                if (jsonArray.getInt(0) == 1) return;
                                JSONObject data = jsonArray.getJSONObject(2);

                                if (data.has("approve")) Heap.accountHotel.setApprove(data.getInt("approve"));

                                if (data.has("region")) Heap.accountHotel.setRegion(data.getString("region"));
                                if (data.has("city")) Heap.accountHotel.setCity(data.getString("city"));
                                if (data.has("contacts")) Heap.accountHotel.setContacts(data.getString("contacts"));
                                if (data.has("phone")) Heap.accountHotel.setPhone(data.getString("phone"));
                                if (data.has("street")) Heap.accountHotel.setStreet(data.getString("street"));
                                if (data.has("house_number")) Heap.accountHotel.setHouse_number(data.getString("house_number"));
                                if (data.has("title")) Heap.accountHotel.setTitle(data.getString("title"));

                                if (data.has("vat")) Heap.accountHotel.setVat(data.getString("vat"));
                                if (data.has("email")) Heap.accountHotel.setEmail(data.getString("email"));

                                if (data.has("latitude") && data.has("longitude"))
                                    Heap.accountHotel.setLatLng(data.getString("latitude"), data.getString("longitude"));

                                if (data.has("territory")) Heap.accountHotel.setFirstLongDesc(data.getString("territory"));
                                if (data.has("neighborhood")) Heap.accountHotel.setSecondLongDesc(data.getString("neighborhood"));
                                if (data.has("functions")) Heap.accountHotel.setThirdLongDesc(data.getString("functions"));
                                if (data.has("interesting")) Heap.accountHotel.setFourLongDesc(data.getString("interesting"));

                                if (data.has("type")) Heap.accountHotel.setType(data.getInt("type"));
                                if (data.has("internet")) Heap.accountHotel.setInternet(data.getInt("internet"));
                                if (data.has("breakfast")) Heap.accountHotel.setBreakfast(data.getInt("breakfast"));
                                if (data.has("parking")) Heap.accountHotel.setParking(data.getInt("parking"));
                                if (data.has("media")) Heap.accountHotel.setMedia(data.getInt("media"));
                                if (data.has("shower")) Heap.accountHotel.setShower(data.getInt("shower"));
                                if (data.has("kitchen")) Heap.accountHotel.setKitchen(data.getInt("kitchen"));
                                if (data.has("pets")) Heap.accountHotel.setPets(data.getInt("pets"));
                                if (data.has("services")) Heap.accountHotel.setServices(data.getInt("services"));
                                if (data.has("money")) Heap.accountHotel.setMoney(data.getInt("money"));

                                Heap.accountHotel.requestHostController.setIsUpdateDataBase(false);
                                Toast.makeText(SettingsProfileHotelActivity.this, "Профиль обновлен", Toast.LENGTH_SHORT).show();

                                if (SettingsController.isHotel() && Heap.accountHotel.getApprove() == 0) {

                                    String error = "";
                                    if (Heap.accountHotel.getStreet().isEmpty())
                                        error += "\n - Улица";
                                    if (Heap.accountHotel.getHouse_number().isEmpty())
                                        error += "\n - Номер дома";
                                    if (Heap.accountHotel.getTitle().isEmpty())
                                        error += "\n - Название отеля";
                                    if (Heap.accountHotel.getContacts().isEmpty())
                                        error += "\n - Контактное лицо";
                                    if (Heap.accountHotel.getPhone().isEmpty())
                                        error += "\n - Телефон";
                                    if (Heap.accountHotel.getVat().isEmpty())
                                        error += "\n - ИНН";
                                    if (Heap.accountHotel.getRegion().isEmpty() && Heap.accountHotel.getCity().isEmpty())
                                        error += "\n - Город и/или Регион";

                                    if (error.isEmpty())
                                        return;

                                    AlertDialog.Builder builder = new AlertDialog.Builder(SettingsProfileHotelActivity.this);
                                    builder.setTitle("Уведомление")
                                            .setIcon(R.drawable.png_hotel_blue)
                                            .setMessage("Для подтверждения Вашего профиля необходимо указать следующую информацию:\n" + error)
                                            .setCancelable(true)
                                            .setPositiveButton("Ок",
                                                    new DialogInterface.OnClickListener() {
                                                        public void onClick(DialogInterface dialog, int id) {
                                                            dialog.cancel();
                                                        }
                                                    });
                                    builder.create().show();
                                }

                            } catch (JSONException e) {
                                Log.v("TAG", "We are here = " + e.toString());
                            }


                        }

                        @Override
                        public void onError(int error) {

                        }
                    };

                    if (saveData.length() > 0)
                        if (SettingsController.isHotel())
                            NetworkService.requestController.sendRequest(RequestConstructor.CChangeHotel(saveData), "SProfileChanged", socketInterface);
                        else NetworkService.requestController.sendRequest(RequestConstructor.CChangeHouse(saveData), "SProfileChanged", socketInterface);
                    else {

                        Toast.makeText(SettingsProfileHotelActivity.this, "Профиль обновлен", Toast.LENGTH_LONG).show();

                        if (SettingsController.isHotel() && Heap.accountHotel.getApprove() == 0) {

                            String error = "";
                            if (Heap.accountHotel.getStreet().isEmpty())
                                error += "\n - Улица";
                            if (Heap.accountHotel.getHouse_number().isEmpty())
                                error += "\n - Номер дома";
                            if (Heap.accountHotel.getTitle().isEmpty())
                                error += "\n - Название отеля";
                            if (Heap.accountHotel.getContacts().isEmpty())
                                error += "\n - Контактное лицо";
                            if (Heap.accountHotel.getPhone().isEmpty())
                                error += "\n - Телефон";
                            if (Heap.accountHotel.getVat().isEmpty())
                                error += "\n - ИНН";
                            if (Heap.accountHotel.getRegion().isEmpty() && Heap.accountHotel.getCity().isEmpty())
                                error += "\n - Город и/или Регион";

                            if (error.isEmpty())
                                return;

                            AlertDialog.Builder builder = new AlertDialog.Builder(SettingsProfileHotelActivity.this);
                            builder.setTitle("Уведомление")
                                    .setIcon(R.drawable.png_hotel_blue)
                                    .setMessage("Для подтверждения Вашего профиля необходимо указать следующую информацию:\n" + error)
                                    .setCancelable(true)
                                    .setPositiveButton("Ок",
                                            new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    dialog.cancel();
                                                }
                                            });
                            builder.create().show();
                        }

                    }
                } catch (Exception e) {
                    Log.v("TAG", e.toString());
                }
            }
        });
    }


}
