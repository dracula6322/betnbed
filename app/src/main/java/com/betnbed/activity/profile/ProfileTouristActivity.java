package com.betnbed.activity.profile;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.betnbed.Application;
import com.betnbed.Heap;
import com.betnbed.R;
import com.betnbed.activity.ScreenSlidePagerAdapter;
import com.betnbed.customView.BetnbedActivity;
import com.betnbed.model.Image;
import com.betnbed.model.User;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProfileTouristActivity extends BetnbedActivity {

    @BindView(R.id.firstLong) TextView firstLong;
    @BindView(R.id.secondLong) TextView secondLong;
    @BindView(R.id.thirdLong) TextView thirdLong;
    @BindView(R.id.gender) TextView gender;
    @BindView(R.id.occupation) TextView occupation;
    @BindView(R.id.dateBirthday) TextView dateBirthday;
    @BindView(R.id.name) TextView name;
    @BindView(R.id.wherePlace) TextView placeBirthday;
    @BindView(R.id.fab) FloatingActionButton fab;
    @BindView(R.id.emptyImagePagerLayout) View emptyImagePagerLayout;
    @BindView(R.id.pager) ViewPager page;
    private User user;

    /*public static ProfileTouristActivity newInstance(boolean isAnotherTourist, ArrayList<String> images, String jsonArray) {
        ProfileTouristActivity fragment = new ProfileTouristActivity();
        Bundle bundle = new Bundle();
        bundle.putStringArrayList("images", images);
        bundle.putBoolean("isAnotherTourist", isAnotherTourist);
        bundle.putString("jsonArray", jsonArray);
        fragment.setArguments(bundle);
        return fragment;
    }*/

    @Override public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tourist_profile);
        if (getIntent().getBooleanExtra("isAnotherTourist", false))
            getProfile();
        else user = Heap.accountUser;

        ButterKnife.bind(this);

        initNavigationView();
        initToolbar("Профиль");
        initView();

    }

    private void initView() {

        if (!user.getFirstName().isEmpty() || !user.getLastName().isEmpty()) name.setText(user.getFirstName() + " " + user.getLastName());

        firstLong.setText(!user.getFirstLongDesc().isEmpty() ? user.getFirstLongDesc() : (user.getId() == Heap.touristId ? getString(R.string.my_interest_my) : getString(R.string.my_interest_other)));
        secondLong.setText(!user.getSecondLongDesc().isEmpty() ? user.getSecondLongDesc() : (user.getId() == Heap.touristId ? getString(R.string.something_interesting_about_me_my) : getString(R.string.something_interesting_about_me_my_other)));
        thirdLong.setText(!user.getThirdLongDesc().isEmpty() ? user.getThirdLongDesc() : (user.getId() == Heap.touristId ? getString(R.string.teach_learn_share_my) : getString(R.string.teach_learn_share_my_other)));
        gender.setText(getResources().getStringArray(R.array.genders)[user.getGender()]);
        occupation.setText(user.getProfession());

        if (user.getYearsOld() > 0)
            dateBirthday.setText("Возраст: " + user.getYearsOld());


        if (!user.getIdCity().isEmpty())
            Places.GeoDataApi.getPlaceById(Application.mGoogleApiClient, user.getIdCity())
                    .setResultCallback(new ResultCallback<PlaceBuffer>() {
                        @Override
                        public void onResult(@NonNull PlaceBuffer places) {
                            if (places.getStatus().isSuccess() && places.getCount() > 0) {
                                placeBirthday.setText(places.get(0).getAddress().toString());
                            } else {
                                user.setFullNameCity("");

                            }
                            places.release();
                        }
                    });
        else placeBirthday.setText("");


        if (user.getId() == Heap.touristId) {
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final String qwe[] = {"Редактировать профиль", "Редактировать изображения"};
                    AlertDialog.Builder builder = new AlertDialog.Builder(ProfileTouristActivity.this, R.style.AppCompatAlertDialogStyle);
                    builder.setItems(qwe, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            switch (i) {
                                case 0:
                                    showTouristSettings();
                                    break;
                                case 1:
                                    showGridImage();
                                    break;
                            }
                        }
                    });
                    builder.show();

                }
            });
        } else fab.setVisibility(View.GONE);


        final ScreenSlidePagerAdapter pagerAdapter = new ScreenSlidePagerAdapter(user.getImages());
        if (user.getImages().size() > 0) {
            emptyImagePagerLayout.setVisibility(View.GONE);
            page.setVisibility(View.VISIBLE);
            page.setClipToPadding(false);
            page.setPadding(48, 0, 48, 0);
            page.setPageMargin(8);
            page.setAdapter(pagerAdapter);

        } else {
            pagerAdapter.notifyDataSetChanged();
            page.setVisibility(View.GONE);
            emptyImagePagerLayout.setVisibility(View.VISIBLE);
        }
    }

    private void showTouristSettings() {

        Intent intent = new Intent(this, SettingsProfileTouristActivity.class);
        startActivity(intent);
        //Fragment fragment = new SettingsProfileTouristFragment();
        //getFragmentManager().beginTransaction().hide(this).addToBackStack("").add(R.id.frame, fragment).commit();
    }

    private void showGridImage() {
        Intent intent = new Intent(this, GridImageActivity.class);
        intent.putExtra("type", 1);
        startActivity(intent);
        //Fragment fragment = GridImageFragment.newInstance(1);
        //getFragmentManager().beginTransaction().addToBackStack("").add(R.id.frame, fragment).commit();
    }

    private void getProfile() {
        try {

            JSONArray data = new JSONArray(getIntent().getStringExtra("jsonArray"));
            int delta = data.length() == 17 ? 1 : 0;
            user = new User.Builder()
                    .id(data.getInt(delta))
                    .clientProfileId(data.getInt(1 + delta))
                    .firstName(data.getString(3 + delta))
                    .lastName(data.getString(4 + delta))
                    .idCity(data.getString(6 + delta))
                    .gender(data.getInt(7 + delta))
                    .profession(data.getString(8 + delta))
                    .photo(data.getString(10 + delta))
                    .dateBirthday(data.getString(11 + delta))
                    .firstLongDesc(data.getString(12 + delta))
                    .secondLongDesc(data.getString(13 + delta))
                    .thirdLongDesc(data.getString(14 + delta))
                    .build();


            String avatar = user.getPhoto();
            List<String> images = getIntent().getStringArrayListExtra("images");
            ArrayList<Image> result = new ArrayList<>();
            if (!avatar.isEmpty()) result.add(new Image(avatar, -1));
            for (int i = 0; i < images.size(); i++) {
                String path = images.get(i);
                if (!avatar.isEmpty()) if (path.equals(result.get(0).getUrl())) continue;
                result.add(new Image(path));
            }
            user.setImages(result);
        } catch (JSONException e) {
            Log.v("TAG", e.toString());
        }
    }

    @Override public void onResume() {
        super.onResume();
        initView();
    }
}