package com.betnbed.activity.profile;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.betnbed.Application;
import com.betnbed.Heap;
import com.betnbed.R;
import com.betnbed.RequestConstructor;
import com.betnbed.activity.ScreenSlidePagerAdapter;
import com.betnbed.activity.ShowHotelOrHostOnMapActivity;
import com.betnbed.controller.RequestController;
import com.betnbed.controller.SettingsController;
import com.betnbed.customView.BetnbedActivity;
import com.betnbed.interfaces.SocketInterface;
import com.betnbed.model.Hotel;
import com.betnbed.model.Image;
import com.betnbed.service.NetworkService;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProfileHotelActivity extends BetnbedActivity {

    @BindView(R.id.title) TextView title;
    @BindView(R.id.address) TextView address;
    @BindView(R.id.statusPhoto) TextView statusPhoto;
    @BindView(R.id.firstLongDesc) TextView firstLongDesc;
    @BindView(R.id.secondLongDesc) TextView secondLongDesc;
    @BindView(R.id.thirdLongDesc) TextView thirdLongDesc;
    @BindView(R.id.fourLongDesc) TextView fourLongDesc;
    @BindView(R.id.contactHeader) TextView contactHeader;
    @BindView(R.id.contact) TextView contact;
    @BindView(R.id.contactCard) View contactCard;
    @BindView(R.id.pager) ViewPager page;
    @BindView(R.id.emptyImagePagerLayout) View emptyImagePagerLayout;
    @BindView(R.id.map) Button map;
    @BindView(R.id.fab) FloatingActionButton fab;
    @BindView(R.id.hostel) View hostel;
    @BindView(R.id.cash) View cash;
    @BindView(R.id.plastic) View plastic;
    @BindView(R.id.home) View ihome;
    @BindView(R.id.hotel) View ihotel;
    @BindView(R.id.avatar) ImageView avatarImageView;
    @BindView(R.id.profileLayout) LinearLayout profileLayout;
    @BindView(R.id.profile_link) TextView linkProfile;

    private Hotel hotel;

    /*public static ProfileHotelActivity newInstance(boolean isAnotherHotel, ArrayList<String> images, String jsonArray) {
        ProfileHotelActivity fragment = new ProfileHotelActivity();
        Bundle bundle = new Bundle();
        bundle.putStringArrayList("images", images);
        bundle.putBoolean("isAnotherHotel", isAnotherHotel);
        bundle.putString("jsonArray", jsonArray);
        fragment.setArguments(bundle);
        return fragment;
    }*/

    @Override public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hotel_profile);
        try {
            if (getIntent().getBooleanExtra("isAnotherHotel", false))
                getProfile();
            else hotel = Heap.accountHotel;
        } catch (JSONException e) {
            Log.v("TAG", "ProfileHotelActivity " + e.toString());
        }

        ButterKnife.bind(this);

        initNavigationView();
        initToolbar(!hotel.isHotel() ? "Профиль хоста" : "Профиль отеля");
        initView();

    }

    private void initView() {

        title.setText(hotel.getTitle());
        address.setText(hotel.getStreet() + " " + hotel.getHouse_number());
        statusPhoto.setText(hotel.getId() == Heap.hotelId ? getString(R.string.zeroImagesInProfileMy) : getString(R.string.zeroImagesInProfileOther));
        firstLongDesc.setText(!hotel.getFirstLongDesc().isEmpty() ? hotel.getFirstLongDesc() : ((hotel.getId() == Heap.hotelId || hotel.getId() == Heap.hostId) ? getString(R.string.hotel_and_territory_my) : getString(R.string.hotel_and_territory_my_other)));
        secondLongDesc.setText(!hotel.getSecondLongDesc().isEmpty() ? hotel.getSecondLongDesc() : ((hotel.getId() == Heap.hotelId || hotel.getId() == Heap.hostId) ? getString(R.string.neighborhood_my) : getString(R.string.neighborhood_my_other)));
        thirdLongDesc.setText(!hotel.getThirdLongDesc().isEmpty() ? hotel.getThirdLongDesc() : ((hotel.getId() == Heap.hotelId || hotel.getId() == Heap.hostId) ? getString(R.string.functions_my) : getString(R.string.functions_my_other)));
        fourLongDesc.setText(!hotel.getFourLongDesc().isEmpty() ? hotel.getFourLongDesc() : ((hotel.getId() == Heap.hotelId || hotel.getId() == Heap.hostId) ? getString(R.string.interesting_my) : getString(R.string.interesting_my_other)));
        contactHeader.setText(hotel.isHotel() ? getString(R.string.contactsAndEssential) : getString(R.string.contacts));


        if (hotel.isHotel() && hotel.getId() == Heap.hotelId) {
            if (!hotel.getPhone().isEmpty() || !hotel.getContacts().isEmpty() || !hotel.getVat().isEmpty()) {
                String contactText = "";

                if (!hotel.getContacts().isEmpty())
                    contactText += ("Контактное лицо: " + hotel.getContacts() + '\n');
                if (!hotel.getPhone().isEmpty())
                    contactText += ("Телефон: " + hotel.getPhone() + '\n');
                if (!hotel.getVat().isEmpty()) contactText += ("ИНН: " + hotel.getVat());
                contact.setText(contactText);
            } else
                contact.setText(getString(R.string.hotel_contact_my));
        } else contactCard.setVisibility(View.GONE);

        if (!hotel.isHotel()) {
            contactCard.setVisibility(View.VISIBLE);
            String contactText = "";
            if (!hotel.getContacts().isEmpty())
                contactText += ("Контактное лицо: " + hotel.getContacts() + '\n');
            if (!hotel.getPhone().isEmpty())
                contactText += ("Телефон: " + hotel.getPhone() + '\n');
            if (!hotel.getEmail().isEmpty()) contactText += ("Прочие контакты: " + hotel.getEmail());
            contact.setHint(getString(R.string.selectContacts));
            contact.setText(contactText);
        }

        ScreenSlidePagerAdapter pagerAdapter = new ScreenSlidePagerAdapter(hotel.getImages());
        if (hotel.getImages().size() > 0) {
            emptyImagePagerLayout.setVisibility(View.GONE);

            page.setVisibility(View.VISIBLE);
            page.setClipToPadding(false);
            page.setPadding(48, 0, 48, 0);
            page.setPageMargin(8);
            page.setAdapter(pagerAdapter);
        } else {

            pagerAdapter.notifyDataSetChanged();
            page.setVisibility(View.GONE);
            emptyImagePagerLayout.setVisibility(View.VISIBLE);
        }


        if (hotel.getLatLng() != null) {


            map.setVisibility(View.VISIBLE);
            if (!hotel.isHotel())
                map.setText("Показать хост на карте");
            else
                map.setText("Показать отель на карте");
            map.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(ProfileHotelActivity.this, ShowHotelOrHostOnMapActivity.class);
                    intent.putExtra("latLng", hotel.getLatLng());
                    intent.putExtra("title", hotel.getTitle());
                    intent.putExtra("address", hotel.getStreet() + " " + hotel.getHouse_number());
                    startActivity(intent);
                    //ShowHotelOrHostOnMapFragment fragment = ShowHotelOrHostOnMapFragment.newInstance(hotel.getLatLng(), hotel.getTitle(), hotel.getStreet() + " " + hotel.getHouse_number());
                    //getFragmentManager().beginTransaction().addToBackStack("").add(R.id.frame, fragment).commit();

                }
            });
        } else map.setVisibility(View.GONE);

        if (hotel.isHotel()) {
            if (SettingsController.isHotel() && hotel.getId() == Heap.hotelId) {
                fab.setVisibility(View.VISIBLE);
            } else fab.setVisibility(View.GONE);
        } else {
            if (!SettingsController.isHotel() && hotel.getId() == Heap.hostId) {
                fab.setVisibility(View.VISIBLE);
            } else fab.setVisibility(View.GONE);
        }

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String qwe[] = {"Редактировать профиль", "Редактировать изображения"};
                AlertDialog.Builder builder = new AlertDialog.Builder(ProfileHotelActivity.this, R.style.AppCompatAlertDialogStyle);
                builder.setItems(qwe, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int i) {
                                Fragment fragment;
                                switch (i) {
                                    case 0:
                                        showHotelSettings();
                                        break;
                                    case 1:
                                        showGridImage();

                                        break;
                                }
                            }
                        }
                );
                builder.show();
            }
        });


        if (hotel.getId() != Heap.hostId && !hotel.isHotel()) {

            linkProfile.setText(Html.fromHtml("Здравствуйте, " + Heap.accountUser.getFirstName() + ", я" + (hotel.getUserName().isEmpty() ? "," : ", <a <href=\"\">" + hotel.getUserName() + "</a>,") + " с радостью приглашаю Вас к себе"));
            profileLayout.setOnClickListener(

                    new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            shotTourist(hotel.getCreatorId());
                        }
                    }

            );

            BitmapImageViewTarget cycleBitmapImageViewTarget = new BitmapImageViewTarget(avatarImageView) {
                @Override
                protected void setResource(Bitmap resource) {
                    RoundedBitmapDrawable circularBitmapDrawable = RoundedBitmapDrawableFactory.create(getResources(), resource);
                    circularBitmapDrawable.setCircular(true);
                    avatarImageView.setImageDrawable(circularBitmapDrawable);
                }
            };
            if (hotel.getUserPhoto().length() > 0)
                Glide.with(this).load(hotel.getUserPhoto()).asBitmap().error(R.drawable.avatar_default).centerCrop().into(cycleBitmapImageViewTarget);
            else
                Glide.with(this).load(R.drawable.avatar_default).asBitmap().error(R.drawable.avatar_default).centerCrop().into(avatarImageView);
        } else profileLayout.setVisibility(View.GONE);


        int massAdditionalIds[] = {R.id.wifi, R.id.tea, R.id.parking, R.id.tv, R.id.bath, R.id.kitchen, R.id.cat, R.id.washing};

        for (int i = 0; i < massAdditionalIds.length; i++) {
            View image = findViewById(massAdditionalIds[i]);
            if (hotel.getMassAdditionalInfo()[i + 1] != 0)
                image.setVisibility(View.VISIBLE);
            else image.setVisibility(View.GONE);
        }
        if (hotel.isHotel()) {
            if ((hotel.getType() & Hotel.HOTEL_TYPE) == Hotel.HOTEL_TYPE) ihotel.setVisibility(View.VISIBLE);
            else ihotel.setVisibility(View.GONE);
            if ((hotel.getType() & Hotel.HOSTEL_TYPE) == Hotel.HOSTEL_TYPE) hostel.setVisibility(View.VISIBLE);
            else hostel.setVisibility(View.GONE);
        } else ihome.setVisibility(View.VISIBLE);

        switch (hotel.getMoney()) {
            case 0:
                cash.setVisibility(View.VISIBLE);
                plastic.setVisibility(View.VISIBLE);
                break;
            case 1:
                cash.setVisibility(View.GONE);
                plastic.setVisibility(View.VISIBLE);
                break;
            case 2:
                cash.setVisibility(View.VISIBLE);
                plastic.setVisibility(View.GONE);
                break;
        }


        if (hotel.getFullNameCity() != null)
            if (hotel.getFullNameCity().length() > 0)
                Places.GeoDataApi.getPlaceById(Application.mGoogleApiClient, hotel.getCity())
                        .setResultCallback(new ResultCallback<PlaceBuffer>() {
                            @Override
                            public void onResult(@NonNull PlaceBuffer places) {
                                if (places.getStatus().isSuccess() && places.getCount() > 0) {
                                    hotel.setFullNameCity(places.get(0).getAddress().toString());
                                } else {

                                    hotel.setFullNameCity("");
                                }
                                places.release();
                            }
                        });

        if (hotel.getRegion().length() > 0)
            Places.GeoDataApi.getPlaceById(Application.mGoogleApiClient, hotel.getRegion())
                    .setResultCallback(new ResultCallback<PlaceBuffer>() {
                        @Override
                        public void onResult(@NonNull PlaceBuffer places) {
                            if (places.getStatus().isSuccess() && places.getCount() > 0) {
                                hotel.setFullNameRegion(places.get(0).getAddress().toString());
                            } else {

                                hotel.setFullNameRegion("");
                            }
                            places.release();
                        }
                    });


    }

    private void showGridImage() {
        Intent intent = new Intent(this, GridImageActivity.class);
        intent.putExtra("type", hotel.isHotel() ? 3 : 2);
        startActivity(intent);
    }

    private void showHotelSettings() {
        Intent intent = new Intent(this, SettingsProfileHotelActivity.class);
        startActivity(intent);
    }

    private void getProfile() throws JSONException {

        hotel = new Hotel((new JSONArray(getIntent().getStringExtra("jsonArray"))), this);
        String avatar = hotel.getPhoto();
        List<String> images = getIntent().getStringArrayListExtra("images");
        List<Image> result = new ArrayList<>();
        if (!avatar.isEmpty()) result.add(new Image(avatar, -1));
        for (int i = 0; i < images.size(); i++) {
            String path = images.get(i);
            if (!avatar.isEmpty()) if (path.equals(result.get(0).getUrl())) continue;
            result.add(new Image(path));
        }
    }

    private void shotTourist(final int id) {
        NetworkService.requestController.sendRequest(RequestConstructor.CGetTourist(id), "SGetTourist", new SocketInterface() {
            @Override
            public void onSuccess(String request) {

                String jsonArray = "";
                final ArrayList<String> images = new ArrayList<>();

                int idImage = 0;
                try {
                    idImage = new JSONObject(request).getJSONArray("data").getInt(0);

                    jsonArray = new JSONObject(request).getJSONArray("data").toString();
                } catch (JSONException e) {
                    Log.v("TAG", e.toString());
                }

                NetworkService.requestController.sendRequest(RequestConstructor.CGetImages(1, idImage), "SGetImages", new SocketInterface() {
                    @Override
                    public void onSuccess(String request) {
                        images.addAll(RequestController.getLinkImagesWithOutId(request));
                    }

                    @Override
                    public void onError(int error) {

                    }
                });
                Intent intent = new Intent(ProfileHotelActivity.this, ProfileTouristActivity.class);
                intent.putExtra("images", images);
                intent.putExtra("isAnotherTourist", true);
                intent.putExtra("jsonArray", jsonArray);
                startActivity(intent);
            }

            @Override
            public void onError(int error) {

            }
        });
    }

    @Override public void onResume() {
        super.onResume();
        Log.v("TAG", "We on onResume   ");
        initView();
    }

    /*@Override public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN) {

            new Thread(new Runnable() {
                @Override public void run() {
                    try {
                        Thread.sleep(5000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    Log.v("TAG", "We add request");

                    Heap.accountUser.getRequestTouristController().SInviteDeleted(79, 1, "Прост");

                }
            }).start();


        }
        return super.onKeyDown(keyCode, event);
    }*/

}