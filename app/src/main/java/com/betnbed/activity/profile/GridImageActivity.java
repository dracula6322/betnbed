package com.betnbed.activity.profile;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import com.betnbed.Heap;
import com.betnbed.R;
import com.betnbed.RequestConstructor;
import com.betnbed.adapters.ImageAdapter;
import com.betnbed.customView.ToolbarActivity;
import com.betnbed.interfaces.SocketInterface;
import com.betnbed.model.Image;
import com.betnbed.model.UserAndHotelModel;
import com.betnbed.service.NetworkService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;

public class GridImageActivity extends ToolbarActivity {

    private final int GET_PHOTO_FROM_CAMERA = 1;
    private final int GET_PHOTO_FROM_GALLERY = 2;


    UserAndHotelModel profile;
    Disposable disposable;

    //private List<Image> content;
    private int type;
    //private String avatar;
    private ImageAdapter adapter;
    /*public Handler handler = new Handler() {
        public void handleMessage(android.os.Message msg) {

            adapter.setContent(profile.getImages());
            adapter.setAvatar(profile.getPhoto());
            toolbar.setTitle(adapter.getCount() > 0 ? (adapter.getCount() + " " + "фото") : getString(R.string.zeroImagesInProfileMy));
        }
    };*/
    public Handler handlerError = new Handler() {
        public void handleMessage(android.os.Message msg) {

            switch (msg.what) {
                case 37:
                    Toast.makeText(GridImageActivity.this, "Изображение слишком маленькое", Toast.LENGTH_LONG).show();
                    break;
                case 36:
                    Toast.makeText(GridImageActivity.this, "Изображение слишком большое", Toast.LENGTH_LONG).show();
                    break;
                case 35:
                    Toast.makeText(GridImageActivity.this, "Ошибка в хеше изображения", Toast.LENGTH_LONG).show();
                    break;
                case 55:
                    Toast.makeText(GridImageActivity.this, "Изображение уже существует", Toast.LENGTH_LONG).show();
                    break;
            }
        }
    };


    @BindView(R.id.gridView) GridView listView;
    @BindView(R.id.fab) FloatingActionButton fab;

    @OnClick(R.id.fab) void fab() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Добавить изображение");
        builder.setItems(getResources().getStringArray(R.array.addImageMethods), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                switch (item) {
                    case 0: {
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        File f = new File(Environment.getExternalStorageDirectory(), "temp.jpg");
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
                        startActivityForResult(intent, GET_PHOTO_FROM_CAMERA);
                        break;
                    }
                    case 1: {
                        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                        intent.setType("image/*");
                        startActivityForResult(Intent.createChooser(intent, "Выберите источник"), GET_PHOTO_FROM_GALLERY);
                        break;
                    }
                }
            }
        });
        builder.show();
    }

    @Override public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_gallery_image);
        type = getIntent().getIntExtra("type", 0);

        profile = type == 1 ? Heap.accountUser : Heap.accountHotel;


        adapter = new ImageAdapter(GridImageActivity.this, profile.getImages(), profile.getPhoto());

        ButterKnife.bind(this);
        initToolbar(adapter.getCount() > 0 ? (adapter.getCount() + " " + "фото") : getString(R.string.zeroImagesInProfileMy));
        initView();


    }

    @Override protected void onStart() {
        super.onStart();

        final Observer<List<Image>> observer = new Observer<List<Image>>() {
            @Override public void onSubscribe(Disposable d) {
                Log.v("TAG", "onSubscribe");
                disposable = d;
            }

            @Override public void onNext(List<Image> images) {
                Log.v("TAG", "onNext " + images.size());
                adapter.setContent(images);
                toolbar.setTitle(adapter.getCount() > 0 ? (adapter.getCount() + " " + "фото") : getString(R.string.zeroImagesInProfileMy));
            }

            @Override public void onError(Throwable e) {
                Log.v("TAG", "onError " + e.toString());
            }

            @Override public void onComplete() {
                Log.v("TAG", "onComplete");
            }
        };

        profile.getImagesObservable().observeOn(AndroidSchedulers.mainThread()).subscribe(observer);

    }

    @Override protected void onStop() {
        super.onStop();

        Log.v("TAG", "disposable.isDisposed() = " + disposable.isDisposed());
        if (!disposable.isDisposed()) disposable.dispose();
    }


    private void initView() {


        listView.setAdapter(adapter);
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {

                AlertDialog.Builder builder = new AlertDialog.Builder(GridImageActivity.this);
                builder.setTitle("Редактировать изображение");
                builder.setItems(adapter.getContent().get(position).getUrl().equals(profile.getPhoto()) ? getResources().getStringArray(R.array.grimImageMenuWithAvatar) : getResources().getStringArray(R.array.grimImageMenuWithOutAvatar), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int item) {
                        if (item == 0) {
                            if (Heap.changeAvatar(type, adapter.getContent().get(position).getUrl())) {
                                Toast.makeText(GridImageActivity.this, "Изображение изменено", Toast.LENGTH_LONG).show();
                                profile.setPhoto(adapter.getContent().get(position).getUrl());
                                adapter.setAvatar(adapter.getContent().get(position).getUrl());
                                adapter.notifyDataSetChanged();
                            } else
                                Toast.makeText(GridImageActivity.this, "Возникла ошибка", Toast.LENGTH_LONG).show();
                        }
                        if (item == 1) {
                            if (adapter.getContent().get(position).getUrl().equals(profile.getPhoto())) {
                                if (Heap.changeAvatar(type, "")) {
                                    profile.setPhoto("");
                                    if (adapter.getContent().get(position).getImageId() == -1)
                                        profile.deleteImage(position);
                                    else {
                                        NetworkService.requestController.sendRequest(RequestConstructor.CDeleteImage(adapter.getContent().get(position).getImageId()), "SImageDeleted", new SocketInterface() {
                                            @Override
                                            public void onSuccess(String request) {
                                                try {
                                                    if (new JSONObject(request).getString("type").equals("SImageDeleted"))
                                                        profile.deleteImage(position);
                                                } catch (JSONException e) {
                                                    Log.v("TAG", "Some error");
                                                }
                                            }

                                            @Override
                                            public void onError(int error) {

                                            }
                                        });

                                    }
                                }
                            } else {
                                NetworkService.requestController.sendRequest(RequestConstructor.CDeleteImage(adapter.getContent().get(position).getImageId()), "SImageDeleted", new SocketInterface() {
                                    @Override
                                    public void onSuccess(String request) {
                                        profile.deleteImage(position);
                                        /*if (type == 1)
                                            Heap.accountUser.getImages().remove(position);
                                        else
                                            Heap.accountHotel.getImages().remove(position);*/
                                        //adapter.notifyDataSetChanged();

                                    }

                                    @Override
                                    public void onError(int error) {

                                    }
                                });
                            }
                            //handler.sendEmptyMessage(PHOTO_DELETED);
                            Toast.makeText(GridImageActivity.this, "Изображение удалено", Toast.LENGTH_LONG).show();
                        }
                        if (item == 2)
                            if (Heap.changeAvatar(type, "")) {
                                Toast.makeText(GridImageActivity.this, "Изображение изменено", Toast.LENGTH_LONG).show();
                                adapter.setAvatar("");
                                profile.setPhoto("");
                            } else
                                Toast.makeText(GridImageActivity.this, "Возникла ошибка", Toast.LENGTH_LONG).show();
                    }
                });
                builder.show();
                return true;
            }
        });
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(GridImageActivity.this, ImagePagerActivity.class);
                intent.putExtra("type", type);
                intent.putExtra("IMAGE_POSITION", position);
                startActivity(intent);
            }
        });


    }

    @Override public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == GET_PHOTO_FROM_CAMERA) {
                File f = new File(Environment.getExternalStorageDirectory().toString());
                for (File temp : f.listFiles()) {
                    if (temp.getName().equals("temp.jpg")) {
                        f = temp;
                        try {
                            Bitmap tm = BitmapFactory.decodeFile(f.getPath());
                            Matrix matrix = new Matrix();
                            int orientation = new ExifInterface(f.getPath()).getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
                            Log.v("TAG", "Orientation = " + orientation);
                            switch (orientation) {
                                case ExifInterface.ORIENTATION_ROTATE_90:
                                    matrix.setRotate(90);
                                    break;
                                case ExifInterface.ORIENTATION_ROTATE_180:
                                    matrix.setRotate(180);
                                    break;
                                case ExifInterface.ORIENTATION_ROTATE_270:
                                    matrix.setRotate(270);
                                    break;
                            }
                            tm = Bitmap.createBitmap(tm, 0, 0, tm.getWidth(), tm.getHeight(), matrix, true);
                            Toast.makeText(GridImageActivity.this, "Загрузка изображения началась", Toast.LENGTH_LONG).show();
                            ByteArrayOutputStream stream = new ByteArrayOutputStream();
                            tm.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                            save(stream.toByteArray(), type);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        break;
                    }
                }
            }
            if (requestCode == GET_PHOTO_FROM_GALLERY) {
                try {
                    InputStream inputStream = getContentResolver().openInputStream(data.getData());

                    ByteArrayOutputStream image = new ByteArrayOutputStream();
                    while (inputStream.available() != 0) {
                        byte[] part = new byte[inputStream.available()];
                        inputStream.read(part);
                        image.write(part);
                    }
                    Toast.makeText(GridImageActivity.this, "Загрузка изображения началась", Toast.LENGTH_LONG).show();
                    save(image.toByteArray(), type);
                } catch (IOException e) {
                    Log.v("TAG", "Error " + e.toString());
                }
            }
        }
    }

    private void save(final byte[] image, final int type) {

        int compress = 100;
        String encodedImage;

        Bitmap bm = BitmapFactory.decodeByteArray(image, 0, image.length);

        do {
            compress -= 10;
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

            bm.compress(Bitmap.CompressFormat.JPEG, compress, byteArrayOutputStream);
            byte[] b = byteArrayOutputStream.toByteArray();
            encodedImage = Base64.encodeToString(b, Base64.NO_WRAP);

        } while (encodedImage.length() > 1500000);

        byte sha256[] = sha256(encodedImage.getBytes());
        String base64sha256 = Base64.encodeToString(sha256, Base64.NO_WRAP);


        final String finalEncodedImage = encodedImage;
        NetworkService.requestController.sendRequest(RequestConstructor.CUploadImage(finalEncodedImage.length(), type, base64sha256), "SUploadImage", new SocketInterface() {
            @Override
            public void onSuccess(String request) {
                try {

                    String image = finalEncodedImage;

                    JSONObject jsonRequest = new JSONObject(request);
                    JSONArray data = jsonRequest.getJSONArray("data");
                    String hash = data.getString(0);
                    int howManyPart = data.getInt(1);
                    int sizePart = data.getInt(2);
                    String partRequest;
                    for (int i = 0; i < howManyPart - 1; i++) {
                        partRequest = RequestConstructor.CImagePart(hash, i, image.substring(i * sizePart, i * sizePart + sizePart));
                        NetworkService.requestController.sendRequest(partRequest, null, null);
                    }
                    partRequest = RequestConstructor.CImagePart(hash, howManyPart - 1, image.substring(image.length() - image.length() % sizePart, image.length()));
                    NetworkService.requestController.sendRequest(partRequest, null, null);
                } catch (JSONException e) {
                    Log.v("TAG", e.toString());
                }
            }

            @Override
            public void onError(int error) {

                switch (error) {
                    case 55:
                        Toast.makeText(GridImageActivity.this, "Данное изображение уже существует", Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        });
    }

    private static byte[] sha256(byte[] bytes) {

        MessageDigest sha256 = null;
        try {
            sha256 = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            Log.v("TAG", e.toString());
        }
        sha256.update(bytes);
        byte[] result = sha256.digest();
        sha256.reset();
        return result;
    }

    /*@Override public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN) {

            new Thread(new Runnable() {
                @Override public void run() {

                    try {
                        Thread.sleep(5000);

                        Heap.accountUser.addImage(new Image("http://192.168.10.101/maxresdefault.jpg"));
                    } catch (Exception e) {
                        Log.v("TAG", e.toString());
                    }
                }
            }).start();


        }
        return super.onKeyDown(keyCode, event);
    }
*/
}