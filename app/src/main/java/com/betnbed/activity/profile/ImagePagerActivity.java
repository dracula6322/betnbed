package com.betnbed.activity.profile;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.betnbed.Heap;
import com.betnbed.R;
import com.betnbed.customView.ToolbarActivity;
import com.betnbed.model.Image;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ImagePagerActivity extends ToolbarActivity{

    private static final int OUR_IMAGE_TOURIST = 1;
    private static final int OUR_IMAGE_HOST = 2;
    private static final int OUR_IMAGE_HOTEL = 3;


    private List<Image> content;
    private String avatar;
    private int imagePosition;
    private int type;

    @BindView(R.id.pager) ViewPager pager;
    @BindView(R.id.fab) FloatingActionButton fab;

    @Override public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_pager);
        type = getIntent().getIntExtra("type", 0);

        if (type == OUR_IMAGE_TOURIST) {
            content = Heap.accountUser.getImages();
            avatar = Heap.accountUser.getPhoto();
        }
        if (type == OUR_IMAGE_HOST || type == OUR_IMAGE_HOTEL) {
            content = Heap.accountHotel.getImages();
            avatar = Heap.accountHotel.getPhoto();
        }
        imagePosition = getIntent().getIntExtra("IMAGE_POSITION", 0);

        ButterKnife.bind(this);

        initToolbar("");
        initView();

    }

    private void initView() {


        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Heap.changeAvatar(type, content.get(imagePosition).getUrl())) {
                    Toast.makeText(ImagePagerActivity.this, "Изображение установлено как главное", Toast.LENGTH_LONG).show();

                    avatar = content.get(imagePosition).getUrl();
                    if (type == 1)
                        Heap.accountUser.setPhoto(avatar);
                    else Heap.accountHotel.setPhoto(avatar);
                    fab.setVisibility(View.GONE);
                } else
                    Toast.makeText(ImagePagerActivity.this, "Возникла ошибка", Toast.LENGTH_LONG).show();
            }
        });


        pager.setAdapter(new ImageAdapter());
        pager.setCurrentItem(imagePosition);
        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                fab.setVisibility(avatar.equals(content.get(position).getUrl()) ? View.GONE : View.VISIBLE);
                imagePosition = position;
                toolbar.setTitle((position + 1) + "/" + content.size());
            }

            @Override
            public void onPageSelected(int position) {
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
    }

    private class ImageAdapter extends PagerAdapter {

        private final Context context;
        private final LayoutInflater inflater;

        ImageAdapter() {
            this.context = ImagePagerActivity.this;
            inflater = LayoutInflater.from(context);

        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

        @Override
        public int getCount() {
            return content.size();
        }

        @Override
        public Object instantiateItem(ViewGroup view, int position) {
            View imageLayout = inflater.inflate(R.layout.image_pager_imageview, view, false);

            final ProgressBar progressBar = (ProgressBar) imageLayout.findViewById(R.id.progress_bar);
            progressBar.getIndeterminateDrawable().setColorFilter(Color.parseColor("#00a0f0"), PorterDuff.Mode.MULTIPLY);

            Glide.with(context)
                    .load(content.get(position).getUrl())
                    .listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            progressBar.setVisibility(View.GONE);
                            return false;
                        }
                    })
                    .into((ImageView) imageLayout.findViewById(R.id.image));

            view.addView(imageLayout, 0);
            return imageLayout;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view.equals(object);
        }

        @Override
        public Parcelable saveState() {
            return null;
        }
    }
}
