package com.betnbed.activity.profile;


import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.Toast;

import com.betnbed.Heap;
import com.betnbed.R;
import com.betnbed.RequestConstructor;
import com.betnbed.adapters.ViewPagerAdapter;
import com.betnbed.customView.ToolbarActivity;
import com.betnbed.fragment.SettingsProfileEditText;
import com.betnbed.fragment.SettingsProfileTouristEditsText;
import com.betnbed.interfaces.SocketInterface;
import com.betnbed.service.NetworkService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SettingsProfileTouristActivity extends ToolbarActivity {

    private final String[] tabMass = {
            "Личная информация",
            "Мои интересы",
            "Интересное обо мне",
            "Научу, изучу, поделюсь",
    };

    @BindView(R.id.viewpager) ViewPager viewPager;
    @BindView(R.id.tabs) TabLayout tabLayout;
    @BindView(R.id.save_user_settings) View save_user_settings;

    @Override public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings_profile_user);

        ButterKnife.bind(this);

        initToolbar(tabMass[0]);
        initView();

    }


    private void initView() {

        final ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        final SettingsProfileTouristEditsText userInformation = new SettingsProfileTouristEditsText();
        final SettingsProfileEditText firstTab = SettingsProfileEditText.newInstance(1);
        final SettingsProfileEditText secondTab = SettingsProfileEditText.newInstance(2);
        final SettingsProfileEditText thirdTab = SettingsProfileEditText.newInstance(3);

        adapter.addFragment(userInformation, "");
        adapter.addFragment(firstTab, "");
        adapter.addFragment(secondTab, "");
        adapter.addFragment(thirdTab, "");
        viewPager.setAdapter(adapter);


        tabLayout.setupWithViewPager(viewPager);
        tabLayout.getTabAt(0).setIcon(R.drawable.png_avatar_orange);
        tabLayout.getTabAt(1).setIcon(R.drawable.png_my_interests);
        tabLayout.getTabAt(2).setIcon(R.drawable.png_funny_about_me);
        tabLayout.getTabAt(3).setIcon(R.drawable.png_teach_learn_share);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
                toolbar.setTitle(tabMass[tab.getPosition()]);

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });

        save_user_settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {

                    JSONObject saveData = new JSONObject();

                    JSONObject data = userInformation.getSaveRequestSetting();

                    if (data.has("fname")) saveData.put("fname", data.getString("fname"));
                    if (data.has("lname")) saveData.put("lname", data.getString("lname"));
                    if (data.has("gender")) saveData.put("gender", data.getInt("gender"));
                    if (data.has("birth")) saveData.put("birth", data.getString("birth"));
                    if (data.has("prof")) saveData.put("prof", data.getString("prof"));
                    if (data.has("city")) saveData.put("city", data.getString("city"));


                    for (int i = 0; i < 3; i++) {
                        switch (i) {
                            case 0:
                                data = firstTab.getContent();
                                break;
                            case 1:
                                data = secondTab.getContent();
                                break;
                            case 2:
                                data = thirdTab.getContent();
                                break;
                            default:
                                throw new RuntimeException("Type error");
                        }

                        if (data.has("interests")) saveData.put("interests", data.getString("interests"));
                        if (data.has("funny")) saveData.put("funny", data.getString("funny"));
                        if (data.has("skills")) saveData.put("skills", data.getString("skills"));
                        if (data.has("territory")) saveData.put("territory", data.getString("territory"));
                        if (data.has("neighborhood")) saveData.put("neighborhood", data.getString("neighborhood"));
                        if (data.has("functions")) saveData.put("functions", data.getString("functions"));
                        if (data.has("interesting")) saveData.put("interesting", data.getString("interesting"));

                    }

                    if (saveData.length() > 0)
                        NetworkService.requestController.sendRequest(RequestConstructor.CChangeTourist(saveData), "SProfileChanged", new SocketInterface() {
                            @Override
                            public void onSuccess(String request) {

                                try {

                                    JSONArray jsonArray = new JSONObject(request).getJSONArray("data");
                                    if (jsonArray.getInt(0) != 1)
                                        return;

                                    JSONObject data = jsonArray.getJSONObject(2);

                                    if (data.has("fname")) Heap.accountUser.setFirstName(data.getString("fname"));
                                    if (data.has("lname")) Heap.accountUser.setLastName(data.getString("lname"));
                                    if (data.has("gender")) Heap.accountUser.setGender(data.getInt("gender"));
                                    if (data.has("birth")) Heap.accountUser.setDateBirthday(data.getString("birth"));
                                    if (data.has("prof")) Heap.accountUser.setProfession(data.getString("prof"));
                                    if (data.has("city")) Heap.accountUser.setIdCity(data.getString("city"));
                                    if (data.has("interests")) Heap.accountUser.setFirstLongDesc(data.getString("interests"));
                                    if (data.has("funny")) Heap.accountUser.setSecondLongDesc(data.getString("funny"));
                                    if (data.has("skills")) Heap.accountUser.setThirdLongDesc(data.getString("skills"));

                                    Toast.makeText(SettingsProfileTouristActivity.this, "Профиль обновлен", Toast.LENGTH_LONG).show();

                                } catch (JSONException ignore) {}

                            }

                            @Override
                            public void onError(int error) {

                            }
                        });
                    else Toast.makeText(SettingsProfileTouristActivity.this, "Профиль обновлен", Toast.LENGTH_LONG).show();
                } catch (Exception ignore) {

                }
            }
        });
    }

}