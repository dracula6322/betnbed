package com.betnbed.activity;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.betnbed.Application;
import com.betnbed.Heap;
import com.betnbed.R;
import com.betnbed.SC;
import com.betnbed.adapters.InviteRecyclerViewAdapter;
import com.betnbed.customView.BetnbedActivity;
import com.betnbed.interfaces.NewInviteTourist;
import com.betnbed.interfaces.RefreshHeader;
import com.betnbed.model.Request;
import com.bumptech.glide.Glide;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.betnbed.model.Hotel.HOSTEL_TYPE;
import static com.betnbed.model.Hotel.HOST_TYPE;
import static com.betnbed.model.Hotel.HOTEL_TYPE;

public class InvitesActivity extends BetnbedActivity {

    private int requestId;
    private Request request;

    private boolean isShowFullRequest = false;

    InviteRecyclerViewAdapter adapter;
    @BindView(R.id.emptyLayout) View emptyLayout;
    @BindView(R.id.header) View header;
    @BindView(R.id.arrival) TextView arrival;
    @BindView(R.id.departure) TextView departure;
    @BindView(R.id.money) TextView money;
    @BindView(R.id.money_type) TextView money_type;
    @BindView(R.id.people) TextView people;
    @BindView(R.id.special) TextView special;
    @BindView(R.id.place) TextView place;
    @BindView(R.id.hours) TextView hours;
    @BindView(R.id.invitesList) RecyclerView rv;
    @BindView(R.id.hotel) View hotel;
    @BindView(R.id.house) View house;
    @BindView(R.id.l1) View l1;
    @BindView(R.id.firstLayout) LinearLayout first;
    @BindView(R.id.more) ImageView more;
    @BindView(R.id.secondLayout) View secondLayout;
    @BindView(R.id.image_money_type) ImageView image_money_type;
    @BindView(R.id.hostel) View hostel;
    @BindView(R.id.wifi) View wifi;
    @BindView(R.id.tea) View tea;
    @BindView(R.id.parking) View parking;
    @BindView(R.id.tv) View tv;
    @BindView(R.id.bath) View bath;
    @BindView(R.id.kitchen) View kitchen;
    @BindView(R.id.cat) View cat;
    @BindView(R.id.washing) View washing;

    @Override public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invites);
        requestId = getIntent().getIntExtra("requestId", 0);
        request = Heap.accountUser.getRequestTouristController().getRequests().get(requestId);
        adapter = new InviteRecyclerViewAdapter(requestId, new RefreshHeader() {
            @Override public void refreshHeader() {
                header.setBackgroundColor(Color.parseColor("#2cc990"));
            }
        });
        adapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onItemRangeChanged(int positionStart, int itemCount) {
                super.onItemRangeChanged(positionStart, itemCount);
                emptyLayout.setVisibility(itemCount == 0 ? View.VISIBLE : View.GONE);
                rv.setVisibility(itemCount == 0 ? View.GONE : View.VISIBLE);
                adapter.notifyDataSetChanged();

            }
        });

        ButterKnife.bind(this);
        initNavigationView();
        initToolbar("Приглашения");
        initView();

    }

    @Override protected void onStart() {
        super.onStart();
        Heap.accountUser.getRequestTouristController().setInviteObserver(new NewInviteTourist() {
            @Override public void onNewInvite() {
                runOnUiThread(new Runnable() {
                    @Override public void run() {
                        adapter.setContents(request.getInvites());
                        rv.setAdapter(adapter);
                        initView();
                    }
                });

            }
        });
        adapter.setContents(request.getInvites());
    }

    @Override protected void onStop() {
        super.onStop();
        Heap.accountUser.getRequestTouristController().setInviteObserver(null);
    }

    private void initView() {

        arrival.setText(SC.df.format(request.getArrival() * 1000));
        departure.setText(SC.df.format(request.getDeparture() * 1000));
        money.setText(request.getMoney() + " " + request.getCurrency());
        money_type.setText(getResources().getStringArray(R.array.paymentMethod)[request.getMoneyType()]);
        people.setText(request.getPeople() + " " + "чел.");

        if (request.getInternet() != 0) wifi.setVisibility(View.VISIBLE);
        if (request.getBreakfast() != 0) tea.setVisibility(View.VISIBLE);
        if (request.getParking() != 0) parking.setVisibility(View.VISIBLE);
        if (request.getMedia() != 0) tv.setVisibility(View.VISIBLE);
        if (request.getShower() != 0) bath.setVisibility(View.VISIBLE);
        if (request.getKitchen() != 0) kitchen.setVisibility(View.VISIBLE);
        if (request.getPets() != 0) cat.setVisibility(View.VISIBLE);
        if (request.getServices() != 0) washing.setVisibility(View.VISIBLE);

        header.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isShowFullRequest) {
                    Glide.with(InvitesActivity.this)
                            .load(R.drawable.ic_keyboard_arrow_down_white_36dp)
                            .into(more);
                    first.setVisibility(View.GONE);
                    secondLayout.setVisibility(View.GONE);
                    isShowFullRequest = false;
                } else {
                    Glide.with(InvitesActivity.this)
                            .load(R.drawable.ic_keyboard_arrow_up_white_36dp)
                            .into(more);
                    first.setVisibility(View.VISIBLE);
                    secondLayout.setVisibility(View.VISIBLE);
                    isShowFullRequest = true;
                }
            }
        });

        if ((request.getHotelType() & HOST_TYPE) == HOST_TYPE)
            house.setVisibility(View.VISIBLE);
        if ((request.getHotelType() & HOTEL_TYPE) == HOTEL_TYPE)
            hotel.setVisibility(View.VISIBLE);
        if ((request.getHotelType() & HOSTEL_TYPE) == HOSTEL_TYPE)
            hostel.setVisibility(View.VISIBLE);

        if (request.getFullPlaceName().isEmpty()) {
            Places.GeoDataApi.getPlaceById(Application.mGoogleApiClient, request.getPlace())
                    .setResultCallback(new ResultCallback<PlaceBuffer>() {
                        @Override
                        public void onResult(@NonNull PlaceBuffer places) {
                            if (places.getStatus().isSuccess() && places.getCount() > 0) {
                                Heap.accountUser.getRequestTouristController().getRequests().get(requestId).setFullPlaceName(places.get(0).getAddress().toString());
                            }
                            places.release();
                        }
                    });
        }

        if (Heap.accountUser.getRequestTouristController().getRequests().get(requestId).isActive()) {
            header.setBackgroundColor(Color.parseColor("#2cc990"));
            l1.setBackground(getResources().getDrawable(R.drawable.edittext_green_bg));

        }

        if (request.getSpecial().length() > 0) {
            special.setVisibility(View.VISIBLE);
            special.setText(request.getSpecial());
        } else special.setVisibility(View.GONE);
        place.setText(request.getFullPlaceName());
        hours.setText(request.getHours());
        Glide.with(this).load(SC.wayToPayIdImage[request.getMoneyType()]).into(image_money_type);


        rv.setLayoutManager(new LinearLayoutManager(this));
        rv.setAdapter(adapter);

        emptyLayout.setVisibility(adapter.getItemCount() == 0 ? View.VISIBLE : View.GONE);
        rv.setVisibility(adapter.getItemCount() == 0 ? View.GONE : View.VISIBLE);

    }

    /*@Override public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN) {

            final Invite invite = new Invite.Builder()
                    .comment("asdasdadasdasd")
                    .title("asd")
                    .photo("")
                    .address("")
                    .city("d")
                    .type(2)
                    .requestId(79)
                    .inviteId(1)
                    .hotelId(2)
                    .state(0).build();


            new Thread(new Runnable() {
                @Override public void run() {
                    try {
                        Thread.sleep(5000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    Log.v("TAG", "We add invite");

                    Heap.accountUser.getRequestTouristController().SNewInvite(invite);

                }
            }).start();


        }
        return super.onKeyDown(keyCode, event);
    }*/

}