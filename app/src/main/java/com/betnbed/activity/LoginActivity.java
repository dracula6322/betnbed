package com.betnbed.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.betnbed.Heap;
import com.betnbed.R;
import com.betnbed.Utils;
import com.betnbed.activity.login.LoginCreateAccountActivity;
import com.betnbed.activity.login.LoginHandler;
import com.betnbed.controller.SettingsController;

import butterknife.BindView;
import butterknife.OnClick;

public class LoginActivity extends AppCompatActivity{

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    final int REGISTRATION_CODE = 101;

    public LoginHandler handlerSetButtonEnable;

    @BindView(R.id.forgetPassword) public TextView forgetPassword;
    @BindView(R.id.registration) public TextView registrationLink;
    @BindView(R.id.email) public TextInputLayout email;
    @BindView(R.id.password) public TextInputLayout password;
    @BindView(R.id.progress_bar_betnbed_login) public ProgressBar progressBarBetnbedLogin;
    @BindView(R.id.login) public View login;

    @OnClick(R.id.forgetPassword) void forgetPassword() {
        Utils.showForgotPasswordDialog(this);
    }

    @Override protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode == RESULT_OK){
            if(requestCode == REGISTRATION_CODE){
                if (email.getEditText() != null)
                    email.getEditText().setText(data.getStringExtra("email"));
                if (password.getEditText() != null)
                    password.getEditText().setText(data.getStringExtra("password"));

                AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
                builder.setTitle("Подтверждение регистрации")
                        .setMessage("На указанную Вами почту выслано письмо для подтвержения регистрации")
                        .setCancelable(false)
                        .setNegativeButton("Ок",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });
                builder.create().show();
            }
        }
    }

    protected void initView(){

        registrationLink.setText(Html.fromHtml("Нет аккаунта? " + "<a href=\"\">Зарегистрироваться</a> "));

        if (email.getEditText() != null && password.getEditText() != null) {
            email.getEditText().addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    email.setError("");
                }

                @Override
                public void afterTextChanged(Editable editable) {
                }
            });


            password.getEditText().addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    password.setError("");
                }

                @Override
                public void afterTextChanged(Editable editable) {
                }
            });
        } else throw new RuntimeException("No view in edit text");

        forgetPassword.setText(Html.fromHtml("<a href=\"\">Забыли пароль?</a> "));

        handlerSetButtonEnable = new LoginHandler(this, progressBarBetnbedLogin, login, email, password);

    }

    public void openRegistrationAcitivity(int type ){
        Intent intent = new Intent(this, LoginCreateAccountActivity.class);
        intent.putExtra("type", type);
        startActivityForResult(intent, REGISTRATION_CODE);
    }

    protected void login(String userLogin, String userPassword, int idAccount, int touristId, int hostId, int hotelId) {
        Heap.touristId = touristId;
        Heap.hostId = hostId;
        Heap.hotelId = hotelId;
        SettingsController.setTagAccountUserId(idAccount);
        SettingsController.setTagAccountLogin(userLogin);
        SettingsController.setTagAccountPassword(userPassword);


    }

}
