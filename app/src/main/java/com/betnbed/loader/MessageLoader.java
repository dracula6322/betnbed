package com.betnbed.loader;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;
import android.util.Log;

import com.betnbed.controller.MessageController;
import com.betnbed.model.Message;

import java.util.List;

public class MessageLoader extends AsyncTaskLoader<List<Message>> {

    public MessageLoader(Context context) {
        super(context);
    }

    @Override public List<Message> loadInBackground() {
        MessageController.downloadMessages();
        return MessageController.getMessages();
    }

    @Override protected void onStartLoading() {
        Log.v("TAG", "onStartLoading");
        super.onStartLoading();
        if (takeContentChanged())
            forceLoad();
    }
}
