package com.betnbed;

import android.os.Build;
import android.util.Log;

import com.betnbed.controller.EncryptController;
import com.betnbed.model.Request;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class RequestConstructor {

    public static String CHello() {

        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("type", "CHello");
            jsonObject.put("data", new JSONArray().put(2).put(System.currentTimeMillis() / 1000L).put(1).put(Build.VERSION.RELEASE).put(BuildConfig.VERSION_a).put(BuildConfig.VERSION_b).put(BuildConfig.VERSION_c));
        } catch (JSONException e) {
            Log.v("TAG", e.toString());
        }
        return jsonObject.toString();
    }

    public static String CGetInvites(JSONArray data) {

        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("type", "CGetInvites");
            jsonObject.put("data", data);
        } catch (JSONException e) {
            Log.v("TAG", e.toString());
        }
        return jsonObject.toString();
    }

    public static String CGetImages(int typeId, int idImage) {

        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("type", "CGetImages");
            jsonObject.put("data", new JSONArray().put(typeId).put(idImage));
        } catch (JSONException e) {
            Log.v("TAG", e.toString());
        }
        return jsonObject.toString();
    }

    public static String CNewSession(String hash) {

        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("type", "CNewSession");
            jsonObject.put("data", hash);
        } catch (JSONException e) {
            Log.v("TAG", e.toString());
        }
        return jsonObject.toString();
    }

    public static String HIsSubscribed() {

        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("type", "HIsSubscribed");
            jsonObject.put("data", 1);
        } catch (JSONException e) {
            Log.v("TAG", e.toString());
        }
        return jsonObject.toString();

    }

    public static String CChangeTourist(JSONObject mass) {

        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("type", "CChangeTourist");
            jsonObject.put("data", mass);
        } catch (JSONException e) {
            Log.v("TAG", e.toString());
        }
        return jsonObject.toString();

    }

    public static String CDeleteImage(int id) {

        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("type", "CDeleteImage");
            jsonObject.put("data", id);
        } catch (JSONException e) {
            Log.v("TAG", e.toString());
        }
        return jsonObject.toString();

        //return "{\"type\":\"CDeleteImage\",\"data\":" + String.valueOf(id) + '}';
    }

    public static String CChangeHotel(JSONObject mass) {

        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("type", "CChangeHotel");
            jsonObject.put("data", mass);
        } catch (JSONException e) {
            Log.v("TAG", e.toString());
        }
        return jsonObject.toString();

        //return "{\"type\":\"CChangeHotel\",\"data\":" + mass + '}';
    }

    public static String CChangeHouse(JSONObject mass) {

        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("type", "CChangeHouse");
            jsonObject.put("data", mass);
        } catch (JSONException e) {
            Log.v("TAG", e.toString());
        }
        return jsonObject.toString();

        //return "{\"type\":\"CChangeHouse\",\"data\":" + mass + '}';
    }

    public static String CKey(String c2, String encryptGood) {

        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("type", "CKey");
            jsonObject.put("data", new JSONArray().put(c2).put(encryptGood));
        } catch (JSONException e) {
            Log.v("TAG", e.toString());
        }
        return jsonObject.toString();

        //return "{\"type\":\"CKey\",\"data\":[\"" + c2 + "\",\"" + encryptGood + "\"]}";
    }

    public static String CPassReset(String email) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("type", "CPassReset");
            jsonObject.put("data", EncryptController.encryptAES(new JSONObject().put("mail", email).toString()));

        } catch (JSONException e) {
            Log.v("TAG", e.toString());
        }
        Log.v("TAG", jsonObject.toString());
        return jsonObject.toString();
    }

    public static String CPassChange(String oldPassword, String newPassword) {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("type", "CPassChange");
            jsonObject.put("data", EncryptController.encryptAES(new JSONObject().put("old", oldPassword).put("new", newPassword).toString()));

        } catch (JSONException e) {
            Log.v("TAG", e.toString());
        }

        return jsonObject.toString();

    }

    public static String CReg(String login, String password, int type) {

        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("type", "CReg");
            jsonObject.put("data", EncryptController.encryptAES(new JSONObject().put("login", login).put("pass", password).put("type", type).toString()));
        } catch (JSONException e) {
            Log.v("TAG", e.toString());
        }
        return jsonObject.toString();

        //return "{\"type\":\"CReg\",\"data\":\"" + EncryptController.encryptAES("{\"login\":\"" + login + "\",\"pass\":\"" + password + "\",\"type\":" + String.valueOf(type) + "}") + "\"}";
    }

    public static String CLogin(String login, String password) {
        String token = "";
        try {
            token = FirebaseInstanceId.getInstance().getToken();
            Log.v("TAG", "fcm token = " + token);
        } catch (Exception ignore) {
        }

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("type", "CLogin");
            jsonObject.put("data", EncryptController.encryptAES(new JSONObject().put("type", 0).put("login", login).put("pass", password).put("push", token).toString()));

        } catch (JSONException e) {
            Log.v("TAG", e.toString());
        }

        Log.v("TAG", jsonObject.toString());
        return jsonObject.toString();

        //return "{\"type\":\"CLogin\",\"data\":\"" + EncryptController.encryptAES("{\"type\":" + 0 + ",\"login\":\"" + login + "\",\"pass\":\"" + password + "\",\"push\":\"" + token + "\"}") + "\"}";
    }

    public static String CLogin(int id, final String token) {
        String firebaseToken = "";
        try {
            firebaseToken = FirebaseInstanceId.getInstance().getToken();
            Log.v("TAG", "fcm token = " + firebaseToken);
        } catch (Exception e) {
            Log.v("TAG", "error fcm token = " + firebaseToken);
        }

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("type", "CLogin");
            JSONObject jsonObject1 = new JSONObject().put("type", 1).put("id", id).put("token", token).put("push", firebaseToken);
            Log.v("TAG", jsonObject1.toString());
            jsonObject.put("data", EncryptController.encryptAES(jsonObject1.toString()));

        } catch (JSONException e) {
            Log.v("TAG", e.toString());
        }

        return jsonObject.toString();

        //return "{\"type\":\"CLogin\",\"data\":\"" + EncryptController.encryptAES("{\"type\":" + 1 + ",\"id\":" + String.valueOf(id) + ",\"token\":\"" + token + "\",\"push\":\"" + firebaseToken + "\"}") + "\"}";
    }

    public static String CNewRequest(Request i) {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("type", "CNewRequest");
            jsonObject.put("data", new JSONArray()
                    .put(i.getPlace())
                    .put(i.getArrival())
                    .put(i.getDeparture())
                    .put(i.getMoney())
                    .put(i.getCurrency())
                    .put(i.getPeople())
                    .put(i.getSpecial())
                    .put(i.getHotelType())
                    .put(i.getInternet())
                    .put(i.getBreakfast())
                    .put(i.getParking())
                    .put(i.getMedia())
                    .put(i.getShower())
                    .put(i.getKitchen())
                    .put(i.getPets())
                    .put(i.getServices())
                    .put(i.getMoneyType())


            );

        } catch (JSONException e) {
            Log.v("TAG", e.toString());
        }

        return jsonObject.toString();

    }

    public static String CImagePart(String hash, int idPart, String valuePart) {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("type", "CImagePart");
            jsonObject.put("data", new JSONArray().put(hash).put(idPart).put(valuePart));

        } catch (JSONException e) {
            Log.v("TAG", e.toString());
        }

        return jsonObject.toString();

        //return "{\"type\":\"CImagePart\",\"data\":[\"" + hash + "\"," + String.valueOf(idPart) + ",\"" + valuePart + "\"]}";
    }

    public static String CUploadImage(int size, int type, String sha256) {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("type", "CUploadImage");
            jsonObject.put("data", new JSONArray().put(size).put("jpg").put(type).put(sha256));

        } catch (JSONException e) {
            Log.v("TAG", e.toString());
        }

        return jsonObject.toString();

        //return "{\"type\":\"CUploadImage\",\"data\":[" + String.valueOf(size) + ",\"" + "jpg" + "\"," + String.valueOf(type) + ",\"" + sha256 + "\"]}";
    }

    public static String HGetRequests(JSONArray data) {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("type", "HGetRequests");
            jsonObject.put("data", data);

        } catch (JSONException e) {
            Log.v("TAG", e.toString());
        }

        return jsonObject.toString();

        //return "{\"type\":\"HGetRequests\",\"data\":[6322," + data + "]}";
    }

    public static String HGetRequestsIds(int mask) {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("type", "HGetRequestsIds");
            jsonObject.put("data", mask);

        } catch (JSONException e) {
            Log.v("TAG", e.toString());
        }

        return jsonObject.toString();

        //return "{\"type\":\"HGetRequestsIds\",\"data\":" + String.valueOf(mask) + '}';
    }

    public static String HInvite(int clientId, int requestId, String comment) {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("type", "HInvite");
            jsonObject.put("data", new JSONArray().put(clientId).put(requestId).put(comment));

        } catch (JSONException e) {
            Log.v("TAG", e.toString());
        }

        return jsonObject.toString();

        //return "{\"type\":\"HInvite\",\"data\":[" + String.valueOf(clientId) + ',' + String.valueOf(requestId) + ",\"" + comment + "\"]}";
    }

    public static String CLogout() {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("type", "CLogout");
            jsonObject.put("data", 1);

        } catch (JSONException e) {
            Log.v("TAG", e.toString());
        }

        return jsonObject.toString();

        //return "{\"type\":\"CLogout\",\"data\":1}";
    }

    public static String CGetRequestsIds(int requestId) {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("type", "CGetRequestsIds");
            jsonObject.put("data", requestId);

        } catch (JSONException e) {
            Log.v("TAG", e.toString());
        }

        return jsonObject.toString();

        //return "{\"type\":\"CGetRequestsIds\",\"data\":" + String.valueOf(requestId) + '}';
    }

    public static String CGetRequests(JSONArray data) {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("type", "CGetRequests");
            jsonObject.put("data", data);

        } catch (JSONException e) {
            Log.v("TAG", e.toString());
        }

        return jsonObject.toString();

        //return "{\"type\":\"CGetRequests\",\"data\":[" + data + "]}";
    }

    public static String CDeleteRequest(int limit) {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("type", "CDeleteRequest");
            jsonObject.put("data", limit);

        } catch (JSONException e) {
            Log.v("TAG", e.toString());
        }

        return jsonObject.toString();

        //return "{\"type\":\"CDeleteRequest\",\"data\":" + String.valueOf(limit) + '}';
    }

    public static String CGetTourist(int id) {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("type", "CGetTourist");
            jsonObject.put("data", id);

        } catch (JSONException e) {
            Log.v("TAG", e.toString());
        }

        return jsonObject.toString();

        ///return "{\"type\":\"CGetTourist\",\"data\":" + String.valueOf(id) + '}';
    }

    public static String CInviteStatus(int requestId, int inviteId, int status) {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("type", "CInviteStatus");
            jsonObject.put("data", new JSONArray().put(requestId).put(inviteId).put(status));

        } catch (JSONException e) {
            Log.v("TAG", e.toString());
        }

        return jsonObject.toString();

        //return "{\"type\":\"CInviteStatus\",\"data\":[" + String.valueOf(requestId) + ',' + String.valueOf(inviteId) + ',' + String.valueOf(status) + "]}";
    }

    public static String CGetProfile(int type, int id) {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("type", "CGetProfile");
            jsonObject.put("data", new JSONArray().put(type).put(id));

        } catch (JSONException e) {
            Log.v("TAG", e.toString());
        }

        return jsonObject.toString();

        //return "{\"type\":\"CGetProfile\",\"data\":[" + String.valueOf(type) + ',' + String.valueOf(id) + "]}";
    }

    public static String HIgnoreRequest(int userId, int requestId) {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("type", "HIgnoreRequest");
            jsonObject.put("data", new JSONArray().put(userId).put(requestId));

        } catch (JSONException e) {
            Log.v("TAG", e.toString());
        }

        return jsonObject.toString();

        //return "{\"type\":\"HIgnoreRequest\",\"data\":[" + String.valueOf(userId) + ',' + String.valueOf(requestId) + "]}";
    }

    public static String HDeleteInvite(int userId, int requestId, int inviteId, String comment) {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("type", "HDeleteInvite");
            jsonObject.put("data", new JSONArray().put(userId).put(requestId).put(inviteId).put(comment));

        } catch (JSONException e) {
            Log.v("TAG", e.toString());
        }

        return jsonObject.toString();

        //return "{\"type\":\"HDeleteInvite\",\"data\":[" + String.valueOf(userId) + ',' + String.valueOf(requestId) + ',' + String.valueOf(inviteId) + ",\"" + comment + "\"]}";
    }

    public static String CGetMessagesIds(int type) {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("type", "CGetMessagesIds");
            jsonObject.put("data",type);

        } catch (JSONException e) {
            Log.v("TAG", e.toString());
        }

        return jsonObject.toString();

        //return "{\"type\":\"CGetMessagesIds\",\"data\":" + String.valueOf(type) + "}";
    }

    public static String CGetMessages(JSONObject data) {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("type", "CGetMessages");
            jsonObject.put("data", data);

        } catch (JSONException e) {
            Log.v("TAG", e.toString());
        }

        return jsonObject.toString();

        //return "{\"type\":\"CGetMessages\",\"data\":{\"ids\":" + data + "}}";}

    }

    public static String CDeleteMessages(JSONArray data) {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("type", "CDeleteMessages");
            jsonObject.put("data", data);

        } catch (JSONException e) {
            Log.v("TAG", e.toString());
        }

        return jsonObject.toString();

        //return "{\"type\":\"CDeleteMessages\",\"data\":" + data + "}";}
    }
}