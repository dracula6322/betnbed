package com.betnbed;

import java.text.SimpleDateFormat;
import java.util.Locale;

public class SC {

    public static final SimpleDateFormat df = new SimpleDateFormat("dd MMM yyyy HH:mm", Locale.getDefault());

    public static final SimpleDateFormat df_with_users_offset = new SimpleDateFormat("dd MMM yyyy HH:mm", Locale.getDefault());

    public static final SimpleDateFormat dfShort = new SimpleDateFormat("dd MMM yyyy", Locale.getDefault());

    public static final SimpleDateFormat dfDayBirthday = new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault());


    public static final int[] wayToPayIdImage = {
            R.drawable.png_both_blue, R.drawable.png_credit_card_blue, R.drawable.png_cash_blue
    };


}