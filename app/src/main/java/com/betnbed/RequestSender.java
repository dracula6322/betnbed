package com.betnbed;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.betnbed.activity.NavigationActivity;
import com.betnbed.controller.EncryptController;
import com.betnbed.controller.MessageController;
import com.betnbed.controller.SettingsController;
import com.betnbed.exception.BetnbedException;
import com.betnbed.interfaces.SocketInterface;
import com.betnbed.model.BaseModel;
import com.betnbed.model.Image;
import com.betnbed.model.Invite;
import com.betnbed.model.Message;
import com.betnbed.model.Request;
import com.betnbed.model.User;
import com.betnbed.model.UserAndHotelModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigInteger;
import java.net.Socket;
import java.util.HashMap;
import java.util.Stack;

import io.reactivex.Single;

public class RequestSender extends AsyncTask<Void, Integer, Void> {

    public static final int ZERO = 0;
    public static final int CONNECTING = 1;
    public static final int WITH_LOGIN_TOURIST = 2;
    public static final int WITH_LOGIN_HOTEL = 3;
    public static final int WITH_OUT_LOGIN = 4;
    public static final int DISCONNECTED = 5;
    public static final int OLD_VERSION = 6;
    private volatile int state;

    private OutputStream outputStream;
    private InputStream inputStream;

    private String toShow;
    private final Context context;
    private final Stack<String> stackRequest;
    private final HashMap<String, String> answers;

    public RequestSender(final Context context) {


        this.stackRequest = new Stack<>();
        this.answers = new HashMap<>();
        this.context = context;

        execute();

    }

    public Single<JSONObject> sendRequestWithObservable(String request, String type) {

        answers.remove("SProfileChanged");
        answers.remove("SError");

        if (type == null) {
            stackRequest.add(request);
            return Single.never();
        }


        if (state == DISCONNECTED) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    connectToServer();
                }
            }).start();
        }

        if (!type.equals("SNewSession") && !type.equals("SHello") && !type.equals("SKey") && !type.equals("SLogin")) {
            while (state == ZERO || state == CONNECTING) {
                try {
                    Thread.sleep(1);

                } catch (InterruptedException e) {
                    Log.v("TAG", e.toString());
                }
            }
        }

        if (state != DISCONNECTED) {
            stackRequest.add(request);
            while (answers.get(type) == null && answers.get("SError") == null && state != DISCONNECTED) {
                try {
                    Thread.sleep(1);

                } catch (InterruptedException e) {
                    Log.v("TAG", e.toString());
                }
            }

            if (answers.get(type) != null) {
                String answer = answers.get(type);
                answers.remove(type);
                try {
                    JSONObject jsonObject = new JSONObject(answer);
                    return Single.just(jsonObject);
                } catch (Exception e) {
                    return Single.error(e);
                }



                /*socketInterface.onSuccess(answers.get(type));
                answers.remove(type);
                return;*/
            }

            if (state == DISCONNECTED)
                return Single.error(new BetnbedException(101));


            if (answers.get("SError") != null) {

                String errorData = answers.remove("SError");
                if (errorData != null) {
                    try {
                        return Single.error(new BetnbedException(new JSONObject(errorData).getJSONObject("data").getInt("id")));
                    } catch (JSONException e) {
                        return Single.error(e);
                    }
                }

                //socketInterface.onError(idError);
            }

        } else return Single.error(new RuntimeException("Нет интернета 101"));
        return Single.never();
    }

    public void sendRequest(String request, String type, SocketInterface socketInterface) {

        answers.remove("SProfileChanged");
        answers.remove("SError");

        if (type == null) {
            stackRequest.add(request);
            return;
        }


        if (state == DISCONNECTED) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    connectToServer();
                }
            }).start();
        }

        if (!type.equals("SNewSession") && !type.equals("SHello") && !type.equals("SKey") && !type.equals("SLogin")) {
            while (state == ZERO || state == CONNECTING) {
                try {
                    Thread.sleep(1);

                } catch (InterruptedException e) {
                    Log.v("TAG", e.toString());
                }
            }
        }

        if (state != DISCONNECTED) {
            stackRequest.add(request);
            while (answers.get(type) == null && answers.get("SError") == null && state != DISCONNECTED) {
                try {
                    Thread.sleep(1);

                } catch (InterruptedException e) {
                    Log.v("TAG", e.toString());
                }
            }

            if (answers.get(type) != null) {
                socketInterface.onSuccess(answers.get(type));
                answers.remove(type);
                return;
            }

            if (state == DISCONNECTED) {
                socketInterface.onError(101);
                return;
            }

            if (answers.get("SError") != null) {
                int idError;
                try {
                    idError = new JSONObject(answers.get("SError")).getJSONObject("data").getInt("id");
                } catch (JSONException e) {
                    Log.v("TAG", e.toString());
                    idError = 1;
                }
                socketInterface.onError(idError);
                answers.remove("SError");
            }
        } else socketInterface.onError(101);
    }

    @Override
    protected Void doInBackground(Void... arg0) {
        Log.v("TAG", "doInBackground is started");

        new Thread(new Runnable() {
            @Override
            public void run() {
                connectToServer();
            }
        }).start();

        toShow = "";
        while (!isCancelled()) {

            if (state != DISCONNECTED && state != ZERO) {

                try {
                    Thread.sleep(50);
                } catch (InterruptedException e) {
                    Log.v("TAG", "1 " + e.toString());
                }

                if (!stackRequest.isEmpty()) {
                    Log.v("SEND", stackRequest.firstElement());
                    try {
                        Thread.sleep(50);
                    } catch (InterruptedException e) {
                        Log.v("TAG", "2 " + e.toString());
                    }
                    try {

                        byte[] request = stackRequest.firstElement().getBytes();
                        stackRequest.remove(0);
                        outputStream.write(request);

                    } catch (IOException e) {
                        Log.v("TAG", e.toString());
                        toShow = ("{\"type\":\"SError\",\"data\":{\"id\":101}}");
                        handlerMessage();
                        setState(DISCONNECTED);
                        //cancel(true);

                    }
                }

                if (!isCancelled()) {

                    try {
                        Thread.sleep(50);
                    } catch (InterruptedException e) {
                        Log.v("TAG", "3 " + e.toString());
                    }
                    try {
                        while (inputStream.available() > 0) {
                            byte[] buffer = new byte[inputStream.available()];
                            inputStream.read(buffer);
                            String part = new String(buffer);
                            toShow += part;

                            handlerMessage();
                        }
                    } catch (Exception e) {
                        Log.v("TAG", e.toString());
                        setState(DISCONNECTED);
                        //cancel(true);
                    }
                }
            }
        }
        return null;
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
        Log.v("TAG", "onCancelled is started");
        setState(DISCONNECTED);

    }


    public void connectToServer() {
        Log.v("TAG", "connectToServer is started");
        setState(ZERO);
        try {
            Socket socket = new Socket(BuildConfig.serverLink, BuildConfig.serverPort);
            outputStream = socket.getOutputStream();
            inputStream = socket.getInputStream();
            setState(CONNECTING);

        } catch (IOException e) {
            Log.v("TAG", "connectToServer error = " + e.toString());
            setState(DISCONNECTED);
        }

        /*if (state != DISCONNECTED) {

            sendRequestWithObservable(RequestConstructor.CHello(), "SNewSession")
                    .flatMap(new Function<JSONObject, ObservableSource<String>>() {
                        @Override public ObservableSource<String> apply(@NonNull JSONObject jsonObject) throws Exception {
                            String encryptData = EncryptController.rsaDecrypt(jsonObject.getString("data"));
                            JSONArray jsonArray = new JSONArray(encryptData.substring(encryptData.indexOf('['), encryptData.length()));
                            if (jsonArray.getInt(3) == 1) {
                                setState(OLD_VERSION);
                                return Observable.error(new BetnbedException(75));
                                //throw new RuntimeException("Старая версия");
                            }
                            String hash = jsonArray.getString(2);
                            return Observable.just(hash);
                        }
                    })
                    .flatMap(new Function<String, ObservableSource<JSONObject>>() {
                        @Override public ObservableSource<JSONObject> apply(@NonNull String s) throws Exception {
                            return sendRequestWithObservable(RequestConstructor.CNewSession(s), "SHello");
                        }
                    })
                    .flatMap(new Function<JSONObject, ObservableSource<String>>() {
                        @Override public ObservableSource<String> apply(@NonNull JSONObject jsonObject) throws Exception {
                            JSONArray data = jsonObject.getJSONArray("data");
                            BigInteger g = new BigInteger(data.getString(1));
                            BigInteger p = new BigInteger(data.getString(2));
                            BigInteger s = new BigInteger(data.getString(3));
                            BigInteger c1 = EncryptController.generateSecretKey();
                            BigInteger c2 = g.modPow(c1, p);
                            BigInteger c3 = s.modPow(c1, p);
                            EncryptController.setKey(EncryptController.md5Custom(c3.toString()));
                            return Observable.just(c2.toString());
                        }
                    })
                    .flatMap(new Function<String, ObservableSource<JSONObject>>() {
                        @Override public ObservableSource<JSONObject> apply(@NonNull String s) throws Exception {
                            return sendRequestWithObservable(RequestConstructor.CKey(s, EncryptController.encryptAES("good")), "SKey");
                        }
                    })
                    .flatMap(new Function<JSONObject, ObservableSource<JSONObject>>() {
                        @Override public ObservableSource<JSONObject> apply(@NonNull JSONObject jsonObject) throws Exception {

                            if (!SettingsController.getAccountLogin().isEmpty() && !SettingsController.getAccountPassword().isEmpty()) {
                                return sendRequestWithObservable(RequestConstructor.CLogin(SettingsController.getAccountLogin(), SettingsController.getAccountPassword()), "SLogin");
                            }
                            if (SettingsController.getAccountLoginVKUserId() != 0 && !SettingsController.getAccountLoginVKUserToken().isEmpty()) {
                                return sendRequestWithObservable(RequestConstructor.CLogin(SettingsController.getAccountLoginVKUserId(), SettingsController.getAccountLoginVKUserToken()), "SLogin");
                            }
                            setState(WITH_OUT_LOGIN);
                            return Observable.empty();
                        }
                    })
                    .doOnNext(new Consumer<JSONObject>() {
                        @Override public void accept(@NonNull JSONObject jsonObject) throws Exception {

                            JSONArray jsonArray = jsonObject.getJSONArray("data");
                            SettingsController.setTagAccountUserId(jsonArray.getInt(0));
                            Heap.touristId = jsonArray.getInt(1);
                            Heap.hostId = jsonArray.getInt(2);
                            Heap.hotelId = jsonArray.getInt(3);

                            if (jsonArray.getInt(4) == 1)
                                setState(WITH_LOGIN_TOURIST);
                            else setState(WITH_LOGIN_HOTEL);
                        }
                    })
                    .subscribe(new Observer<JSONObject>() {
                        @Override public void onSubscribe(Disposable d) {

                        }

                        @Override public void onNext(JSONObject s) {
                        }

                        @Override public void onError(Throwable e) {
                            Log.v("TAG", "onError " + e.toString());

                            if (e instanceof BetnbedException) {
                                BetnbedException error = (BetnbedException) e;

                                switch (error.getErrorCode()) {
                                    case 75:
                                        setState(OLD_VERSION);
                                        break;
                                }

                                return;
                            }

                            setState(DISCONNECTED);

                        }

                        @Override public void onComplete() {
                            Log.v("TAG", "onComplete");
                        }
                    });


        }*/






        if (state != DISCONNECTED) {
            sendRequest(RequestConstructor.CHello(), "SNewSession", new SocketInterface() {
                @Override
                public void onSuccess(String request) {

                    try {
                        String encryptData = EncryptController.rsaDecrypt(new JSONObject(request).getString("data"));
                        JSONArray jsonArray = new JSONArray(encryptData.substring(encryptData.indexOf('['), encryptData.length()));
                        if (jsonArray.getInt(3) == 1) {
                            setState(OLD_VERSION);
                            return;
                        }
                        String hash = jsonArray.getString(2);

                        sendRequest(RequestConstructor.CNewSession(hash), "SHello", new SocketInterface() {
                            @Override
                            public void onSuccess(String request) {
                                try {
                                    JSONArray data = new JSONObject(request).getJSONArray("data");
                                    BigInteger g = new BigInteger(data.getString(1));
                                    BigInteger p = new BigInteger(data.getString(2));
                                    BigInteger s = new BigInteger(data.getString(3));
                                    BigInteger c1 = EncryptController.generateSecretKey();
                                    BigInteger c2 = g.modPow(c1, p);
                                    BigInteger c3 = s.modPow(c1, p);
                                    EncryptController.setKey(EncryptController.md5Custom(c3.toString()));

                                    sendRequest(RequestConstructor.CKey(c2.toString(), EncryptController.encryptAES("good")), "SKey", new SocketInterface() {
                                        @Override
                                        public void onSuccess(String request) {
                                            boolean weSendLogin = false;

                                            SocketInterface socketInterface = new SocketInterface() {
                                                @Override
                                                public void onSuccess(String request) {
                                                    try {
                                                        JSONArray jsonArray = new JSONObject(request).getJSONArray("data");
                                                        SettingsController.setTagAccountUserId(jsonArray.getInt(0));
                                                        Heap.touristId = jsonArray.getInt(1);
                                                        Heap.hostId = jsonArray.getInt(2);
                                                        Heap.hotelId = jsonArray.getInt(3);

                                                        if (jsonArray.getInt(4) == 1)
                                                            setState(WITH_LOGIN_TOURIST);
                                                        else setState(WITH_LOGIN_HOTEL);

                                                    } catch (JSONException e) {
                                                        setState(WITH_OUT_LOGIN);
                                                    }
                                                }

                                                @Override
                                                public void onError(int error) {
                                                    setState(WITH_OUT_LOGIN);
                                                }
                                            };

                                            if (!SettingsController.getAccountLogin().isEmpty() && !SettingsController.getAccountPassword().isEmpty()) {
                                                sendRequest(RequestConstructor.CLogin(SettingsController.getAccountLogin(), SettingsController.getAccountPassword()), "SLogin", socketInterface);
                                                weSendLogin = true;
                                            }
                                            if (SettingsController.getAccountLoginVKUserId() != 0 && !SettingsController.getAccountLoginVKUserToken().isEmpty()) {
                                                sendRequest(RequestConstructor.CLogin(SettingsController.getAccountLoginVKUserId(), SettingsController.getAccountLoginVKUserToken()), "SLogin", socketInterface);
                                                weSendLogin = true;
                                            }
                                            if (!weSendLogin) setState(WITH_OUT_LOGIN);
                                        }

                                        @Override
                                        public void onError(int error) {
                                            setState(WITH_OUT_LOGIN);
                                        }
                                    });
                                } catch (JSONException e) { setState(WITH_OUT_LOGIN);}
                            }

                            @Override
                            public void onError(int error) {

                                Log.v("TAG", "We get here error " + error);
                                setState(DISCONNECTED);
                            }
                        });

                    } catch (Exception e) {
                        Log.v("TAG", "error 32 = " + e.toString());
                        setState(DISCONNECTED);
                    }
                }

                @Override
                public void onError(int error) {
                    Log.v("TAG", "We get error " + error);
                    switch (error) {
                        case 101:
                            setState(DISCONNECTED);
                            break;
                        case 75:
                            setState(OLD_VERSION);
                            break;
                    }

                }
            });

        }
        LocalBroadcastManager.getInstance(context).sendBroadcast(new Intent(NavigationActivity.tagLoginIsEnd).putExtra("state", getState()));
    }

    private void handlerMessage() {
        Log.v("GET", toShow);

        try {
            while (checkIsHaveRequestInToShow() > 0) {
                int point = checkIsHaveRequestInToShow();
                if (point == 0)
                    return;
                JSONObject jsonRequest = new JSONObject(toShow.substring(0, point));
                String putInHashMap = toShow.substring(0, point);
                String type = jsonRequest.getString("type");
                toShow = toShow.substring(point, toShow.length());

                if (type.equals("SImageUploaded")) {
                    JSONArray array = jsonRequest.getJSONArray("data");
                    switch (array.getInt(2)) {
                        case 1:
                            Heap.accountUser.addImage(new Image(Heap.imagePrefix + array.getString(4), array.getInt(1)));
                            break;
                        case 2:
                            Heap.accountHotel.addImage(new Image(Heap.imagePrefix + array.getString(4), array.getInt(1)));
                            break;
                        case 3:
                            Heap.accountHotel.addImage(new Image(Heap.imagePrefix + array.getString(4), array.getInt(1)));
                            break;
                    }
                }


                if (type.equals("GRequestDeleted"))
                    Heap.accountHotel.requestHostController.GRequestDeleted(jsonRequest.getJSONArray("data").getInt(0), jsonRequest.getJSONArray("data").getInt(1));

                if (type.equals("SLogout")) {
                    NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                    notificationManager.cancelAll();
                }


                if (type.equals("GNewRequest")) {
                    JSONArray data = jsonRequest.getJSONArray("data");

                    BaseModel.BaseBuilder baseBuilder = new BaseModel.BaseBuilder()
                            .hotelType(data.getInt(10))
                            .internet(data.getInt(11))
                            .breakfast(data.getInt(12))
                            .parking(data.getInt(13))
                            .media(data.getInt(14))
                            .shower(data.getInt(15))
                            .kitchen(data.getInt(16))
                            .pets(data.getInt(17))
                            .services(data.getInt(18))
                            .moneyType(data.getInt(19));

                    Request request = new Request.Builder()
                            .catsAndDogs(baseBuilder)
                            .user(new User.Builder().firstName(data.getString(20)).lastName(data.getString(21)).photo(data.getString(22)).build())
                            .senderId(data.getInt(0))
                            .requestId(data.getInt(1))
                            .arrival(data.getLong(2))
                            .departure(data.getLong(3))
                            .money(data.getInt(4))
                            .currency(data.getString(5))
                            .people(data.getInt(6))
                            .special(data.getString(7))
                            .isActive(false)
                            .build();

                    Heap.accountHotel.requestHostController.GNewRequest(request);

                }


                if (type.equals("SError")) {
                    JSONObject jsonObject1 = jsonRequest.getJSONObject("data");
                    int idError = jsonObject1.getInt("id");
                    switch (idError) {
                        case 55:
                            //GridImageActivity.handlerError.sendEmptyMessage(55);
                            break;
                        case 76:
                            continue;
                    }
                }

                if (type.equals("SProfileChanged")) {
                    JSONArray jsonArray = jsonRequest.getJSONArray("data");

                    UserAndHotelModel profile;

                    if (jsonArray.getInt(0) == 1)
                        profile = Heap.accountUser;
                    else profile = Heap.accountHotel;

                    JSONObject jsonObject = jsonArray.getJSONObject(2);
                    if (jsonObject.has("photo"))
                        profile.setPhoto(jsonObject.getString("photo"));

                }


                if (type.equals("GInviteStatus")) {
                    JSONArray jsonArray = jsonRequest.getJSONArray("data");
                    Heap.accountHotel.requestHostController.GInviteStatus(jsonArray.getInt(0), jsonArray.getInt(1), jsonArray.getInt(3));
                }

                if (type.equals("GApproveStatus")) {
                    JSONArray jsonArray = jsonRequest.getJSONArray("data");
                    Heap.accountHotel.setApprove(jsonArray.getInt(1));

                }

                if (type.equals("GRequestCompleted")) {
                    JSONArray jsonArray = jsonRequest.getJSONArray("data");
                    Heap.accountHotel.requestHostController.GRequestCompleted(jsonArray.getInt(0), jsonArray.getInt(1));
                }

                if (type.equals("SNewInvite")) {

                    JSONArray data = jsonRequest.getJSONArray("data");

                    Invite invite = new Invite.Builder()
                            .hotelId(data.getInt(0))
                            .type(data.getInt(1))
                            .requestId(data.getInt(2))
                            .inviteId(data.getInt(3))
                            .comment(data.getString(4))
                            .title(data.getString(5))
                            .photo(data.getString(6))
                            .address(data.getString(7))
                            .city(data.getString(8))
                            .state(data.getInt(9))
                            .build();

                    Heap.accountUser.getRequestTouristController().SNewInvite(invite);
                    //onProgressUpdate(WE_GET_NEW_INVITE);

                }

                if (type.equals("SInviteDeleted")) {
                    JSONArray jsonArray = jsonRequest.getJSONArray("data");
                    Heap.accountUser.getRequestTouristController().SInviteDeleted(jsonArray.getInt(0), jsonArray.getInt(1), jsonArray.getString(2));
                }


                if (type.equals("SKeepAlive")) sendRequest("{\"type\":\"CKeepAlive\",\"data\":" + jsonRequest.getInt("data") + "}", null, null);

                if (type.equals("SNewMessage")) {
                    try {
                        String afterEncrypt = EncryptController.decryptAES(jsonRequest.getString("data"));
                        Log.v("TAG", "AfterEncrypt = " + afterEncrypt);
                        JSONObject jsonObject = new JSONObject(afterEncrypt);
                        MessageController.addMessage(new Message(jsonObject.getInt("time"), jsonObject.getInt("id"), jsonObject.getString("text"), jsonObject.getInt("type") == 1 ? Message.STATE_UNREADED_MESSAGE : Message.STATE_UNREADED_PUSH));
                    } catch (Exception e) { Log.v("TAG", e.toString()); }
                }

                answers.put(jsonRequest.getString("type"), putInHashMap);
            }
        } catch (JSONException e) {
            Log.v("TAG", e.toString());
        }
    }

    private int checkIsHaveRequestInToShow() {
        if (toShow.length() == 0)
            return 0;

        int ii = 0;
        int openCount;
        for (openCount = 0; (openCount != 0 && ii < toShow.length()) || ii == 0; ii++) {
            if (toShow.charAt(ii) == '}')
                openCount--;
            if (toShow.charAt(ii) == '{')
                openCount++;
        }

        if (openCount != 0)
            return 0;
        else return ii;

    }

    public void setState(final int state) {this.state = state;}

    public int getState() { return this.state;}

    public OutputStream getOutputStream() {
        return outputStream;
    }

    public void setOutputStream(OutputStream outputStream) {
        this.outputStream = outputStream;
    }

    public InputStream getInputStream() {
        return inputStream;
    }

    public void setInputStream(InputStream inputStream) {
        this.inputStream = inputStream;
    }
}
