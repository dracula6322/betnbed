package com.betnbed.model;

public class Image {

    int imageId;
    String url;

    public Image(String url) {
        this(url, -1);
    }

    public Image(String url, int imageId) {
        setImageId(imageId);
        setUrl(url);
    }

    public int getImageId() {
        return imageId;
    }

    public void setImageId(int imageId) {
        this.imageId = imageId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
