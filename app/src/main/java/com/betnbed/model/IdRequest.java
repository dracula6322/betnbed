package com.betnbed.model;

public class IdRequest{

    private int idRequest;
    private int idUser;

    public IdRequest(int idUser, int idRequest) {
        this.idRequest = idRequest;
        this.idUser = idUser;
    }

    public int getIdRequest() {
        return idRequest;
    }

    public void setIdRequest(int idRequest) {
        this.idRequest = idRequest;
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public boolean equals(IdRequest obj) {
        return (idRequest == obj.idRequest) && (idUser == obj.idUser);
    }

    @Override
    public int hashCode() {
        return idRequest * idUser;
    }
}