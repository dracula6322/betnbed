package com.betnbed.model;

import java.util.ArrayList;

public class Request extends BaseModel {

    public static final int TOURIST_DELETE_REQUEST = 1;
    public static final int TOURIST_AGREED_ANOTHER_INVITE = 2;
    public static final int TOURIST_DELETE_OUR_INVITE = 3;

    // In builder

    private User user;

    private String place;
    private String currency;
    private String special;

    private long arrival;
    private long departure;
    private int requestId;
    private int senderId;
    private int money;
    private int people;
    private int mask;
    private int reasonInActive;
    private int inviteId;

    public boolean isSendId = false;
    public boolean isActive = false;
    // Generate from constructor

    private String hours = "";
    private String fullPlaceName = "";

    //



    private ArrayList<Invite> invites = new ArrayList<>();
    private ArrayList<Integer> invitesIds = new ArrayList<>();

    public Request(Request.Builder builder) {
        super(builder.catsAndDogs);

        setUser(builder.user);

        setPlace(builder.place);
        setCurrency(builder.currency);
        setSpecial(builder.special);

        setDepartureAndArrival(builder.departure, builder.arrival);
        setRequestId(builder.requestId);
        setSenderId(builder.senderId);
        setMoney(builder.money);
        setPeople(builder.people);
        setMask(builder.mask);
        setReasonInActive(builder.reasonInActive);
        setInviteId(builder.inviteId);

        setSendId(builder.isSendId);
        setActive(builder.isActive);

    }
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getPlace() {
        return place;
    }

    private void setPlace(String place) {
        this.place = place;
    }

    public long getArrival() {
        return arrival;
    }

    private void setArrival(long arrival) {
        this.arrival = arrival;
    }

    public long getDeparture() {
        return departure;
    }

    private void setDeparture(long departure) {
        this.departure = departure;
    }

    public int getMoney() {
        return money;
    }

    private void setMoney(int money) {
        this.money = money;
    }

    public String getCurrency() {
        return currency;
    }

    private void setCurrency(String currency) {
        this.currency = currency;
    }

    public int getPeople() {
        return people;
    }

    private void setPeople(int people) {
        this.people = people;
    }

    public String getSpecial() {
        return special;
    }

    private void setSpecial(String special) {
        this.special = special;
    }

    private void setHotel_type(int hotel_type) {
        this.hotelType = hotel_type;
    }

    public String getFullPlaceName() {
        return fullPlaceName;
    }

    public void setFullPlaceName(String fullPlaceName) {
        this.fullPlaceName = fullPlaceName;
    }

    public int getRequestId() {
        return requestId;
    }

    public void setRequestId(int requestId) {
        this.requestId = requestId;
    }

    public ArrayList<Invite> getInvites() {
        return invites;
    }

    public String getHours() {
        return hours;
    }

    public String getFirstName() {
        return user.getFirstName();
    }

    public void setFirstName(String firstName) {
        user.setFirstName(firstName);
    }

    public String getSecondName() {
        return user.getLastName();
    }

    public void setSecondName(String secondName) {
        user.setLastName(secondName);
    }

    public String getImage() {
        return user.getPhoto();
    }

    public void setImage(String image) {
        user.setPhoto(image);
    }

    public int getSenderId() {
        return senderId;
    }

    public void setSenderId(int senderId) {
        this.senderId = senderId;
    }

    public boolean isSendId() {
        return !isSendId;
    }

    public void setSendId(boolean sendId) {
        isSendId = sendId;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public int getReasonInActive() {
        return reasonInActive;
    }

    public void setReasonInActive(int reasonInActive) {
        this.reasonInActive = reasonInActive;
    }

    public ArrayList<Integer> getInvitesIds() {
        return invitesIds;
    }

    public int getMask() {
        return mask;
    }

    public void setMask(int mask) {
        this.mask = mask;
    }

    public int getInviteId() {
        return inviteId;
    }

    public void setInviteId(int inviteId) {
        this.inviteId = inviteId;
    }


    public boolean equals(IdRequest idRequest) {
        return (idRequest.getIdRequest() == requestId) && (idRequest.getIdUser() == senderId);
    }

    private void setDepartureAndArrival(long departure, long arrival) {
        setDeparture(departure);
        setArrival(arrival);
        long hours = ((departure - arrival) / 3600);
        if (hours > 23) {
            this.hours = hours / 24 + " " + "д." + " ";
            if (hours % 24 != 0)
                this.hours = hours / 24 + " " + "д." + " " + hours % 24 + " " + "час.";
        } else {
            this.hours = hours + " " + "час.";
        }
    }

    public static class Builder{

        BaseModel.BaseBuilder catsAndDogs;

        private User user;

        private String place;
        private String currency;
        private String special;

        private long arrival;
        private long departure;
        private int requestId;
        private int senderId;
        private int money;
        private int people;
        private int mask;
        private int reasonInActive;
        private int inviteId;
        private boolean isSendId;
        private boolean isActive;
        public Builder() {

        }

        public Builder catsAndDogs(BaseModel.BaseBuilder value) {
            this.catsAndDogs = value;
            return this;
        }

        public Builder user(User value) {
            this.user = value;
            return this;
        }

        public Builder place(String value) {
            this.place = value;
            return this;
        }

        public Builder currency(String value){
            this.currency = value;
            return this;
        }
        public Builder special(String value){
            this.special = value;
            return this;
        }

        public Builder arrival(long value) {
            this.arrival = value;
            return this;
        }

        public Builder departure(long value) {
            this.departure = value;
            return this;
        }

        public Builder requestId(int value) {
            this.requestId = value;
            return this;
        }

        public Builder senderId(int value) {
            this.senderId = value;
            return this;
        }

        public Builder money(int value) {
            this.money = value;
            return this;
        }

        public Builder people(int value) {
            this.people = value;
            return this;
        }

        public Builder mask(int value) {
            this.mask = value;
            return this;
        }

        public Builder reasonInActive(int value){
            this.reasonInActive = value;
            return this;
        }

        public Builder inviteId(int value){
            this.inviteId = value;
            return this;
        }

        public Builder isSendId(boolean value){
            this.isSendId = value;
            return this;
        }
        public Builder isActive(boolean value){
            this.isActive = value;
            return this;
        }

        public Request build() {
            return new Request(this);
        }

    }

    @Override public String toString() {
        return "Request{" +
                "user=" + user +
                ", place='" + place + '\'' +
                ", currency='" + currency + '\'' +
                ", special='" + special + '\'' +
                ", arrival=" + arrival +
                ", departure=" + departure +
                ", requestId=" + requestId +
                ", senderId=" + senderId +
                ", money=" + money +
                ", people=" + people +
                ", mask=" + mask +
                ", reasonInActive=" + reasonInActive +
                ", inviteId=" + inviteId +
                ", isSendId=" + isSendId +
                ", isActive=" + isActive +
                ", hours='" + hours + '\'' +
                ", fullPlaceName='" + fullPlaceName + '\'' +
                ", invites=" + invites +
                ", invitesIds=" + invitesIds +
                '}';
    }
}
