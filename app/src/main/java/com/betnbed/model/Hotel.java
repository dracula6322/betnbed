package com.betnbed.model;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import com.betnbed.Application;
import com.betnbed.controller.RequestHostController;
import com.betnbed.interfaces.ApproveStatusChanged;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;

public class Hotel extends UserAndHotelModel{
    ApproveStatusChanged approveStatusChanged;

    public static final int HOST_TYPE = 1;
    public static final int HOTEL_TYPE = 2;
    public static final int HOSTEL_TYPE = 4;

    //In construction
    private int id;
    private int creatorId;
    private final int[] massAdditionalInfo = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

    private String title;
    private String region;
    private String city;
    private String firstLongDesc;
    private String secondLongDesc;
    private String thirdLongDesc;
    private String fourLongDesc;
    private String address;
    private String phone;
    private String contacts;
    private String email;
    private String fullNameCity;
    private String fullNameRegion;
    private String userPhoto;
    private String userName;
    private Integer rating;
    private String street;
    private String house_number;
    private LatLng latLng;
    private LatLng regionPlace;
    private LatLng cityPlace;

    public RequestHostController requestHostController;

    private String vat;
    private int approve;


    public Hotel(JSONArray array, Context context) {
        super();

        requestHostController = new RequestHostController(context);

        try {
            JSONArray jsonArray = array.getJSONArray(3);
            setType(jsonArray.getInt(0));
            setInternet(jsonArray.getInt(1));
            setBreakfast(jsonArray.getInt(2));
            setParking(jsonArray.getInt(3));
            setMedia(jsonArray.getInt(4));
            setShower(jsonArray.getInt(5));
            setKitchen(jsonArray.getInt(6));
            setPets(jsonArray.getInt(7));
            setServices(jsonArray.getInt(8));
            setMoney(jsonArray.getInt(9));


            setId(array.getInt(1));
            setCreatorId(array.getInt(2));
            setPhoto(array.getString(4));
            setTitle(array.getString(5));
            setRegion(array.getString(7));
            setCity(array.getString(8));
            setFirstLongDesc(array.getString(9));
            setSecondLongDesc(array.getString(10));
            setThirdLongDesc(array.getString(11));
            setFourLongDesc(array.getString(12));
            setAddress(array.getString(13));
            setEmail(array.getString(14));
            setPhone(array.getString(15));
            setContacts(array.getString(16));
            setRating(array.getInt(17));
            setStreet(array.getString(18));
            setHouse_number(array.getString(19));
            setLatLng(array.getString(20), array.getString(21));


            if (isHotel()) {
                setVat(array.getString(22));
                setApprove(array.getInt(24));

            } else {
                int point = array.getString(6).indexOf(' ');
                if (point < 1) {
                    setUserPhoto("");
                    setUserName("");
                } else {
                    setUserPhoto(array.getString(6).subSequence(0, point).toString());
                    setUserName(array.getString(6).subSequence(point + 1, array.getString(6).length()).toString());
                }
            }


        } catch (JSONException e) {
            Log.v("TAG", "Parse hotel error = " + e.toString());
        }
    }

    public Hotel(int type, int hotelId, int creatorId) {
        super();
        setInternet(0);
        setBreakfast(0);
        setParking(0);
        setMedia(0);
        setShower(0);
        setKitchen(0);
        setPets(0);
        setServices(0);
        setMoney(0);

        setType(type);
        setId(hotelId);
        setCreatorId(creatorId);
        setPhoto("");
        setTitle("");
        setRegion("");
        setCity("");
        setFirstLongDesc("");
        setSecondLongDesc("");
        setThirdLongDesc("");
        setFourLongDesc("");
        setAddress("");
        setEmail("");
        setPhone("");
        setContacts("");
        setRating(0);
        setStreet("");
        setHouse_number("");
        setLatLng(null, null);


        if (isHotel()) {
            setVat("");
            setApprove(0);
        } else {
            setUserPhoto("");
            setUserName("");
        }
    }


    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public boolean isHotel(){
        return (getType() & HOST_TYPE) != HOST_TYPE;
    }

    public RequestHostController getRequestHostController() {
        return requestHostController;
    }

    public void setRequestHostController(RequestHostController requestHostController) {
        this.requestHostController = requestHostController;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
        setFullNameRegion("");
        if (!getRegion().isEmpty())
            Places.GeoDataApi.getPlaceById(Application.mGoogleApiClient, getRegion())
                    .setResultCallback(new ResultCallback<PlaceBuffer>() {
                        @Override
                        public void onResult(@NonNull PlaceBuffer places) {
                            if (places.getStatus().isSuccess() && places.getCount() > 0) {
                                setFullNameRegion(places.get(0).getAddress().toString());
                                setRegionPlace(places.get(0).getLatLng());
                            } else setFullNameRegion("");
                            places.release();
                        }
                    });
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
        setFullNameCity("");
        if (!getCity().isEmpty())
            Places.GeoDataApi.getPlaceById(Application.mGoogleApiClient, getCity())
                    .setResultCallback(new ResultCallback<PlaceBuffer>() {
                        @Override
                        public void onResult(@NonNull PlaceBuffer places) {
                            if (places.getStatus().isSuccess() && places.getCount() > 0) {
                                setFullNameCity(places.get(0).getAddress().toString());
                                setCityPlace(places.get(0).getLatLng());
                            } else setFullNameCity("");
                            places.release();
                        }
                    });
    }

    public String getFirstLongDesc() {
        return firstLongDesc;
    }

    public void setFirstLongDesc(String firstLongDesc) {
        this.firstLongDesc = firstLongDesc;
    }

    public String getSecondLongDesc() {
        return secondLongDesc;
    }

    public void setSecondLongDesc(String secondLongDesc) {
        this.secondLongDesc = secondLongDesc;
    }

    public String getThirdLongDesc() {
        return thirdLongDesc;
    }

    public void setThirdLongDesc(String thirdLongDesc) {
        this.thirdLongDesc = thirdLongDesc;
    }

    public String getFourLongDesc() {
        return fourLongDesc;
    }

    public void setFourLongDesc(String fourLongDesc) {
        this.fourLongDesc = fourLongDesc;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }


    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getContacts() {
        return contacts;
    }

    public void setContacts(String contacts) {
        this.contacts = contacts;
    }

    public void setVat(String vat) {
        this.vat = vat;
    }

    public void setType(int type) {
        massAdditionalInfo[0] = type;
    }

    public int getType() {
        return massAdditionalInfo[0];
    }

    public void setInternet(int internet) {
        massAdditionalInfo[1] = internet;
    }

    public int getInternet() {
        return massAdditionalInfo[1];
    }

    public void setBreakfast(int breakfast) {
        massAdditionalInfo[2] = breakfast;
    }

    public int getBreakfast() {
        return massAdditionalInfo[2];
    }

    public void setParking(int parking) {
        massAdditionalInfo[3] = parking;
    }

    public int getParking() {
        return massAdditionalInfo[3];
    }

    public void setMedia(int media) {
        massAdditionalInfo[4] = media;
    }

    public int getMedia() {
        return massAdditionalInfo[4];
    }

    public void setShower(int shower) {
        massAdditionalInfo[5] = shower;
    }

    public int getShower() {
        return massAdditionalInfo[5];
    }

    public void setKitchen(int kitchen) {
        massAdditionalInfo[6] = kitchen;
    }

    public int getKitchen() {
        return massAdditionalInfo[6];
    }

    public void setPets(int pets) {
        massAdditionalInfo[7] = pets;
    }

    public int getPets() {
        return massAdditionalInfo[7];
    }


    public int getServices() {
        return massAdditionalInfo[8];
    }

    public void setServices(int services) {
        massAdditionalInfo[8] = services;
    }

    public int getMoney() {
        return massAdditionalInfo[9];
    }

    public void setMoney(int money) {
        massAdditionalInfo[9] = money;
    }

    public String getFullNameCity() {
        return fullNameCity;
    }

    public void setFullNameCity(String fullNameCity) {
        this.fullNameCity = fullNameCity;
    }

    public String getFullNameRegion() {
        return fullNameRegion;
    }

    public void setFullNameRegion(String fullNameRegion) {
        this.fullNameRegion = fullNameRegion;
    }

    public int getId() {
        return id;
    }

    private void setId(int id) {
        this.id = id;
    }

    public int getCreatorId() {
        return creatorId;
    }

    private void setCreatorId(int creatorId) {
        this.creatorId = creatorId;
    }

    public int[] getMassAdditionalInfo() {
        return massAdditionalInfo;
    }

    public String getUserPhoto() {
        return userPhoto;
    }

    private void setUserPhoto(String userPhoto) {
        this.userPhoto = userPhoto;
    }

    public String getUserName() {
        return userName;
    }

    private void setUserName(String userName) {
        this.userName = userName;
    }

    public int getApprove() {
        return approve;
    }

    public void setApprove(int approve) {
        this.approve = approve;
        if (approveStatusChanged != null) approveStatusChanged.onChange(getApprove());
    }

    public String getVat() {
        return vat;
    }


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public LatLng getCityPlace() {
        return cityPlace;
    }

    public void setCityPlace(LatLng cityPlace) {
        this.cityPlace = cityPlace;
    }

    public LatLng getRegionPlace() {
        return regionPlace;
    }

    public void setRegionPlace(LatLng regionPlace) {
        this.regionPlace = regionPlace;
    }

    public LatLng getLatLng() {
        return latLng;
    }

    public void setLatLng(String lat, String lng) {
        if (lat == null || lng == null)
            return;
        if (lat.isEmpty() || lng.isEmpty())
            return;
        this.latLng = new LatLng(Double.valueOf(lat), Double.valueOf(lng));

    }

    public void setLatLng(LatLng latLng) {
        this.latLng = latLng;

    }

    public String getHouse_number() {
        return house_number;
    }

    public void setHouse_number(String house_number) {
        this.house_number = house_number;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    public ApproveStatusChanged getApproveStatusChanged() {
        return approveStatusChanged;
    }

    public void setApproveStatusChanged(ApproveStatusChanged approveStatusChanged) {
        this.approveStatusChanged = approveStatusChanged;
    }

}
