package com.betnbed.model;

public class BaseModel {

    private int internet;
    private int breakfast;
    private int parking;
    private int media;
    private int shower;
    private int kitchen;
    private int pets;
    private int services;
    private int moneyType;
    int hotelType;

    BaseModel(BaseBuilder baseBuilder){

        if(baseBuilder == null) return;

        setInternet(baseBuilder.internet);
        setBreakfast(baseBuilder.breakfast);
        setParking(baseBuilder.parking);
        setMedia(baseBuilder.media);
        setShower(baseBuilder.shower);
        setKitchen(baseBuilder.kitchen);
        setPets(baseBuilder.pets);
        setServices(baseBuilder.services);
        setMoneyType(baseBuilder.moneyType);
        setHotelType(baseBuilder.hotelType);

    }

    public int getInternet() {
        return internet;
    }

    public void setInternet(int internet) {
        this.internet = internet;
    }

    public int getBreakfast() {
        return breakfast;
    }

    public void setBreakfast(int breakfast) {
        this.breakfast = breakfast;
    }

    public int getParking() {
        return parking;
    }

    public void setParking(int parking) {
        this.parking = parking;
    }

    public int getMedia() {
        return media;
    }

    public void setMedia(int media) {
        this.media = media;
    }

    public int getShower() {
        return shower;
    }

    public void setShower(int shower) {
        this.shower = shower;
    }

    public int getKitchen() {
        return kitchen;
    }

    public void setKitchen(int kitchen) {
        this.kitchen = kitchen;
    }

    public int getPets() {
        return pets;
    }

    public void setPets(int pets) {
        this.pets = pets;
    }

    public int getServices() {return services;}

    public void setServices(int services) {
        this.services = services;
    }

    public int getMoneyType() {
        return moneyType;
    }

    public void setMoneyType(int moneyType) {
        this.moneyType = moneyType;
    }

    public int getHotelType() {
        return hotelType;
    }

    public void setHotelType(int hotelType) {
        this.hotelType = hotelType;
    }

    public static class BaseBuilder {

        protected int internet;
        protected int breakfast;
        protected int parking;
        protected int media;
        protected int shower;
        protected int kitchen;
        protected int pets;
        protected int services;
        protected int moneyType;
        protected int hotelType;

        public BaseBuilder internet(int value) {
            this.internet = value;
            return this;
        }

        public BaseBuilder breakfast(int value) {
            this.breakfast = value;
            return this;
        }

        public BaseBuilder parking(int value) {
            this.parking = value;
            return this;
        }

        public BaseBuilder media(int value) {
            this.media = value;
            return this;
        }

        public BaseBuilder shower(int value) {
            this.shower = value;
            return this;
        }

        public BaseBuilder kitchen(int value) {
            this.kitchen = value;
            return this;
        }

        public BaseBuilder pets(int value) {
            this.pets = value;
            return this;
        }

        public BaseBuilder services(int value) {
            this.services = value;
            return this;
        }

        public BaseBuilder moneyType(int value) {
            this.moneyType = value;
            return this;
        }

        public BaseBuilder hotelType(int value) {
            this.hotelType = value;
            return this;
        }

        public BaseModel build(){
            return new BaseModel(this);
        }
    }

}
