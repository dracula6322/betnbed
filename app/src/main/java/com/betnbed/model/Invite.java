package com.betnbed.model;

public class Invite {

    // In construction
    private int type;
    private int requestId;
    private int inviteId;
    private int hotelId;
    private int state;
    private String comment;
    private String title;
    private String photo;
    private String address;
    private String city;
    private String deleteReason;

    private Invite(Builder builder) {

        setComment(builder.comment);
        setTitle(builder.title);
        setPhoto(builder.photo);
        setAddress(builder.address);
        setCity(builder.city);
        setDeleteReason(builder.deleteReason);

        setType(builder.type);
        setRequestId(builder.requestId);
        setInviteId(builder.inviteId);
        setHotelId(builder.hotelId);
        setState(builder.state);

    }

    public int getRequestId() {
        return requestId;
    }

    public void setRequestId(int requestId) {
        this.requestId = requestId;
    }

    public int getInviteId() {
        return inviteId;
    }

    public void setInviteId(int inviteId) {
        this.inviteId = inviteId;
    }

    public int getHotelId() {
        return hotelId;
    }

    public void setHotelId(int hotelId) {
        this.hotelId = hotelId;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public String getDeleteReason() {
        return deleteReason;
    }

    public void setDeleteReason(String deleteReason) {
        this.deleteReason = deleteReason;
    }

    public static class Builder{

        private String comment;
        private String title;
        private String photo;
        private String address;
        private String city;
        private String deleteReason;
        private int type;
        private int requestId;
        private int inviteId;
        private int hotelId;
        private int state;

        public Builder comment(String value){
            this.comment = value;
            return this;
        }
        public Builder title(String value){
            this.title = value;
            return this;
        }
        public Builder photo(String value){
            this.photo = value;
            return this;
        }
        public Builder address(String value){
            this.address = value;
            return this;
        }
        public Builder city(String value){
            this.city = value;
            return this;
        }
        public Builder deleteReason(String value){
            this.deleteReason = value;
            return this;
        }

        public Builder type(int value){
            this.type = value;
            return this;
        }
        public Builder requestId(int value){
            this.requestId = value;
            return this;
        }
        public Builder inviteId(int value){
            this.inviteId = value;
            return this;
        }
        public Builder hotelId(int value){
            this.hotelId = value;
            return this;
        }
        public Builder state(int value){
            this.state = value;
            return this;
        }




        public Invite build(){
            return new Invite(this);
        }
    }

    @Override public String toString() {
        return "Invite{" +
                "type=" + type +
                ", requestId=" + requestId +
                ", inviteId=" + inviteId +
                ", hotelId=" + hotelId +
                ", state=" + state +
                ", comment='" + comment + '\'' +
                ", title='" + title + '\'' +
                ", photo='" + photo + '\'' +
                ", address='" + address + '\'' +
                ", city='" + city + '\'' +
                ", deleteReason='" + deleteReason + '\'' +
                '}';
    }
}
