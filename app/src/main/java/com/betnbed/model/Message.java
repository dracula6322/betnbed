package com.betnbed.model;


public class Message {

    static public final int STATE_READED_MESSAGE = 1;
    static public final int STATE_UNREADED_MESSAGE = 2;
    static public final int STATE_READED_PUSH = 4;
    static public final int STATE_UNREADED_PUSH = 5;
    private int time;
    private int id;
    private String text;
    private int type;


    public Message(int time, int id, String text, int type) {
        setId(id);
        setTime(time);
        setText(text);
        setType(type);

    }

    public int getType() {return type;}

    public void setType(int type) {this.type = type;}

    public String getMessage() {
        return text;
    }

    public void setMessage(String text) {
        this.text = text;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override public String toString() {
        return "Message{" +
                "time=" + time +
                ", id=" + id +
                ", text='" + text + '\'' +
                ", type=" + type +
                '}';
    }
}
