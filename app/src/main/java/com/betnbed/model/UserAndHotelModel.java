package com.betnbed.model;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.subjects.BehaviorSubject;
import io.reactivex.subjects.Subject;

public class UserAndHotelModel {

    //Observable<List<Image>> observable;
    private BehaviorSubject<ArrayList<Image>> imageObservable;
    private BehaviorSubject<Image> photoObservable;
    private String photo;
    private ArrayList<Image> images;

    public UserAndHotelModel() {

        images = new ArrayList<>();

        imageObservable = BehaviorSubject.createDefault(images);
        photoObservable = BehaviorSubject.createDefault(new Image("", -1));
    }



    public void addImage(Image image) {
        Log.v("TAG", "We add image");
        this.images.add(image);
        imageObservable.onNext(images);



    }


    public void deleteImage(int position) {
        images.remove(position);
        imageObservable.onNext(images);
    }

    public Subject<ArrayList<Image>> getImagesObservable() {
        return imageObservable;
    }

    public List<Image> getImages() {
        return images;
    }

    public void setImages(ArrayList<Image> images) {
        this.images = new ArrayList<>(images);
        imageObservable.onNext(images);
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;


    }

    public BehaviorSubject<Image> getPhotoObservable() {
        return photoObservable;
    }

    public void setPhotoObservable(BehaviorSubject<Image> photoObservable) {
        this.photoObservable = photoObservable;
    }
}
