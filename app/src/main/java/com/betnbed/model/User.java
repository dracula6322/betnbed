package com.betnbed.model;

import android.util.Log;

import com.betnbed.Heap;
import com.betnbed.SC;
import com.betnbed.controller.RequestTouristController;

import java.text.ParseException;
import java.util.Calendar;

public class User extends UserAndHotelModel {
    // In construction


    private RequestTouristController requestTouristController;
    private String firstName;
    private String dateBirthday;
    private String lastName;
    private String idCity;
    private String profession;
    private String secondLongDesc;
    private String firstLongDesc;
    private String thirdLongDesc;
    private int gender;
    private int id;
    private int clientProfileId;
    private int yearsOld;


    private String fullNameCity;

    public User(Builder value) {
        super();


        setFirstName(value.firstName);
        setDateBirthday(value.dateBirthday);
        setLastName(value.lastName);
        setIdCity(value.idCity);
        setProfession(value.profession);
        setPhoto(value.photo);
        setSecondLongDesc(value.secondLongDesc);
        setFirstLongDesc(value.firstLongDesc);
        setThirdLongDesc(value.thirdLongDesc);
        setGender(value.gender);
        setId(value.id);
        setClientProfileId(value.clientProfileId);
        setYearsOld(value.yearsOld);

        if (getId() == Heap.touristId && Heap.touristId != 0) requestTouristController = new RequestTouristController();

    }

    /*public void setRequestTouristController(RequestTouristController requestTouristController) {
        this.requestTouristController = requestTouristController;
    }*/

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getDateBirthday() {
        return dateBirthday;
    }

    public void setDateBirthday(String dateBirthday) {
        this.dateBirthday = dateBirthday;

        if (dateBirthday != null)
            if (!dateBirthday.isEmpty()) {
                try {
                    Calendar birthDay = Calendar.getInstance();
                    birthDay.setTime(SC.dfDayBirthday.parse(this.dateBirthday));
                    Calendar current = Calendar.getInstance();

                    setYearsOld(current.get(Calendar.YEAR) - birthDay.get(Calendar.YEAR) - 1);
                    if (current.get(Calendar.MONTH) > birthDay.get(Calendar.MONTH))
                        setYearsOld(getYearsOld() + 1);

                    if (current.get(Calendar.MONTH) == birthDay.get(Calendar.MONTH))
                        if (current.get(Calendar.DAY_OF_MONTH) >= birthDay.get(Calendar.DAY_OF_MONTH)) {
                            setYearsOld(getYearsOld() + 1);
                        }

                } catch (ParseException e) {
                    Log.v("TAG", e.toString());
                    setYearsOld(0);
                }
            } else setYearsOld(0);
        else setYearsOld(0);
    }

    public RequestTouristController getRequestTouristController() {
        return requestTouristController;
    }

    public void setRequestTouristController(RequestTouristController requestTouristController) {
        this.requestTouristController = requestTouristController;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getIdCity() {
        return idCity;
    }

    public void setIdCity(String idCity) {
        this.idCity = idCity;
    }

    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public int getId() {
        return id;
    }

    private void setId(int id) {
        this.id = id;
    }

    public String getFullNameCity() {
        return fullNameCity;
    }

    public void setFullNameCity(String fullNameCity) {
        this.fullNameCity = fullNameCity;
    }

    public int getYearsOld() {
        return yearsOld;
    }

    private void setYearsOld(int yearsOld) {
        this.yearsOld = yearsOld;
    }

    public String getSecondLongDesc() {
        return secondLongDesc;
    }

    public void setSecondLongDesc(String secondLongDesc) {
        this.secondLongDesc = secondLongDesc;
    }

    public String getFirstLongDesc() {
        return firstLongDesc;
    }

    public void setFirstLongDesc(String firstLongDesc) {
        this.firstLongDesc = firstLongDesc;
    }

    public String getThirdLongDesc() {
        return thirdLongDesc;
    }

    public void setThirdLongDesc(String thirdLongDesc) {
        this.thirdLongDesc = thirdLongDesc;
    }

    private void setClientProfileId(int clientProfileId) {
        this.clientProfileId = clientProfileId;
    }

    public static class Builder {
        private String firstName;
        private String dateBirthday;
        private String lastName;
        private String idCity;
        private String profession;
        private String photo;
        private String secondLongDesc;
        private String firstLongDesc;
        private String thirdLongDesc;
        private int gender;
        private int id;
        private int clientProfileId;
        private int yearsOld;

        public Builder firstName(String value) {
            this.firstName = value;
            return this;
        }

        public Builder dateBirthday(String value) {
            this.dateBirthday = value;
            return this;
        }

        public Builder lastName(String value) {
            this.lastName = value;
            return this;
        }

        public Builder idCity(String value) {
            this.idCity = value;
            return this;
        }

        public Builder profession(String value) {
            this.profession = value;
            return this;
        }

        public Builder photo(String value) {
            this.photo = value;
            return this;
        }

        public Builder secondLongDesc(String value) {
            this.secondLongDesc = value;
            return this;
        }

        public Builder firstLongDesc(String value) {
            this.firstLongDesc = value;
            return this;
        }

        public Builder thirdLongDesc(String value) {
            this.thirdLongDesc = value;
            return this;
        }

        public Builder gender(int value) {
            this.gender = value;
            return this;
        }

        public Builder id(int value) {
            this.id = value;
            return this;
        }

        public Builder clientProfileId(int value) {
            this.clientProfileId = value;
            return this;
        }

        public Builder yearsOld(int value) {
            this.yearsOld = value;
            return this;
        }

        public User build() {
            return new User(this);
        }

    }


}
