package com.betnbed;


import android.content.Context;
import android.util.Log;

import com.betnbed.controller.MessageController;
import com.betnbed.controller.SettingsController;
import com.betnbed.interfaces.SocketInterface;
import com.betnbed.model.Hotel;
import com.betnbed.model.Image;
import com.betnbed.model.User;
import com.betnbed.service.NetworkService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Heap {


    public static User accountUser;
    public static int touristId = 0;
    public static int hostId = 0;
    public static int hotelId = 0;
    public static Hotel accountHotel;

    public static final String imagePrefix = BuildConfig.imagePrefix;

    public static void initUserProfile() {
        if (touristId != 0) {
            NetworkService.requestController.sendRequest(RequestConstructor.CGetProfile(1, touristId), "SGetProfile", new SocketInterface() {
                @Override
                public void onSuccess(String request) {
                    try {

                        JSONArray data = new JSONObject(request).getJSONArray("data");
                        int delta = data.length() == 17 ? 1 : 0;
                        accountUser = new User.Builder()
                                .id(data.getInt(delta))
                                .clientProfileId(data.getInt(1 + delta))
                                .firstName(data.getString(3 + delta))
                                .lastName(data.getString(4 + delta))
                                .idCity(data.getString(6 + delta))
                                .gender(data.getInt(7 + delta))
                                .profession(data.getString(8 + delta))
                                .photo(data.getString(10 + delta))
                                .dateBirthday(data.getString(11 + delta))
                                .firstLongDesc(data.getString(12 + delta))
                                .secondLongDesc(data.getString(13 + delta))
                                .thirdLongDesc(data.getString(14 + delta))
                                .build();
                    } catch (JSONException e) {
                        Log.v("TAG", e.toString());
                    }

                    NetworkService.requestController.sendRequest(RequestConstructor.CGetImages(1, touristId), "SGetImages", new SocketInterface() {
                        @Override
                        public void onSuccess(String request) {
                            accountUser.setImages(getLinkImages(request, accountUser.getPhoto()));
                        }

                        @Override
                        public void onError(int error) {

                        }
                    });
                }

                @Override
                public void onError(int error) {
                }
            });
        } else accountUser = new User.Builder().id(touristId).clientProfileId(SettingsController.getAccoutUserId()).build();

    }

    public static void initHotelProfile(final int type, final Context context) {


        if ((type == 2 && Heap.hostId != 0) || (type == 3 && Heap.hotelId != 0)) {
            NetworkService.requestController.sendRequest(RequestConstructor.CGetProfile(type, type == 2 ? Heap.hostId : Heap.hotelId), "SGetProfile", new SocketInterface() {
                @Override
                public void onSuccess(String request) {
                    try {
                        Heap.accountHotel = new Hotel(new JSONObject(request).getJSONArray("data"), context);
                    } catch (JSONException e) {
                        Log.v("TAG", "Some error");

                    }
                    NetworkService.requestController.sendRequest(RequestConstructor.CGetImages(type, type == 2 ? Heap.hostId : Heap.hotelId), "SGetImages", new SocketInterface() {
                        @Override
                        public void onSuccess(String request) {
                            Heap.accountHotel.setImages(getLinkImages(request, accountHotel.getPhoto()));
                        }

                        @Override
                        public void onError(int error) {

                        }
                    });

                    MessageController.init(context);
                }

                @Override
                public void onError(int error) {

                }
            });
        } else accountHotel = new Hotel(type, hotelId, SettingsController.getAccoutUserId());
    }

    public static boolean changeAvatar(int type, final String image) {


        SocketInterface socketInterface = new SocketInterface() {
            @Override
            public void onSuccess(String request) {
                try {
                    JSONArray jsonArray = new JSONObject(request).getJSONArray("data");
                    int typeProfile = jsonArray.getInt(0);
                    JSONObject jsonObject = jsonArray.getJSONObject(2);
                    if (jsonObject.has("photo")) {
                        String photo = jsonObject.getString("photo");
                        switch (typeProfile) {
                            case 1:
                                Heap.accountUser.setPhoto(photo);
                                break;
                            case 2:
                                Heap.accountHotel.setPhoto(photo);
                                break;
                            case 3:
                                Heap.accountHotel.setPhoto(photo);
                                break;
                        }
                    }

                } catch (JSONException e) {
                    Log.v("TAG", e.toString());
                }
            }

            @Override
            public void onError(int error) {
            }
        };
        try {
            switch (type) {
                case 1:
                    NetworkService.requestController.sendRequest(RequestConstructor.CChangeTourist(new JSONObject().put("photo", image)), "SProfileChanged", socketInterface);
                    break;
                case 2:
                    NetworkService.requestController.sendRequest(RequestConstructor.CChangeHouse(new JSONObject().put("photo", image)), "SProfileChanged", socketInterface);
                    break;
                case 3:
                    NetworkService.requestController.sendRequest(RequestConstructor.CChangeHotel(new JSONObject().put("photo", image)), "SProfileChanged", socketInterface);
                    break;
            }
        } catch (JSONException e) {
            Log.v("TAG", e.toString());
        }
        return true;
    }

    static private ArrayList<Image> getLinkImages(String request, String avatar) {
        ArrayList<Image> result = new ArrayList<>();
        if (!avatar.isEmpty()) result.add(new Image(avatar, -1));
        try {
            JSONArray data = new JSONObject(request).getJSONArray("data");
            int count = data.getInt(2);
            for (int i = 0; i < count; i++) {
                JSONArray elem = data.getJSONArray(i + 3);
                String path = imagePrefix + elem.getString(1);
                if (!avatar.isEmpty()) if (path.equals(result.get(0).getUrl())) continue;
                result.add(new Image(path, elem.getInt(0)));
            }
        } catch (JSONException e) {
            Log.v("TAG", e.toString());
        }
        return result;
    }

}
