package com.betnbed.customView;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.betnbed.R;

import butterknife.BindView;

public class ToolbarActivity extends AppCompatActivity {

    static public final String ARGUMENT_OPEN_PAGE = "OPEN_PAGE";
    static public final String ARGUMENT_GSM_ERROR = "GSM_ERROR";

    @BindView(R.id.toolbar) public Toolbar toolbar;

    public void initToolbar(String title) {

        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        toolbar.setTitle(title);
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                onBackPressed();
            }
        });

    }

}
