package com.betnbed.customView;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatDelegate;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.betnbed.Heap;
import com.betnbed.R;
import com.betnbed.activity.MessageActivity;
import com.betnbed.activity.MyHostActivity;
import com.betnbed.activity.TouristRequestListActivity;
import com.betnbed.activity.UserSettingsActivity;
import com.betnbed.activity.profile.ProfileHotelActivity;
import com.betnbed.activity.profile.ProfileTouristActivity;
import com.betnbed.controller.SettingsController;
import com.betnbed.service.NetworkService;
import com.betnbed.service.NotificationService;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;

import butterknife.BindView;

abstract public class BetnbedActivity extends ToolbarActivity {

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    private ImageView navigationViewImageView;
    private TextView navigationTextView;
    private BitmapImageViewTarget cycleBitmapImageViewTarget;

    @BindView(R.id.nav_view) NavigationView navigationView;
    @BindView(R.id.drawer_layout) public DrawerLayout mDrawer;

    @Override public void onResume() {
        super.onResume();
        drawAvatar();
        reWriteName();
    }

    protected void initNavigationView() {

        navigationView.setItemIconTintList(null);
        navigationView.inflateMenu(SettingsController.isHotel() ? R.menu.navigation_view_hotel_menu : R.menu.navigation_view_tourist_menu);
        navigationViewImageView = (ImageView) navigationView.getHeaderView(0).findViewById(R.id.nav_header_imageView);
        cycleBitmapImageViewTarget = new BitmapImageViewTarget(navigationViewImageView) {
            @Override
            protected void setResource(Bitmap resource) {
                RoundedBitmapDrawable circularBitmapDrawable = RoundedBitmapDrawableFactory.create(getResources(), resource);
                circularBitmapDrawable.setCircular(true);
                navigationViewImageView.setImageDrawable(circularBitmapDrawable);
            }
        };
        navigationTextView = (TextView) navigationView.getHeaderView(0).findViewById(R.id.nav_header_textView);
        reWriteName();
        drawAvatar();


        mDrawer.addDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
            }

            @Override
            public void onDrawerOpened(View drawerView) {
            }

            @Override
            public void onDrawerClosed(View drawerView) {
            }

            @Override
            public void onDrawerStateChanged(int newState) {
                navigationView.getMenu().getItem(SettingsController.isHotel() ? 2 : 3).getSubMenu().getItem(0).setTitle("Сообщения " + (NotificationService.getCountUnreadMessage() > 0 ? ("(" + NotificationService.getCountUnreadMessage() + ")") : ""));
                navigationView.getMenu().getItem(SettingsController.isHotel() ? 0 : 1).getSubMenu().getItem(1).setTitle("Мои гости " + (NotificationService.getCountUnreadOurInviteAccepted() > 0 ? ("(" + NotificationService.getCountUnreadOurInviteAccepted() + ")") : ""));

                if (!SettingsController.isHotel())
                    navigationView.getMenu().getItem(0).getSubMenu().getItem(1).setTitle("Мои приглашения " + (NotificationService.getCountUnreadInvites() > 0 ? ("(" + NotificationService.getCountUnreadInvites() + ")") : ""));
            }
        });
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, mDrawer, 0, 0);
        actionBarDrawerToggle.syncState();

        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                        menuItem.setChecked(true);
                        mDrawer.closeDrawers();
                        switch (menuItem.getItemId()) {
                            case R.id.navigation_view_menu_invitations:
                                showTouristRequestList(0);
                                return true;
                            case R.id.navigation_view_menu_search:
                                showTouristRequestList(1);
                                return true;
                            case R.id.i_wanna_invite:
                                showMyHost(0);
                                return true;
                            case R.id.menu_guests:
                                showMyHost(2);
                                return true;
                            case R.id.navigation_view_menu_profile:
                                if (SettingsController.isHotel())
                                    showHotelProfile();
                                else
                                    showTouristProfile();
                                return true;
                            case R.id.navigation_view_menu_myHost:
                                showHotelProfile();
                                return true;
                            case R.id.admin_message:
                                showMessage(0);
                                return true;
                            case R.id.settings:
                                showSettins();
                                return true;
                            default:
                                return true;
                        }
                    }


                }
        );
    }

    private void showHotelProfile() {
        Intent intent = new Intent(this, ProfileHotelActivity.class);
        intent.putExtra("isAnotherHotel", false);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    private void showTouristProfile() {


        Intent intent = new Intent(this, ProfileTouristActivity.class);
        intent.putExtra("isAnotherTourist", false);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    private void showTouristRequestList(int page) {
        Intent intent = new Intent(this, TouristRequestListActivity.class);
        intent.putExtra("page", page);
        startActivity(intent);
    }

    private void showMyHost(int page) {
        Intent intent = new Intent(this, MyHostActivity.class);
        intent.putExtra("page", page);
        startActivity(intent);
    }

    private void showMessage(int page) {
        Intent intent = new Intent(this, MessageActivity.class);
        intent.putExtra("page", page);
        startActivity(intent);
    }

    private void showSettins() {
        Intent intent = new Intent(this, UserSettingsActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    @Override
    public void initToolbar(String title) {
        super.initToolbar(title);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                if (toolbar.getTitle().equals("Betnbed")) mDrawer.openDrawer(GravityCompat.START);
                else onBackPressed();
            }
        });
    }

    public void drawAvatar() {
        if (navigationViewImageView != null)
            if (!SettingsController.isHotel()) {
                if (Heap.accountUser == null)
                    Glide.with(this).load(R.drawable.png_tourist_empty_profile_white).error(R.drawable.png_tourist_empty_profile_white).override(92, 92).centerCrop().into(navigationViewImageView);
                else if (!Heap.accountUser.getPhoto().isEmpty())
                    Glide.with(this)
                            .load(Heap.accountUser.getPhoto())
                            .asBitmap()
                            .placeholder(R.drawable.png_tourist_empty_profile_white)
                            .error(R.drawable.png_tourist_empty_profile_white)
                            .centerCrop()
                            .into(cycleBitmapImageViewTarget);
                else Glide.with(this).load(R.drawable.png_tourist_empty_profile_white).error(R.drawable.png_tourist_empty_profile_white).override(92, 92).centerCrop().into(navigationViewImageView);
            } else {
                if (!Heap.accountHotel.getPhoto().isEmpty())
                    Glide.with(this)
                            .load(Heap.accountHotel.getPhoto())
                            .asBitmap()
                            .placeholder(R.drawable.png_hotel_white)
                            .error(R.drawable.png_hotel_white)
                            .centerCrop()
                            .into(cycleBitmapImageViewTarget);
                else Glide.with(this).load(R.drawable.png_hotel_white).error(R.drawable.png_hotel_white).override(92, 92).centerCrop().into(navigationViewImageView);
            }
    }


    public void reWriteName() {
        String name = "";
        try {
            name = !SettingsController.isHotel() ? Heap.accountUser.getFirstName() + " " + Heap.accountUser.getLastName() : Heap.accountHotel.getTitle();
        } catch (Exception e) {
            Log.v("TAG", e.toString());
            name = "";
        } finally {
            navigationTextView.setText(name);
        }
    }

    boolean doubleBackToExitPressedOnce = false;

    @Override public void onBackPressed() {

        if (mDrawer.isDrawerOpen(GravityCompat.START)) {
            mDrawer.closeDrawer(GravityCompat.START);
            return;
        }

        if (isTaskRoot()) {
            if (doubleBackToExitPressedOnce) {
                Intent intent = new Intent(this, NetworkService.class);
                stopService(intent);
                super.onBackPressed();
                return;
            }

            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, getString(R.string.twiceBackPress), Toast.LENGTH_SHORT).show();

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 2000);
        } else super.onBackPressed();
    }
}