package com.betnbed.customView;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.Button;

public class BetnbedButton extends Button {

    public BetnbedButton(Context context) {
        super(context);
    }

    public BetnbedButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public BetnbedButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN)
            setAlpha((float) 0.5);
        if (event.getAction() == MotionEvent.ACTION_UP && isEnabled())
            setAlpha((float) 1);
        return super.onTouchEvent(event);
    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        if (enabled) setAlpha((float) 1);
        else setAlpha((float) 0.5);
    }


}
