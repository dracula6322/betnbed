package com.betnbed.exception;

public class BetnbedException extends Exception {

    private int errorCode;

    public BetnbedException(int errorCode) {
        this.errorCode = errorCode;
    }

    public String getMessageByCode(){
        switch (errorCode){
            case 11 : return "Неправильный пароль";
        }
        return "";
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }
}
