package com.betnbed.interfaces;

public interface ApproveStatusChanged {

    public void onChange(int approveStatus);

}
