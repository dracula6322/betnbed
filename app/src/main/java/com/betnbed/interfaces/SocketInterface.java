package com.betnbed.interfaces;

public interface SocketInterface {

    void onSuccess(String request);
    void onError(int error);

}
