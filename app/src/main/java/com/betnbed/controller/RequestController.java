package com.betnbed.controller;

import android.util.Log;

import com.betnbed.Heap;
import com.betnbed.model.BaseModel;
import com.betnbed.model.Invite;
import com.betnbed.model.Request;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class RequestController {



    static public ArrayList<String> getLinkImagesWithOutId(String request) {
        ArrayList<String> result = new ArrayList<>();
        try {
            JSONArray data = new JSONObject(request).getJSONArray("data");
            int count = data.getInt(2);
            for (int i = 0; i < count; i++) {
                JSONArray elem = data.getJSONArray(i + 3);
                result.add(Heap.imagePrefix + elem.getString(1));
            }
        } catch (JSONException e) {
            Log.v("TAG", e.toString());
        }
        return result;
    }


    static public ArrayList<Invite> getInvitesFromCGetActiveInvites(String request) {
        ArrayList<Invite> result = new ArrayList<>();

        try {
            JSONArray data = (new JSONObject(request)).getJSONArray("data");
            for (int i = 0; i < data.length(); i += 2) {
                int requestId = data.getInt(i);
                JSONArray elem = data.getJSONArray(i + 1);
                for (int j = 0; j < elem.getInt(0); j++) {
                    JSONArray elemElem = elem.getJSONArray(j + 1);
                    Invite invite = new Invite.Builder()
                            .hotelId(elemElem.getInt(0))
                            .type(elemElem.getInt(1))
                            .inviteId(elemElem.getInt(2))
                            .comment(elemElem.getString(3))
                            .title(elemElem.getString(4))
                            .photo(elemElem.getString(5))
                            .address(elemElem.getString(6))
                            .city(elemElem.getString(7))
                            .state(elemElem.getInt(8))
                            .requestId(requestId)
                            .build();
                    result.add(invite);
                }

            }
        } catch (JSONException e) {
            Log.v("TAG", e.toString() + "");
            return new ArrayList<>();
        }
        return result;
    }


    static public ArrayList<Request> getRequestsFromGetRequests(String request, boolean accepted) {

        ArrayList<Request> requestMass = new ArrayList<>();
        try {
            JSONArray data = new JSONObject(request).getJSONArray("data");
            for (int i = 0; i < data.getInt(1); i++)
                requestMass.add(getRequestFromJSONArray(data.getJSONArray(i + 2), accepted));
        } catch (JSONException e) {
            Log.v("TAG", e.toString() + "");
        }
        return requestMass;
    }


    private static Request getRequestFromJSONArray(JSONArray elem, boolean accepted) {
        try {
            /*Request request = new Request(
                    elem.getInt(0),
                    elem.getString(1),
                    elem.getLong(2),
                    elem.getLong(3),
                    elem.getInt(4),
                    elem.getString(5),
                    elem.getInt(6),
                    elem.getString(7),
                    elem.getInt(9),
                    elem.getInt(10),
                    elem.getInt(11),
                    elem.getInt(12),
                    elem.getInt(13),
                    elem.getInt(14),
                    elem.getInt(15),
                    elem.getInt(16),
                    elem.getInt(17),
                    elem.getInt(18),
                    accepted);*/

            BaseModel.BaseBuilder baseBuilder = new BaseModel.BaseBuilder()
                    .hotelType(elem.getInt(9))
                    .internet(elem.getInt(10))
                    .breakfast(elem.getInt(11))
                    .parking(elem.getInt(12))
                    .media(elem.getInt(13))
                    .shower(elem.getInt(14))
                    .kitchen(elem.getInt(15))
                    .pets(elem.getInt(16))
                    .services(elem.getInt(17))
                    .moneyType(elem.getInt(18));

            Request request = new Request.Builder()
                    .catsAndDogs(baseBuilder)
                    .requestId(elem.getInt(0))
                    .place(elem.getString(1))
                    .arrival(elem.getLong(2))
                    .departure(elem.getLong(3))
                    .money(elem.getInt(4))
                    .currency(elem.getString(5))
                    .people(elem.getInt(6))
                    .special(elem.getString(7))
                    .isActive(accepted)
                    .build();

            JSONObject jsonObject = elem.getJSONObject(19);
            for (int i = 0; i < jsonObject.getInt("N"); i++) {
                request.getInvitesIds().add(jsonObject.getJSONArray("ids").getInt(i));
            }
            return request;

        } catch (JSONException e) {
            Log.v("TAG", e.toString());
        }
        return null;
    }


}