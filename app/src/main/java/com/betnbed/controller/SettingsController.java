package com.betnbed.controller;


import android.content.SharedPreferences;


public class SettingsController {

	static public final String tagSettings = "accountSettings";
	static private final String tagAccountLogin = "accountLogin";
	static private final String tagAccountLoginVK = "accountLoginVK";
	static private final String tagAccountUserId = "accountUserId";
	static private final String tagAccountLoginVKUserId = "accountLoginVKUserId";
	static private final String tagAccountLoginVKUserToken = "accountLoginVKUserToken";
	static private final String tagAccountPassword = "accountPassword";
	static private final String tagAccountIsHotel = "accountIsHotel";
	static private final String tagAdditionalInfoWhenInviteTourist = "additionalInfoWhenInviteTourist";
	static private final String tagFirstLauncherTourist = "firstLauncherTourist";
	static private final String tagFirstLauncherHotel = "firstLauncherHotel";
	static private final String tagFirstLauncher = "firstLauncher";

	static private SharedPreferences accountSetting;

	static public void initAccountSetting (SharedPreferences _sr) {
		accountSetting = _sr;
	}

	static public String getAccountLogin () {
		return accountSetting.getString(tagAccountLogin, "");
	}

	static public String getAccountPassword () {
		return accountSetting.getString(tagAccountPassword, "");
	}

	static public int getAccountLoginVKUserId() {
		return accountSetting.getInt(tagAccountLoginVKUserId, 0);
	}

	static public int getAccoutUserId() {
		return accountSetting.getInt(tagAccountUserId, 0);
	}

	static public String getAccountLoginVKUserToken() {
		return accountSetting.getString(tagAccountLoginVKUserToken, "");
	}

	static public String getAdditionalInfoWhenInviteTourist() {
		return accountSetting.getString(tagAdditionalInfoWhenInviteTourist, "");
	}

	static public boolean isHotel () {
		return accountSetting.getBoolean(tagAccountIsHotel, false);
	}

	static public boolean isFirstLaunchTourist () {
		boolean result = accountSetting.getBoolean(tagFirstLauncherTourist, false);
		setFirstLauncherTourist();
		return result;
	}

	static public boolean isFirstLaunchHotel () {
		boolean result = accountSetting.getBoolean(tagFirstLauncherHotel, false);
		setFirstLauncherHotel();
		return result;
	}

	static public boolean isFirstLaunch() {
		boolean result = accountSetting.getBoolean(tagFirstLauncher, false);
		setFirstLauncher();
		return result;
	}

	static public void clearLoginPasswordId () {

		SharedPreferences.Editor editor = accountSetting.edit();
		editor.putString(tagAccountPassword, "");
		editor.putString(tagAccountLogin, "");
		editor.putString(tagAccountLoginVK, "");
		editor.putInt(tagAccountLoginVKUserId, 0);
		editor.apply();
	}

	static public void setTagAccountPassword (String password) {
		SharedPreferences.Editor editor = accountSetting.edit();
		editor.putString(tagAccountPassword, password);
		editor.apply();
	}

	static public void setTagAccountUserId (int userId) {
		SharedPreferences.Editor editor = accountSetting.edit();
		editor.putInt(tagAccountUserId, userId);
		editor.apply();
	}

	static public void setTagAccountLogin (String login) {
		SharedPreferences.Editor editor = accountSetting.edit();
		editor.putString(tagAccountLogin, login);
		editor.apply();
	}

	static public void setAdditionalInfoWhenInviteTourist (String additionalInfoWhenInviteTourist) {
		SharedPreferences.Editor editor = accountSetting.edit();
		editor.putString(tagAdditionalInfoWhenInviteTourist, additionalInfoWhenInviteTourist);
		editor.apply();
	}

	static public void setIsHotel (boolean isHotel) {
		SharedPreferences.Editor editor = accountSetting.edit();
		editor.putBoolean(tagAccountIsHotel, isHotel);
		editor.apply();
	}

	static public void setIsLoginVKUserId (int userId) {
		SharedPreferences.Editor editor = accountSetting.edit();
		editor.putInt(tagAccountLoginVKUserId, userId);
		editor.apply();
	}

	static public void setIsLoginVKUserToken (String token) {
		SharedPreferences.Editor editor = accountSetting.edit();
		editor.putString(tagAccountLoginVKUserToken, token);
		editor.apply();
	}

	private static void setFirstLauncherTourist() {
		SharedPreferences.Editor editor = accountSetting.edit();
		editor.putBoolean(tagFirstLauncherTourist, true);
		editor.apply();
	}

	private static void setFirstLauncherHotel() {
		SharedPreferences.Editor editor = accountSetting.edit();
		editor.putBoolean(tagFirstLauncherHotel, true);
		editor.apply();
	}

	private static void setFirstLauncher() {
		SharedPreferences.Editor editor = accountSetting.edit();
		editor.putBoolean(tagFirstLauncher, true);
		editor.apply();
	}

}