package com.betnbed.controller;

import android.util.Log;
import android.util.SparseArray;

import com.betnbed.RequestConstructor;
import com.betnbed.interfaces.NewInviteTourist;
import com.betnbed.interfaces.NewTouristRequest;
import com.betnbed.interfaces.SocketInterface;
import com.betnbed.model.Invite;
import com.betnbed.model.Request;
import com.betnbed.service.NetworkService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.concurrent.Callable;

import io.reactivex.Single;
import io.reactivex.SingleSource;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Function;

public class RequestTouristController {


    private NewTouristRequest requestObserver;
    private NewInviteTourist inviteObserver;
    private boolean isUpdateDataBase;
    private SparseArray<Request> requests;

    public RequestTouristController() {


        this.requests = new SparseArray<>();
        this.isUpdateDataBase = false;

    }

    public void addRequest(Request request) {

        requests.put(request.getRequestId(), request);
        Log.v("TAG", "requestObserver != null = " + (requestObserver != null));
        if (requestObserver != null) requestObserver.onNewRequest();
    }

    public void SNewInvite(Invite invite) {

        Request request = requests.get(invite.getRequestId());
        if (request == null) return;

        request.getInvites().add(0, invite);
        if (requestObserver != null) requestObserver.onNewRequest();
        if (inviteObserver != null) inviteObserver.onNewInvite();
    }

    public void SInviteDeleted(int requestId, int inviteId, String deleteReason) {

        Request request = requests.get(requestId);
        if (request == null) return;

        for (Invite invite : request.getInvites())
            if (invite.getInviteId() == inviteId) {
                invite.setDeleteReason(deleteReason);
            }

        if (inviteObserver != null) inviteObserver.onNewInvite();

    }

    public void getActiveRequestsAndInvites() {

        /*if (!isUpdateDataBase()) {

            Single<ArrayList<Request>> activeRequests = getRequestObservable(3);
            Single<ArrayList<Request>> finishRequests = getRequestObservable(1);
            Single<ArrayList<Request>> allRequests = Single.zip(activeRequests, finishRequests, new BiFunction<ArrayList<Request>, ArrayList<Request>, ArrayList<Request>>() {
                @Override public ArrayList<Request> apply(@NonNull ArrayList<Request> arg1, @NonNull ArrayList<Request> arg2) throws Exception {
                    ArrayList<Request> result = new ArrayList<>();
                    result.addAll(arg1);
                    result.addAll(arg2);
                    return result;
                }
            }).cache();

            Single<ArrayList<Invite>> allInvite = allRequests
                    .flatMap(new Function<ArrayList<Request>, Single<JSONObject>>() {
                        @Override public Single<JSONObject> apply(@NonNull ArrayList<Request> requests) throws Exception {
                            JSONArray invitesData = new JSONArray();
                            for (Request request : requests) {
                                if (request.getInvitesIds().size() == 0) continue;

                                invitesData.put(request.getRequestId());
                                JSONArray local = new JSONArray();
                                for (Integer integer : request.getInvitesIds()) local.put(integer);

                                invitesData.put(local);
                            }
                            return NetworkService.requestController.sendRequestWithObservable(RequestConstructor.CGetInvites(invitesData), "SGetInvites");
                        }
                    })
                    .flatMap(new Function<JSONObject, Single<ArrayList<Invite>>>() {
                        @Override public Single<ArrayList<Invite>> apply(@NonNull JSONObject jsonObject) throws Exception {
                            return Single.just(RequestController.getInvitesFromCGetActiveInvites(jsonObject.toString()));
                        }
                    }).cache();

            Single.zip(allRequests, allInvite, new BiFunction<ArrayList<Request>, ArrayList<Invite>, ArrayList<Request>>() {
                @Override public ArrayList<Request> apply(@NonNull ArrayList<Request> requests, @NonNull ArrayList<Invite> invites) throws Exception {

                    for (Invite invite : invites)
                        for (Request request : requests)
                            if (request.getRequestId() == invite.getRequestId())
                                if (invite.getState() == 4 || invite.getState() == 2)
                                    request.getInvites().add(invite);

                    return requests;
                }
            }).subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Consumer<ArrayList<Request>>() {
                        @Override public void accept(@NonNull ArrayList<Request> req) throws Exception {
                            for (Request request : req)
                                requests.put(request.getRequestId(), request);
                            requestObserver.onNewRequest();
                        }
                    }, new Consumer<Throwable>() {
                        @Override public void accept(@NonNull Throwable throwable) throws Exception {

                        }
                    });

        }
        setIsUpdateDataBase(true);*/

        //////////////////////////////////////

        final JSONArray active = new JSONArray();
        if (!isUpdateDataBase()) {
            active.put(6322);
            NetworkService.requestController.sendRequest(RequestConstructor.CGetRequestsIds(3), "SGetRequestsIds", new SocketInterface() {
                @Override
                public void onSuccess(String request) {
                    try {
                        JSONArray jsonArray = new JSONObject(request).getJSONArray("data");
                        if (jsonArray.getInt(1) > 0)
                            for (int i = 0; i < jsonArray.getInt(1); i++) {
                                active.put(jsonArray.getJSONArray(2).getInt(i));
                            }

                        if (active.length() > 1) {
                            NetworkService.requestController.sendRequest(RequestConstructor.CGetRequests(active), "SGetRequests", new SocketInterface() {
                                @Override
                                public void onSuccess(String request) {
                                    ArrayList<Request> activeRequests = RequestController.getRequestsFromGetRequests(request, true);
                                    for (Request activeRequest : activeRequests)
                                        requests.put(activeRequest.getRequestId(), activeRequest);
                                }

                                @Override
                                public void onError(int error) {

                                }
                            });
                        }

                    } catch (JSONException e) {
                        Log.v("TAG", "Some error");
                    }
                }

                @Override
                public void onError(int error) {
                }
            });

            final JSONArray finish = new JSONArray().put(6322);
            //final StringBuilder finish = new StringBuilder("6322,");
            NetworkService.requestController.sendRequest(RequestConstructor.CGetRequestsIds(1), "SGetRequestsIds", new SocketInterface() {
                @Override
                public void onSuccess(String request) {
                    try {
                        JSONArray jsonArray = new JSONObject(request).getJSONArray("data");
                        if (jsonArray.getInt(1) > 0)
                            for (int i = 0; i < jsonArray.getInt(1); i++)
                                finish.put(jsonArray.getJSONArray(2).getInt(i));
                                //finish.append(jsonArray.getJSONArray(2).getInt(i) + (i != (jsonArray.getInt(1) - 1) ? "," : ""));

                        if (finish.length() > 1) {
                            NetworkService.requestController.sendRequest(RequestConstructor.CGetRequests(finish), "SGetRequests", new SocketInterface() {
                                @Override
                                public void onSuccess(String request) {
                                    ArrayList<Request> activeRequests = RequestController.getRequestsFromGetRequests(request, false);
                                    for (Request activeRequest : activeRequests)
                                        requests.put(activeRequest.getRequestId(), activeRequest);
                                }

                                @Override
                                public void onError(int error) {

                                }
                            });
                        }

                    } catch (JSONException e) {
                        Log.v("TAG", "Some error");
                    }
                }

                @Override
                public void onError(int error) {
                }
            });

            JSONArray invitesData = new JSONArray();

            for (int i=0; i< requests.size();i++){
                Request request = requests.valueAt(i);
                if(request.getInvitesIds().size() > 0){

                    invitesData.put(request.getRequestId());
                    JSONArray local = new JSONArray();
                    for(int j=0; j< request.getInvitesIds().size(); j++)
                        local.put(request.getInvitesIds().get(j));

                    invitesData.put(local);

                }

            }
            if (invitesData.length() > 0)
                NetworkService.requestController.sendRequest(RequestConstructor.CGetInvites(invitesData), "SGetInvites", new SocketInterface() {
                    @Override
                    public void onSuccess(String request) {
                        ArrayList<Invite> invites = RequestController.getInvitesFromCGetActiveInvites(request);
                        if (invites != null)
                            for (int i = 0; i < invites.size(); i++)
                                for (int j = 0; j < requests.size(); j++)
                                    if (requests.valueAt(j).getRequestId() == invites.get(i).getRequestId())
                                        if (invites.get(i).getState() == 4 || invites.get(i).getState() == 2)
                                            requests.valueAt(j).getInvites().add(invites.get(i));
                    }

                    @Override
                    public void onError(int error) {

                    }
                });


        }
        setIsUpdateDataBase(true);

    }

    private Single<ArrayList<Request>> getRequestObservable(final int type) {

        /*Observable.fromCallable(new Callable<Object>() {

            @Override public Object call() throws Exception {
                NetworkService.requestController.sendRequestWithObservable(RequestConstructor.CGetRequestsIds(type), "SGetRequestsIds")
                return null;
            }
        });*/

        /*return Observable.defer(new Callable<SingleSource<JSONObject>>() {
            @Override public SingleSource<JSONObject> call() throws Exception {
                return NetworkService.requestController.sendRequestWithObservable(RequestConstructor.CGetRequestsIds(type), "SGetRequestsIds");
            }
        })*/
        return Single.defer(new Callable<SingleSource<JSONObject>>() {
            @Override public SingleSource<JSONObject> call() throws Exception {
                return NetworkService.requestController.sendRequestWithObservable(RequestConstructor.CGetRequestsIds(type), "SGetRequestsIds");
            }
        }).flatMap(new Function<JSONObject, SingleSource<JSONArray>>() {
            @Override public SingleSource<JSONArray> apply(@NonNull JSONObject jsonObject) throws Exception {
                final JSONArray active = new JSONArray();
                JSONArray jsonArray = jsonObject.getJSONArray("data");
                active.put(6322);
                if (jsonArray.getInt(1) > 0)
                    for (int i = 0; i < jsonArray.getInt(1); i++)
                        active.put(jsonArray.getJSONArray(2).getInt(i));
                return Single.just(active);
            }
        }).flatMap(new Function<JSONArray, SingleSource<JSONObject>>() {
            @Override public SingleSource<JSONObject> apply(@NonNull JSONArray jsonArray) throws Exception {
                if (jsonArray.length() > 1) {
                    return NetworkService.requestController.sendRequestWithObservable(RequestConstructor.CGetRequests(jsonArray), "SGetRequests");
                } else return Single.never();
            }
        }).flatMap(new Function<JSONObject, SingleSource<ArrayList<Request>>>() {
            @Override public SingleSource<ArrayList<Request>> apply(@NonNull JSONObject jsonObject) throws Exception {
                ArrayList<Request> activeRequests = RequestController.getRequestsFromGetRequests(jsonObject.toString(), type == 3);
                return Single.just(activeRequests);
            }
        }).cache();
    }

    ////// Getter-Setter Area, Go away ///////////////////////////////////////////////////////////////////////////////////


    public NewInviteTourist getInviteObserver() {
        return inviteObserver;
    }

    public void setInviteObserver(NewInviteTourist inviteObserver) {
        this.inviteObserver = inviteObserver;
    }

    public boolean isUpdateDataBase() {
        return isUpdateDataBase;
    }

    public void setIsUpdateDataBase(boolean isUpdateDataBase) {
        this.isUpdateDataBase = isUpdateDataBase;
    }

    public NewTouristRequest getRequestObserver() {
        return requestObserver;
    }

    public void setRequestObserver(NewTouristRequest requestObserver) {
        this.requestObserver = requestObserver;
    }

    public SparseArray<Request> getRequests() {
        return this.requests;
    }

    public void setRequests(SparseArray<Request> requests) {
        this.requests = requests;
    }

}
