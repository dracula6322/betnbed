package com.betnbed.controller;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.betnbed.RequestConstructor;
import com.betnbed.activity.MyHostActivity;
import com.betnbed.interfaces.BetnbedController;
import com.betnbed.interfaces.SocketInterface;
import com.betnbed.model.BaseModel;
import com.betnbed.model.IdRequest;
import com.betnbed.model.Request;
import com.betnbed.model.User;
import com.betnbed.service.NetworkService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.TimeZone;

public class RequestHostController implements BetnbedController {

    private boolean isUpdateDataBase = false;
    public ArrayList<Request> withOutInvite = new ArrayList<>();
    public ArrayList<Request> withInvite = new ArrayList<>();
    public ArrayList<Request> acceptOutInvite = new ArrayList<>();
    public ArrayList<Request> filtered = new ArrayList<>();
    private int typeMoney = 0;
    private long arrival = 0;
    private long departure = Integer.MAX_VALUE;

    private Context context;

    public RequestHostController(Context context) {
        this.context = context;
    }

    public void addRequests(ArrayList<Request> requests) {

        withOutInvite.clear();
        withInvite.clear();
        acceptOutInvite.clear();
        filtered.clear();

        for (int i = 0; i < requests.size(); i++)
            addRequest(requests.get(i));

        filtered.addAll(withOutInvite);

        isUpdateDataBase = true;
    }

    public void addRequest(Request request) {

        switch (request.getMask()) {
            case 4:
                withInvite.add(0, request);
                break;
            case 6:                     // Пригласивший без параметров
                withInvite.add(0, request);
                break;
            case 8:
                if (request.getArrival() * 1000 < System.currentTimeMillis() + TimeZone.getDefault().getRawOffset())
                    return;
                withOutInvite.add(0, request);
                break;
            case 16:
                request.setReasonInActive(Request.TOURIST_DELETE_REQUEST);
                withInvite.add(0, request);
                break;
            case 18:                    // // Пригласивший без параметров
                request.setReasonInActive(Request.TOURIST_DELETE_REQUEST);
                withInvite.add(0, request);
                break;
            case 32:
                request.setReasonInActive(Request.TOURIST_DELETE_OUR_INVITE);
                withInvite.add(0, request);
                break;
            case 34:                    // Без параметров
                request.setReasonInActive(Request.TOURIST_DELETE_OUR_INVITE);
                withInvite.add(0, request);
                break;
            case 48:
                request.setReasonInActive(Request.TOURIST_DELETE_OUR_INVITE);
                withInvite.add(0, request);
                break;
            case 50:                    // Без параметров
                request.setReasonInActive(Request.TOURIST_DELETE_OUR_INVITE);
                withInvite.add(0, request);
                break;
            case 64:
                acceptOutInvite.add(0, request);
                break;
            case 66:                    // Зеленый без совпадений
                acceptOutInvite.add(0, request);
                break;
            case 160:
                request.setReasonInActive(Request.TOURIST_AGREED_ANOTHER_INVITE);
                withInvite.add(0, request);
                break;
        }

    }

    public void GNewRequest(Request request) {
        if (request.getArrival() > arrival && request.getDeparture() < departure)
            if (typeMoney == 0)
                filtered.add(0, request);
            else if (typeMoney == request.getMoneyType() || request.getMoneyType() == 0)
                filtered.add(0, request);
        withOutInvite.add(0, request);
        LocalBroadcastManager.getInstance(context).sendBroadcast(new Intent(MyHostActivity.tagRefreshFragment).putExtra("FRAGMENT", 0));
    }

    public void GRequestCompleted(int idUser, int idRequest) {

        IdRequest requestId = new IdRequest(idUser, idRequest);

        for (int i = 0; i < withOutInvite.size(); i++)
            if (withOutInvite.get(i).equals(requestId)) {
                withOutInvite.remove(i);
                LocalBroadcastManager.getInstance(context).sendBroadcast(new Intent(MyHostActivity.tagRefreshFragment).putExtra("FRAGMENT", 0));
            }
        for (int i = 0; i < filtered.size(); i++)
            if (filtered.get(i).equals(requestId)) {
                filtered.remove(i);
                LocalBroadcastManager.getInstance(context).sendBroadcast(new Intent(MyHostActivity.tagRefreshFragment).putExtra("FRAGMENT", 0));
            }
    }

    public void GRequestDeleted(int idUser, int idRequest) {

        IdRequest requestId = new IdRequest(idUser, idRequest);

        for (int i = 0; i < withOutInvite.size(); i++)
            if (withOutInvite.get(i).equals(requestId)) {
                withOutInvite.remove(i);
                LocalBroadcastManager.getInstance(context).sendBroadcast(new Intent(MyHostActivity.tagRefreshFragment).putExtra("FRAGMENT", 0));
            }
        for (int i = 0; i < filtered.size(); i++)
            if (filtered.get(i).equals(requestId)) {
                filtered.remove(i);
                LocalBroadcastManager.getInstance(context).sendBroadcast(new Intent(MyHostActivity.tagRefreshFragment).putExtra("FRAGMENT", 0));
            }

        for (int i = 0; i < withInvite.size(); i++)
            if (withInvite.get(i).equals(requestId)) {
                withInvite.get(i).setReasonInActive(Request.TOURIST_DELETE_REQUEST);
                LocalBroadcastManager.getInstance(context).sendBroadcast(new Intent(MyHostActivity.tagRefreshFragment).putExtra("FRAGMENT", 1));
            }
    }

    public void GInviteStatus(int idUser, int idRequest, int type) {

        IdRequest requestId = new IdRequest(idUser, idRequest);

        if (type == 2) {
            for (int i = 0; i < withInvite.size(); i++) {
                if (withInvite.get(i).equals(requestId)) {
                    acceptOutInvite.add(0, withInvite.get(i));
                    withInvite.remove(i);
                    LocalBroadcastManager.getInstance(context).sendBroadcast(new Intent(MyHostActivity.tagRefreshFragment).putExtra("FRAGMENT", 2));
                    LocalBroadcastManager.getInstance(context).sendBroadcast(new Intent(MyHostActivity.tagRefreshFragment).putExtra("FRAGMENT", 1));
                    return;
                }
            }
        }
        if (type == 5) {
            for (int i = 0; i < withInvite.size(); i++)
                if (withInvite.get(i).equals(requestId)) {
                    withInvite.get(i).setReasonInActive(Request.TOURIST_DELETE_REQUEST);
                    LocalBroadcastManager.getInstance(context).sendBroadcast(new Intent(MyHostActivity.tagRefreshFragment).putExtra("FRAGMENT", 1));
                    return;
                }
        }
        if (type == 3) {
            for (int i = 0; i < withInvite.size(); i++)
                if (withInvite.get(i).equals(requestId)) {
                    withInvite.get(i).setReasonInActive(Request.TOURIST_DELETE_OUR_INVITE);
                    LocalBroadcastManager.getInstance(context).sendBroadcast(new Intent(MyHostActivity.tagRefreshFragment).putExtra("FRAGMENT", 1));
                    return;
                }
        }
    }

    public void deleteRequestFromWithOutInviteAndFiltered(int userId, int requestId) {
        IdRequest request = new IdRequest(userId, requestId);

        for (int i = 0; i < withOutInvite.size(); i++)
            if (withOutInvite.get(i).equals(request)) {
                withOutInvite.remove(i);
                break;
            }
        for (int i = 0; i < filtered.size(); i++)
            if (filtered.get(i).equals(request)) {
                filtered.remove(i);
                break;
            }
    }

    public void clearRequests() {
        isUpdateDataBase = false;
        withOutInvite.clear();
        withInvite.clear();
        acceptOutInvite.clear();
        filtered.clear();
        disableFilter();
    }

    public void applyFilter(long arrivalTime, long departureTime, int moneyType) {

        setArrival(arrivalTime);
        setDeparture(departureTime);
        setTypeMoney(moneyType);

        filtered.clear();
        for (int i = 0; i < withOutInvite.size(); i++)
            if (withOutInvite.get(i).getArrival() > arrival && withOutInvite.get(i).getDeparture() < departure)
                if (typeMoney == 0)
                    filtered.add(0, withOutInvite.get(i));
                else if (typeMoney == withOutInvite.get(i).getMoneyType() || withOutInvite.get(i).getMoneyType() == 0)
                    filtered.add(0, withOutInvite.get(i));

    }

    public void disableFilter() {
        typeMoney = 0;
        arrival = 0;
        departure = Integer.MAX_VALUE;
        filtered.clear();
        for (int i = 0; i < withOutInvite.size(); i++)
            filtered.add(0, withOutInvite.get(i));
    }

    public void getActiveRequest() {
        if (!isUpdateDataBase()) {
            NetworkService.requestController.sendRequest(RequestConstructor.HIsSubscribed(), "GIsSubscribed", new SocketInterface() {
                @Override
                public void onSuccess(String request) {
                    try {
                        final JSONObject answerIsSubscribed = new JSONObject(request);
                        if (answerIsSubscribed.getJSONArray("data").getString(0).length() > 0 || answerIsSubscribed.getJSONArray("data").getString(1).length() > 0) {

                            int WITH_OUR_INV = 0x004;                   // 4
                            int WITHOUT_OUR_INV = 0x008;                // 8
                            int DELETED_BY_CLIENT = 0x010;              // 16
                            int OUR_INV_DECLINED = 0x020;               // 32
                            int OUR_INV_ACCEPTED = 0x040;               // 64
                            int ENEMY_INV_ACCEPTED = 0x080;             // 128

                            final ArrayList<IdRequest> ids = new ArrayList<>();

                            NetworkService.requestController.sendRequest(RequestConstructor.HGetRequestsIds(WITH_OUR_INV | WITHOUT_OUR_INV | DELETED_BY_CLIENT | OUR_INV_ACCEPTED | OUR_INV_DECLINED | ENEMY_INV_ACCEPTED), "GGetRequestsIds", new SocketInterface() {
                                @Override
                                public void onSuccess(String request) {
                                    try {
                                        JSONObject jsonObject = new JSONObject(request).getJSONObject("data");
                                        JSONArray jsonArray_WITHOUT_OUR_INV = jsonObject.getJSONArray("4"); // Показывать без приглашения
                                        for (int i = 0; i < jsonArray_WITHOUT_OUR_INV.getInt(0); i++)
                                            ids.add(new IdRequest(jsonArray_WITHOUT_OUR_INV.getJSONArray(i + 1).getInt(0), jsonArray_WITHOUT_OUR_INV.getJSONArray(i + 1).getInt(1)));

                                        jsonArray_WITHOUT_OUR_INV = jsonObject.getJSONArray("8");           // Показать  с приглашениями
                                        for (int i = 0; i < jsonArray_WITHOUT_OUR_INV.getInt(0); i++)
                                            ids.add(new IdRequest(jsonArray_WITHOUT_OUR_INV.getJSONArray(i + 1).getInt(0), jsonArray_WITHOUT_OUR_INV.getJSONArray(i + 1).getInt(1)));

                                        jsonArray_WITHOUT_OUR_INV = jsonObject.getJSONArray("16");          // Показывать удаленные
                                        for (int i = 0; i < jsonArray_WITHOUT_OUR_INV.getInt(0); i++)
                                            ids.add(new IdRequest(jsonArray_WITHOUT_OUR_INV.getJSONArray(i + 1).getInt(0), jsonArray_WITHOUT_OUR_INV.getJSONArray(i + 1).getInt(1)));

                                        jsonArray_WITHOUT_OUR_INV = jsonObject.getJSONArray("32");
                                        for (int i = 0; i < jsonArray_WITHOUT_OUR_INV.getInt(0); i++)
                                            ids.add(new IdRequest(jsonArray_WITHOUT_OUR_INV.getJSONArray(i + 1).getInt(0), jsonArray_WITHOUT_OUR_INV.getJSONArray(i + 1).getInt(1)));

                                        jsonArray_WITHOUT_OUR_INV = jsonObject.getJSONArray("64");          // Принятые
                                        for (int i = 0; i < jsonArray_WITHOUT_OUR_INV.getInt(0); i++)
                                            ids.add(new IdRequest(jsonArray_WITHOUT_OUR_INV.getJSONArray(i + 1).getInt(0), jsonArray_WITHOUT_OUR_INV.getJSONArray(i + 1).getInt(1)));
                                    } catch (JSONException e) {
                                        Log.v("TAG", "Some error");
                                    }
                                }

                                @Override
                                public void onError(int error) {

                                }
                            });

                            if (ids.size() > 0) {
                                JSONArray data = new JSONArray().put(6322);
                                for (IdRequest idRequest:ids)
                                    data.put(new JSONArray().put(idRequest.getIdUser()).put(idRequest.getIdRequest()));
                                NetworkService.requestController.sendRequest(RequestConstructor.HGetRequests(data), "GGetRequests", new SocketInterface() {
                                    @Override
                                    public void onSuccess(String request) {
                                        addRequests(getRequestsFromHGetRequests(request));
                                    }

                                    @Override
                                    public void onError(int error) {
                                    }
                                });
                            }
                            LocalBroadcastManager.getInstance(context).sendBroadcast(new Intent(MyHostActivity.tagRefreshFragment).putExtra("FRAGMENT", 0));
                            LocalBroadcastManager.getInstance(context).sendBroadcast(new Intent(MyHostActivity.tagRefreshFragment).putExtra("FRAGMENT", 1));
                            LocalBroadcastManager.getInstance(context).sendBroadcast(new Intent(MyHostActivity.tagRefreshFragment).putExtra("FRAGMENT", 2));

                            setIsUpdateDataBase(true);

                        }
                    } catch (JSONException e) {
                        Log.v("TAG", e.toString());
                    }
                }

                @Override
                public void onError(int error) {
                }
            });

        }
    }

    private ArrayList<Request> getRequestsFromHGetRequests(String answer) {
        ArrayList<Request> result = new ArrayList<>();

        JSONArray data;
        try {
            JSONObject jsonRequest = new JSONObject(answer);
            data = jsonRequest.getJSONArray("data");

            for (int i = 0; i < data.getInt(1); i++) {

                JSONArray elem = data.getJSONArray(i + 2);


                BaseModel.BaseBuilder baseBuilder = new BaseModel.BaseBuilder()
                        .hotelType(elem.getInt(11))
                        .internet(elem.getInt(12))
                        .breakfast(elem.getInt(13))
                        .parking(elem.getInt(14))
                        .media(elem.getInt(15))
                        .shower(elem.getInt(16))
                        .kitchen(elem.getInt(17))
                        .pets(elem.getInt(18))
                        .services(elem.getInt(19))
                        .moneyType(elem.getInt(20));

                Request request = new Request.Builder()
                        .catsAndDogs(baseBuilder)
                        .senderId(elem.getInt(0))
                        .requestId(elem.getInt(1))
                        .place(elem.getString(2))
                        .arrival(elem.getLong(3))
                        .departure(elem.getLong(4))
                        .money(elem.getInt(5))
                        .currency(elem.getString(6))
                        .people(elem.getInt(7))
                        .special(elem.getString(8))
                        .mask(elem.getInt(10))
                        .isActive(elem.getInt(21) == 1)
                        .inviteId(elem.getInt(21))
                        .user(new User.Builder().firstName(elem.getString(22)).lastName(elem.getString(23)).photo(elem.getString(24)).build())
                        .build();


                result.add(request);
            }
        } catch (JSONException e) {
            Log.v("TAG", "Parser error " + e.toString());
            return result;
        }
        return result;
    }

    //Start Setter and Getter Session

    public boolean isUpdateDataBase() {
        return isUpdateDataBase;
    }

    public void setIsUpdateDataBase(boolean isUpdateDataBase) {
        this.isUpdateDataBase = isUpdateDataBase;
        if(!isUpdateDataBase){
            withOutInvite.clear();
            withInvite.clear();
            acceptOutInvite.clear();
            filtered.clear();
        }
    }

    public long getArrival() {
        return arrival;
    }

    public void setArrival(long arrival) {
        this.arrival = arrival;
    }

    public long getDeparture() {
        return departure;
    }

    public void setDeparture(long departure) {
        this.departure = departure;
    }

    public int getTypeMoney() {
        return typeMoney;
    }

    public void setTypeMoney(int typeMoney) {
        this.typeMoney = typeMoney;
    }

}
