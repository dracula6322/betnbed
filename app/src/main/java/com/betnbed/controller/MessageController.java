package com.betnbed.controller;

import android.content.Context;
import android.database.ContentObserver;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v4.content.Loader;
import android.support.v4.util.ArrayMap;
import android.util.Log;

import com.betnbed.RequestConstructor;
import com.betnbed.interfaces.SocketInterface;
import com.betnbed.model.Message;
import com.betnbed.service.NetworkService;
import com.betnbed.service.NotificationService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class MessageController {

    static private final int STATE_NOT_UPDATED = 0;
    static private final int STATE_UPDATED = 1;


    //static public int countUnreadMessage;
    static private int state;
    static private ArrayMap<String, Message> messages;
    static private SQLiteDatabase betnbedDatabase;
    static private Loader.ForceLoadContentObserver contentObserver;

    static public void init(Context context) {
        Log.v("TAG", "Message init   ");
        setState(STATE_NOT_UPDATED);
        betnbedDatabase = context.openOrCreateDatabase("betnbed.db", Context.MODE_PRIVATE, null);
        if(betnbedDatabase.getVersion() == 0) {
            betnbedDatabase.execSQL("DROP TABLE IF EXISTS MESSAGE");
            betnbedDatabase.execSQL("CREATE TABLE IF NOT EXISTS MESSAGE (" + "keyId TEXT PRIMARY KEY NOT NULL ," + "userId INTEGER NOT NULL ," + "id INTEGER NOT NULL ," + "time INTEGER NOT NULL ," + "message TEXT NOT NULL ," + "type INTEGER NOT NULL " + ")");
            betnbedDatabase.setVersion(1);
        } else {
            betnbedDatabase.execSQL("CREATE TABLE IF NOT EXISTS MESSAGE (" + "keyId TEXT PRIMARY KEY NOT NULL ," + "userId INTEGER NOT NULL ," + "id INTEGER NOT NULL ," + "time INTEGER NOT NULL ," + "message TEXT NOT NULL ," + "type INTEGER NOT NULL " + ")");
        }


        loadFromDatabase();

    }

    static private void loadFromDatabase() {


        Log.v("TAG", "We read from memory");
        //ContentValues contentValues = new ContentValues();

        messages = new ArrayMap<>();
        Cursor cursor = betnbedDatabase.rawQuery("SELECT * FROM MESSAGE WHERE userId=" + SettingsController.getAccoutUserId() + " ORDER BY time DESC", null);
        while (cursor.moveToNext()) {
            Message message = new Message(cursor.getInt(3), cursor.getInt(2), cursor.getString(4), cursor.getInt(5));
            Log.v("TAG", message.toString());
            messages.put(SettingsController.getAccoutUserId() + "_" + message.getId(), message);
            //contentValues.put("type", message.getType() == Message.STATE_UNREADED_MESSAGE ? Message.STATE_READED_MESSAGE : Message.STATE_READED_PUSH);
            //betnbedDatabase.update("MESSAGE", contentValues, "id = " + message.getId(), null);
        }
        cursor.close();
        Log.v("TAG", "In memory " + messages.size() + " messages");


    }

    static public void downloadMessages() {
        if (NetworkService.requestController == null)
            return;

        final Set<String> download_ids = new TreeSet<>();

        loadFromDatabase();

        if (getState() == STATE_NOT_UPDATED) {
            NetworkService.requestController.sendRequest(RequestConstructor.CGetMessagesIds(0), "SGetMessagesIds", new SocketInterface() {
                @Override public void onSuccess(String request) {
                    try {
                        JSONObject jsonObject = new JSONObject(request);
                        JSONArray jsonArray = jsonObject.getJSONArray("data").getJSONArray(1);
                        if (jsonArray.length() == 0) return;
                        StringBuilder stringBuilder = new StringBuilder(jsonArray.toString());
                        List<String> messages_ids = Arrays.asList(stringBuilder.substring(1, stringBuilder.length() - 1).split(","));
                        for (String string : messages_ids) if (!messages.containsKey(SettingsController.getAccoutUserId() + "_" + string)) download_ids.add(string);

                    } catch (JSONException e) {Log.v("TAG", e.toString());}

                }

                @Override public void onError(int error) {}
            });
            NetworkService.requestController.sendRequest(RequestConstructor.CGetMessagesIds(3), "SGetMessagesIds", new SocketInterface() {
                @Override public void onSuccess(String request) {
                    try {
                        JSONObject jsonObject = new JSONObject(request);
                        JSONArray jsonArray = jsonObject.getJSONArray("data").getJSONArray(1);
                        if (jsonArray.length() == 0) return;
                        StringBuilder stringBuilder = new StringBuilder(jsonArray.toString());
                        List<String> messages_ids = Arrays.asList(stringBuilder.substring(1, stringBuilder.length() - 1).split(","));
                        for (String string : messages_ids) if (!messages.containsKey(SettingsController.getAccoutUserId() + "_" + string)) download_ids.add(string);

                    } catch (JSONException e) {Log.v("TAG", e.toString());}
                }

                @Override public void onError(int error) {}
            });

            Log.v("TAG", "In server need download " + download_ids.toString());
            if (download_ids.size() != 0) {

                JSONArray jsonArray = new JSONArray();
                for(String string : download_ids)
                    jsonArray.put(Integer.parseInt(string));

                JSONObject data = new JSONObject();
                try {

                    data.put("ids", jsonArray);

                } catch (JSONException e){
                    Log.v("TAG", e.toString());
                }

                NetworkService.requestController.sendRequest(RequestConstructor.CGetMessages(data), "SGetMessages", new SocketInterface() {
                    @Override public void onSuccess(String request) {
                        try {
                            JSONObject jsonObject = new JSONObject(request);
                            JSONArray jsonMass = jsonObject.getJSONArray("data").getJSONArray(1);
                            for (int i = 0; i < jsonMass.length(); i++) {
                                JSONArray jsonArray = jsonMass.getJSONArray(i);
                                addMessage(new Message(jsonArray.getInt(2), jsonArray.getInt(0), jsonArray.getString(1), jsonArray.getInt(3)));
                            }


                        } catch (JSONException ignored) {}
                    }

                    @Override public void onError(int error) {}
                });
            }
            setState(STATE_UPDATED);
        }
    }

    static public void addMessage(final Message message) {
        Log.v("TAG", "We want addMessage " + message.toString());
        if (!messages.containsKey(SettingsController.getAccoutUserId() + "_" + message.getId()) && contentObserver == null) {
            NotificationService.incCountUnreadMessage();

        }
        try {
            String request = "INSERT OR REPLACE INTO MESSAGE (keyId ,userId, id, time, message, type) values " +
                    "('" + SettingsController.getAccoutUserId() + "_" + message.getId() + "'," + SettingsController.getAccoutUserId() + "," + message.getId() + "," + message.getTime() + ",'" + message.getText() + "'," + message.getType() + ")";
            betnbedDatabase.execSQL(request);
            messages.put(SettingsController.getAccoutUserId() + "_" + message.getId(), message);
            if (contentObserver != null) contentObserver.dispatchChange(false, null);
        } catch (Exception e){
            Log.v("TAG", e.toString());
        }
    }


    static public void removeFromDatabase(int userId, int id) {

        betnbedDatabase.delete("MESSAGE", "keyId = \"" + userId + "_" + id + "\"", null);
        messages.remove(userId + "_" + id);
    }

    // Getter and Setter

    public static ContentObserver getContentObserver() {
        return contentObserver;
    }

    public static void setContentObserver(Loader.ForceLoadContentObserver contentObserver) {
        MessageController.contentObserver = contentObserver;
    }

    static public List<Message> getMessages() {
        List<Message> list = new ArrayList<>(messages.values());
        Collections.sort(list, new Comparator<Message>() {
            @Override
            public int compare(Message msg1, Message msg2) {
                return msg1.getTime() < msg2.getTime() ? 1 : -1;
            }
        });
        return list;
    }

    static public void setMessages(ArrayMap<String, Message> _messages) {messages = _messages;}

    public static int getState() {
        return state;
    }

    public static void setState(int state) {
        MessageController.state = state;
    }
}
