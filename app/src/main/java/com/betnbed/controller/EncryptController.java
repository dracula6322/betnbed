package com.betnbed.controller;

import android.util.Base64;
import android.util.Log;

import com.betnbed.BuildConfig;

import java.io.IOException;
import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.RSAPublicKeySpec;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class EncryptController {

    private static byte[] key;
    private static final SecureRandom random = new SecureRandom(SecureRandom.getSeed(32));
    private static final int SECRET_KEY_BYTE_LENGTH = 32;
    private static final String INIT_VECTOR = "betnbad123456789";

    public static BigInteger generateSecretKey() {
        return new BigInteger(1, random.generateSeed(SECRET_KEY_BYTE_LENGTH));
    }

    public static byte[] md5Custom(String st) {
        MessageDigest messageDigest;
        byte[] digest = new byte[0];
        try {
            messageDigest = MessageDigest.getInstance("MD5");
            messageDigest.reset();
            messageDigest.update(st.getBytes());
            digest = messageDigest.digest();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return digest;
    }

    public static byte[] md5Custom(byte[] st) {
        MessageDigest messageDigest;
        byte[] digest = new byte[0];
        try {
            messageDigest = MessageDigest.getInstance("MD5");
            messageDigest.reset();
            digest = messageDigest.digest(st);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return digest;
    }

    public static void setKey(byte[] _key) {
        key = _key;
    }

    public static String encryptAES (String value) {
        try {
			IvParameterSpec iv = new IvParameterSpec(INIT_VECTOR.getBytes("UTF-8"));
			SecretKeySpec secretKeySpec = new SecretKeySpec(key, "AES");
			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
			cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec, iv);

			return Base64.encodeToString(cipher.doFinal(value.getBytes()), Base64.NO_WRAP);
		} catch (Exception ex) {
			Log.v("TAG", "encryptAES error = " + ex.toString());

		}
		return null;
	}

    public static String decryptAES(String encrypted) {
        try {
            IvParameterSpec iv = new IvParameterSpec(INIT_VECTOR.getBytes("UTF-8"));
            SecretKeySpec skeySpec = new SecretKeySpec(key, "AES");

            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
            cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);

            byte[] original = cipher.doFinal(Base64.decode(encrypted, Base64.NO_WRAP));

            return new String(original);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }


    private static byte[] rsaDecrypt(String base64, BigInteger m, BigInteger d) throws InvalidKeyException, NoSuchPaddingException, NoSuchAlgorithmException, BadPaddingException, IllegalBlockSizeException, InvalidKeySpecException {
        RSAPublicKeySpec keySpec = new RSAPublicKeySpec(m, d);
        KeyFactory fact = KeyFactory.getInstance("RSA");
        PublicKey pubKey = fact.generatePublic(keySpec);
        Cipher cipher = Cipher.getInstance("RSA");
        cipher.init(Cipher.DECRYPT_MODE, pubKey);
        return cipher.doFinal(Base64.decode(base64, Base64.NO_CLOSE));
    }

    public static String rsaDecrypt(String base64) throws NoSuchPaddingException, NoSuchAlgorithmException, IOException, BadPaddingException, IllegalBlockSizeException, InvalidKeyException, InvalidKeySpecException {
        String KEY_MODULUS = BuildConfig.serverKey;
        String CLIENT_KEY_EXPONENT = "65537";
        byte[] decrypt = rsaDecrypt(base64, new BigInteger(KEY_MODULUS), new BigInteger(CLIENT_KEY_EXPONENT));
        String string = new String(decrypt, "UTF-8");
        return string.substring(string.indexOf('['),string.length());
    }

}