package com.betnbed.fragment;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.graphics.drawable.VectorDrawableCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.betnbed.Application;
import com.betnbed.Heap;
import com.betnbed.R;
import com.betnbed.SC;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;

import org.json.JSONObject;

import java.util.Calendar;
import java.util.GregorianCalendar;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SettingsProfileTouristEditsText extends Fragment implements PlaceSelectionListener {

    private int selectedGender = Heap.accountUser.getGender();
    private String placeBirthDayId = "";

    @BindView(R.id.progress) ProgressBar progress;
    @BindView(R.id.firstName) EditText firstName;
    @BindView(R.id.secondName) EditText secondName;
    @BindView(R.id.gender_spinner) TextView gender;
    @BindView(R.id.place_birthday) TextView placeBirthday;
    @BindView(R.id.date_birthday) TextView dateBirthday;
    @BindView(R.id.clear_view) View clear;
    @BindView(R.id.occupation) EditText occupation;

    public JSONObject getSaveRequestSetting() throws Exception {

        JSONObject jsonObject = new JSONObject();

        if (!Heap.accountUser.getFirstName().equals(firstName.getText().toString())) jsonObject.put("fname", firstName.getText().toString());
        if (!Heap.accountUser.getLastName().equals(secondName.getText().toString())) jsonObject.put("lname", secondName.getText().toString());
        if (Heap.accountUser.getGender() != selectedGender) jsonObject.put("gender", selectedGender);
        if (!Heap.accountUser.getDateBirthday().equals(dateBirthday.getText().toString())) jsonObject.put("birth", dateBirthday.getText().toString());
        if (!Heap.accountUser.getProfession().equals(occupation.getText().toString())) jsonObject.put("prof", occupation.getText().toString());
        if (!Heap.accountUser.getIdCity().equals(placeBirthDayId)) jsonObject.put("city", placeBirthDayId);

        return jsonObject;
    }

    public SettingsProfileTouristEditsText() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_settings_user_profile_some_edit_text, container, false);

        ButterKnife.bind(this, v);
        Drawable closeImage = VectorDrawableCompat.create(getResources(), R.drawable.ic_close_black_24dp, getActivity().getTheme());
        clear.setBackground(closeImage);


        firstName.setText(Heap.accountUser.getFirstName());

        secondName.setText(Heap.accountUser.getLastName());

        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                placeBirthday.setText("");
                placeBirthDayId = "";
                clear.setVisibility(View.GONE);
            }
        });

        if (!Heap.accountUser.getIdCity().equals(""))
            Places.GeoDataApi.getPlaceById(Application.mGoogleApiClient, Heap.accountUser.getIdCity())
                    .setResultCallback(new ResultCallback<PlaceBuffer>() {
                        @Override
                        public void onResult(@NonNull PlaceBuffer places) {
                            if (places.getStatus().isSuccess() && places.getCount() > 0) {
                                placeBirthday.setText(places.get(0).getAddress().toString());
                                clear.setVisibility(View.VISIBLE);
                            }
                            places.release();
                        }
                    });
        else placeBirthday.setText("");
        if (Heap.accountUser.getFullNameCity() != null)
            if (Heap.accountUser.getFullNameCity().length() > 0)
                clear.setVisibility(View.VISIBLE);
        placeBirthday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    placeBirthday.setClickable(false);
                    progress.setVisibility(View.VISIBLE);
                    AutocompleteFilter typeFilter = new AutocompleteFilter.Builder().setTypeFilter(AutocompleteFilter.TYPE_FILTER_CITIES).setTypeFilter(AutocompleteFilter.TYPE_FILTER_REGIONS).build();
                    Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY).setFilter(typeFilter).build(getActivity());
                    startActivityForResult(intent, 1);
                } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
                    Log.v("TAG", "Error" + e.toString());
                }
            }
        });


        dateBirthday.setText(Heap.accountUser.getDateBirthday());
        dateBirthday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog dateBirthdayDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        Calendar calendar = new GregorianCalendar(year, monthOfYear, dayOfMonth);
                        dateBirthday.setText(SC.dfDayBirthday.format(calendar.getTime()));
                    }
                }, Calendar.getInstance().get(Calendar.YEAR), Calendar.getInstance().get(Calendar.MONTH), Calendar.getInstance().get(Calendar.DAY_OF_MONTH));
                dateBirthdayDialog.show();
            }
        });


        occupation.setText(Heap.accountUser.getProfession());


        if (selectedGender != 0)
            gender.setText(getResources().getStringArray(R.array.gendersInSettins)[selectedGender]);

        v.findViewById(R.id.gender_layout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builderSingle = new AlertDialog.Builder(getContext());
                builderSingle.setTitle("Укажите пол");
                builderSingle.setIcon(R.drawable.png_avatar_orange);
                final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(getContext(), R.layout.list_view_gender);
                arrayAdapter.add("Не указывать");
                arrayAdapter.add("Женский");
                arrayAdapter.add("Мужской");

                builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        selectedGender = which;
                        if (selectedGender != 0)
                            gender.setText(getResources().getStringArray(R.array.gendersInSettins)[which]);
                        else gender.setText("");
                    }
                });
                builderSingle.show();
            }
        });

        return v;
    }

    @Override
    public void onError(Status status) {
        Log.v("TAG", "Google onError()");
    }

    @Override
    public void onPlaceSelected(Place place) {
        progress.setVisibility(View.GONE);
        placeBirthday.setClickable(true);
        placeBirthday.setTypeface(null, Typeface.ITALIC);

    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        progress.setVisibility(View.GONE);
        placeBirthday.setClickable(true);
        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(getContext(), data);
                placeBirthday.setText(place.getAddress());
                placeBirthDayId = place.getId();
                clear.setVisibility(View.VISIBLE);
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(getContext(), data);
                this.onError(status);
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}