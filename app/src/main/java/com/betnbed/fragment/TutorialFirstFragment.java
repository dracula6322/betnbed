package com.betnbed.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.betnbed.R;

public class TutorialFirstFragment extends Fragment {

    private int type;

    static public TutorialFirstFragment newInstance(int type) {

        TutorialFirstFragment fragment = new TutorialFirstFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("type", type);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        type = getArguments().getInt("type");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        switch (type) {
            case 1:
                return inflater.inflate(R.layout.fragment_first_tourist_tutorial, container, false);
            case 2:
                return inflater.inflate(R.layout.fragment_first_hotel_tutorial, container, false);
            case 3:
                return inflater.inflate(R.layout.fragment_first_host_tutorial, container, false);
            default:
                throw new RuntimeException("Type error");
        }
    }

}
