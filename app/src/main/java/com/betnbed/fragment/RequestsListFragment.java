package com.betnbed.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.betnbed.Heap;
import com.betnbed.R;
import com.betnbed.adapters.RequestRecyclerViewAdapter;
import com.betnbed.interfaces.MovingToTab;
import com.betnbed.interfaces.NewTouristRequest;
import com.betnbed.model.Request;
import com.betnbed.service.NotificationService;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RequestsListFragment extends Fragment {

    public RequestRecyclerViewAdapter mAdapter;

    NewTouristRequest newTouristRequest = new NewTouristRequest() {
        @Override public void onNewRequest() {
            getActivity().runOnUiThread(new Runnable() {
                @Override public void run() {
                    mAdapter.setContents(Heap.accountUser.getRequestTouristController().getRequests());
                }
            });

        }
    };

    private MovingToTab movingToTab;

    @BindView(R.id.emptyLayout) public View emptyLayout;
    @BindView(R.id.requestList) public RecyclerView rc;

    @OnClick(R.id.createRequest) void createRequest() {
        movingToTab.moveTo(1);
    }

    static public RequestsListFragment newInstance() {

        RequestsListFragment fragment = new RequestsListFragment();
        return fragment;
    }

    @Override public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



        mAdapter = new RequestRecyclerViewAdapter(Heap.accountUser.getRequestTouristController().getRequests());
        mAdapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onItemRangeChanged(int positionStart, int itemCount) {
                super.onItemRangeChanged(positionStart, itemCount);
                rc.setVisibility(itemCount == 0 ? View.GONE : View.VISIBLE);
                emptyLayout.setVisibility(itemCount == 0 ? View.VISIBLE : View.GONE);
            }
        });

        Heap.accountUser.getRequestTouristController().setRequestObserver(newTouristRequest);

        Heap.accountUser.getRequestTouristController().getActiveRequestsAndInvites();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_list_request, container, false);

        ButterKnife.bind(this, view);
        initView();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();


        NotificationService.setCountUnreadInvites(0);
        NotificationService.hideNewInviteNotiications();

        mAdapter.notifyItemRangeChanged(0, Heap.accountUser.getRequestTouristController().getRequests().size());
        mAdapter.notifyDataSetChanged();

    }

    private void initView() {

        rc.setLayoutManager(new LinearLayoutManager(getActivity()));
        rc.setAdapter(mAdapter);

    }

    public MovingToTab getMovingToTab() {
        return movingToTab;
    }

    public void setMovingToTab(MovingToTab movingToTab) {
        this.movingToTab = movingToTab;
    }


    public void setContents(SparseArray<Request> contents) {
        mAdapter.setContents(contents);
    }
}