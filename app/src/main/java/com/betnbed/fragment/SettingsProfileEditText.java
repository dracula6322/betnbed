package com.betnbed.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.betnbed.Heap;
import com.betnbed.R;

import org.json.JSONObject;

public class SettingsProfileEditText extends Fragment {

    private EditText content;
    private int type;

    static public SettingsProfileEditText newInstance(int type) {

        SettingsProfileEditText fragment = new SettingsProfileEditText();
        Bundle bundle = new Bundle();
        bundle.putInt("type", type);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        type = getArguments().getInt("type");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_settings_profile_edit_text, container, false);

        content = (EditText) v.findViewById(R.id.content);
        content.setSelection(content.getText().length());
        switch (type) {
            case 1:
                content.setText(Heap.accountUser.getFirstLongDesc());
                content.setHint(getString(R.string.my_interest_my));
                break;
            case 2:
                content.setText(Heap.accountUser.getSecondLongDesc());
                content.setHint(getString(R.string.something_interesting_about_me_my));
                break;
            case 3:
                content.setText(Heap.accountUser.getThirdLongDesc());
                content.setHint(getString(R.string.teach_learn_share_my));
                break;
            case 11:
                content.setText(Heap.accountHotel.getFirstLongDesc());
                content.setHint(getString(R.string.hotel_and_territory_my));
                break;
            case 12:
                content.setText(Heap.accountHotel.getSecondLongDesc());
                content.setHint(getString(R.string.neighborhood_my));
                break;
            case 13:
                content.setText(Heap.accountHotel.getThirdLongDesc());
                content.setHint(getString(R.string.functions_my));
                break;
            case 14:
                content.setText(Heap.accountHotel.getFourLongDesc());
                content.setHint(getString(R.string.interesting_my));
                break;
        }
        return v;
    }

    public JSONObject getContent() {
        try {
            JSONObject jsonObject = new JSONObject();
            String value;
            switch (type) {
                case 1:
                    value = Heap.accountUser.getFirstLongDesc();
                    break;
                case 2:
                    value = Heap.accountUser.getSecondLongDesc();
                    break;
                case 3:
                    value = Heap.accountUser.getThirdLongDesc();
                    break;
                case 11:
                    value = Heap.accountHotel.getFirstLongDesc();
                    break;
                case 12:
                    value = Heap.accountHotel.getSecondLongDesc();
                    break;
                case 13:
                    value = Heap.accountHotel.getThirdLongDesc();
                    break;
                case 14:
                    value = Heap.accountHotel.getFourLongDesc();
                    break;
                default:
                    throw new RuntimeException("Type error");
            }

            if (!value.equals(content.getText().toString()))
                switch (type) {
                    case 1:
                        jsonObject.put("interests", content.getText().toString());
                        break;
                    case 2:
                        jsonObject.put("funny", content.getText().toString());
                        break;
                    case 3:
                        jsonObject.put("skills", content.getText().toString());
                        break;
                    case 11:
                        jsonObject.put("territory", content.getText().toString());
                        break;
                    case 12:
                        jsonObject.put("neighborhood", content.getText().toString());
                        break;
                    case 13:
                        jsonObject.put("functions", content.getText().toString());
                        break;
                    case 14:
                        jsonObject.put("interesting", content.getText().toString());
                        break;
                    default:
                        throw new RuntimeException("Type error");
                }

            return jsonObject;
        } catch (Exception ignored) {

        }
        return new JSONObject();
    }

}
