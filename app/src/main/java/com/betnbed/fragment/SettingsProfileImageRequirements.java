package com.betnbed.fragment;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.betnbed.Heap;
import com.betnbed.R;
import com.betnbed.controller.SettingsController;
import com.betnbed.model.Hotel;
import com.bumptech.glide.Glide;

import org.json.JSONObject;

public class SettingsProfileImageRequirements extends Fragment implements View.OnClickListener {
    public SettingsProfileImageRequirements() {
    }

    private final int[] massColor = {Color.GRAY, Color.parseColor("#ff009ff0")};
    private TextView requirements_desc;

    private static final int[][] imageMass = {
            {R.id.homeHouseLayout, 3, R.drawable.png_both_blue, R.drawable.png_hotel_blue, R.drawable.png_home_blue},
            {R.id.internetLayout, 2, R.drawable.png_wifi_gray, R.drawable.png_wifi_blue},
            {R.id.breakfastLayout, 2, R.drawable.png_breakfast_gray, R.drawable.png_breakfast_blue},
            {R.id.parkingLayout, 2, R.drawable.png_parking_gray, R.drawable.png_parking_blue},
            {R.id.mediaLayout, 2, R.drawable.png_tv_gray, R.drawable.png_tv_blue},
            {R.id.bathLayout, 2, R.drawable.png_wash_grey, R.drawable.png_wash_blue},
            {R.id.kitchenLayout, 2, R.drawable.png_kitchen_gray, R.drawable.png_kitchen_blue},
            {R.id.animalLayout, 2, R.drawable.png_animal_gray, R.drawable.png_animal_blue},
            {R.id.serviceLayout, 2, R.drawable.png_service_gray, R.drawable.png_service_blue},
            {R.id.coinLayout, 2, R.drawable.png_both_blue, R.drawable.png_credit_card_blue, R.drawable.png_cash_blue}
    };

    private final String[][] titleMass = {
            {null, null, null},
            {"Интернет", "Интернет"},
            {"Завтрак", "Завтрак"},
            {"Парковка", "Парковка"},
            {"Медиа", "Медиа"},
            {"Ванная", "Ванная"},
            {"Кухня", "Кухня"},
            {"Животные", "Животные"},
            {"Сервисы", "Сервисы"},
            {"На выбор гостя", "Приму карту", "Приму наличные"}
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_settings_profile_image_requirements, container, false);


        requirements_desc = (TextView) v.findViewById(R.id.requirements_desc);
        requirements_desc.setMovementMethod(new ScrollingMovementMethod());
        //requirements_desc.setText(getResources().getStringArray(R.array.image_requirements_desc)[0]);


        LinearLayout linearLayoutTypeHost = (LinearLayout) v.findViewById(imageMass[0][0]);
        imageMass[0][1] = Heap.accountHotel.getType();
        if (SettingsController.isHotel()) linearLayoutTypeHost.setOnClickListener(this);
        else linearLayoutTypeHost.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                requirements_desc.setText("Иконка \"Отдельные номера\" установлена в случае, если Вы можете разместить гостя в отдельном номере.\nИконка \"Места в хостеле\" установлена в случае, если Вы сдаете места в общих номерах.\nИконка \"Хостел с номерами\" установлена в случае, если Вы имеете возможность разместить гостя как в отдельном номере, так и в общем.\nИконка \"Дом\" установлена в случае, если Вы владелец частного жилья, сдаёте в аренду дом, апартаменты, комнату или прочие места для проживания Ваших будущих гостей.");
            }
        });
        TextView hotelHostelTextView = (TextView) (linearLayoutTypeHost).getChildAt(0);
        ImageView hotelHostelImageView = (ImageView) (linearLayoutTypeHost).getChildAt(1);
        switch (imageMass[0][1]) {
            case Hotel.HOST_TYPE:
                hotelHostelTextView.setText("Дом");
                hotelHostelImageView.setImageResource(R.drawable.png_home_blue);
                break;
            case Hotel.HOTEL_TYPE:
                hotelHostelTextView.setText("Отдельные номера");
                hotelHostelImageView.setImageResource(R.drawable.png_hotel_blue);
                break;
            case Hotel.HOSTEL_TYPE:
                hotelHostelTextView.setText("Места в хостеле");
                hotelHostelImageView.setImageResource(R.drawable.png_hostel_blue);
                break;
            case Hotel.HOTEL_TYPE | Hotel.HOSTEL_TYPE:
                hotelHostelTextView.setText("Хостел с номерами");
                hotelHostelImageView.setImageResource(R.drawable.png_two);
                break;
        }

        for (int i = 1; i < imageMass.length; i++) {
            LinearLayout linearLayout = (LinearLayout) v.findViewById(imageMass[i][0]);
            linearLayout.setOnClickListener(this);
            if (Heap.accountHotel.getMassAdditionalInfo()[i] != 0 && i != 0) {

                imageMass[i][1] = 2 + Heap.accountHotel.getMassAdditionalInfo()[i];
                if (linearLayout.getId() != R.id.coinLayout)
                    ((TextView) (linearLayout).getChildAt(0)).setTextColor(massColor[imageMass[i][1] - 2]);
                ((TextView) (linearLayout).getChildAt(0)).setText(titleMass[i][imageMass[i][1] - 2]);
                ((ImageView) (linearLayout).getChildAt(1)).setImageResource(imageMass[i][imageMass[i][1]]);
            }
        }


        return v;
    }

    public void onClick(final View v) {

        //if (v.getId() == imageMass[0][1])
        //requirements_desc.setText(getResources().getStringArray(R.array.image_requirements_desc)[0]);

        if (imageMass[0][0] == v.getId()) {
            requirements_desc.setText("Иконка \"Отдельные номера\" установлена в случае, если Вы можете разместить гостя в отдельном номере.\nИконка \"Места в хостеле\" установлена в случае, если Вы сдаете места в общих номерах.\nИконка \"Хостел с номерами\" установлена в случае, если Вы имеете возможность разместить гостя как в отдельном номере, так и в общем.\nИконка \"Дом\" установлена в случае, если Вы владелец частного жилья, сдаёте в аренду дом, апартаменты, комнату или прочие места для проживания Ваших будущих гостей.");
            final View layout = getActivity().getLayoutInflater().inflate(R.layout.hotel_hostel_popup, null);
            final int[] currentState = {imageMass[0][1]};

            final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                    .setView(layout)
                    .setTitle("Варианты размещения");

            final AlertDialog dialog = builder.create();
            dialog.show();

            final LinearLayout first = (LinearLayout) layout.findViewById(R.id.first);
            final LinearLayout second = (LinearLayout) layout.findViewById(R.id.second);

            final int defaultColor = ((TextView) first.getChildAt(0)).getCurrentTextColor();

            if ((currentState[0] & Hotel.HOTEL_TYPE) == Hotel.HOTEL_TYPE)
                ((TextView) first.getChildAt(0)).setTextColor(Color.parseColor("#00a0f0"));
            if ((currentState[0] & Hotel.HOSTEL_TYPE) == Hotel.HOSTEL_TYPE)
                ((TextView) second.getChildAt(0)).setTextColor(Color.parseColor("#00a0f0"));

            Glide.with(getActivity()).load((currentState[0] & Hotel.HOTEL_TYPE) == Hotel.HOTEL_TYPE ? R.drawable.png_hotel_blue : R.drawable.png_hotel_gray).into((ImageView) first.getChildAt(1));
            Glide.with(getActivity()).load((currentState[0] & Hotel.HOSTEL_TYPE) == Hotel.HOSTEL_TYPE ? R.drawable.png_hostel_blue : R.drawable.png_hostel_gray).into((ImageView) second.getChildAt(1));

            layout.findViewById(R.id.button).setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View ignore) {
                    imageMass[0][1] = currentState[0];

                    TextView textView = (TextView) ((LinearLayout) v).getChildAt(0);
                    ImageView imageView = (ImageView) ((LinearLayout) v).getChildAt(1);

                    if (imageMass[0][1] == Hotel.HOSTEL_TYPE) {
                        textView.setText("Места в хостеле");
                        Glide.with(getActivity()).load(R.drawable.png_hostel_blue).into(imageView);
                    } else if (imageMass[0][1] == Hotel.HOTEL_TYPE) {
                        textView.setText("Отдельные номера");
                        Glide.with(getActivity()).load(R.drawable.png_hotel_blue).into(imageView);
                    } else {
                        textView.setText("Хостел с номерами");
                        Glide.with(getActivity()).load(R.drawable.png_two).into(imageView);
                    }
                    dialog.dismiss();

                }
            });
            layout.findViewById(R.id.first).setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {
                    currentState[0] ^= Hotel.HOTEL_TYPE;
                    ((TextView) first.getChildAt(0)).setTextColor((currentState[0] & Hotel.HOTEL_TYPE) == Hotel.HOTEL_TYPE ? Color.parseColor("#00a0f0") : defaultColor);
                    Glide.with(getActivity()).load((currentState[0] & Hotel.HOTEL_TYPE) == Hotel.HOTEL_TYPE ? R.drawable.png_hotel_blue : R.drawable.png_hotel_gray).into((ImageView) layout.findViewById(R.id.first_icon));
                    layout.findViewById(R.id.button).setEnabled(currentState[0] != 0);
                }
            });
            layout.findViewById(R.id.second).setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {
                    currentState[0] ^= Hotel.HOSTEL_TYPE;
                    ((TextView) second.getChildAt(0)).setTextColor((currentState[0] & Hotel.HOSTEL_TYPE) == Hotel.HOSTEL_TYPE ? Color.parseColor("#00a0f0") : defaultColor);
                    Glide.with(getActivity()).load((currentState[0] & Hotel.HOSTEL_TYPE) == Hotel.HOSTEL_TYPE ? R.drawable.png_hostel_blue : R.drawable.png_hostel_gray).into((ImageView) layout.findViewById(R.id.second_icon));
                    layout.findViewById(R.id.button).setEnabled(currentState[0] != 0);
                }
            });
        }

        for (int i = 1; i < imageMass.length; i++)
            if (imageMass[i][0] == v.getId()) {
                if (imageMass[i][1] != imageMass[i].length - 1)
                    imageMass[i][1]++;
                else imageMass[i][1] = 2;

                if (v.getId() != R.id.coinLayout)
                    ((TextView) ((LinearLayout) v).getChildAt(0)).setTextColor(massColor[imageMass[i][1] - 2]);
                ((TextView) ((LinearLayout) v).getChildAt(0)).setText(titleMass[i][imageMass[i][1] - 2]);
                ((ImageView) ((LinearLayout) v).getChildAt(1)).setImageResource(imageMass[i][imageMass[i][1]]);
                requirements_desc.setText(getResources().getStringArray(R.array.image_requirements_desc)[i]);
            }
    }

    public JSONObject getContent() throws Exception {

        JSONObject jsonObject = new JSONObject();

        if (SettingsController.isHotel())
            if (Heap.accountHotel.getType() != imageMass[0][1]) jsonObject.put("type", imageMass[0][1]);
        if (Heap.accountHotel.getInternet() != imageMass[1][1] - 2) jsonObject.put("internet", imageMass[1][1] - 2);
        if (Heap.accountHotel.getBreakfast() != imageMass[2][1] - 2) jsonObject.put("breakfast", imageMass[2][1] - 2);
        if (Heap.accountHotel.getParking() != imageMass[3][1] - 2) jsonObject.put("parking", imageMass[3][1] - 2);
        if (Heap.accountHotel.getMedia() != imageMass[4][1] - 2) jsonObject.put("media", imageMass[4][1] - 2);
        if (Heap.accountHotel.getShower() != imageMass[5][1] - 2) jsonObject.put("shower", imageMass[5][1] - 2);
        if (Heap.accountHotel.getKitchen() != imageMass[6][1] - 2) jsonObject.put("kitchen", imageMass[6][1] - 2);
        if (Heap.accountHotel.getPets() != imageMass[7][1] - 2) jsonObject.put("pets", imageMass[7][1] - 2);
        if (Heap.accountHotel.getServices() != imageMass[8][1] - 2) jsonObject.put("services", imageMass[8][1] - 2);
        if (Heap.accountHotel.getMoney() != imageMass[9][1] - 2) jsonObject.put("money", imageMass[9][1] - 2);

        return jsonObject;
    }


}