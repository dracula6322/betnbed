package com.betnbed.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.betnbed.Heap;
import com.betnbed.R;
import com.betnbed.adapters.MyGuestAcceptedListRecyclerViewAdapter;
import com.betnbed.adapters.MyGuestWaitingListRecyclerViewAdapter;
import com.betnbed.adapters.MyHostRecycleViewAdapter;
import com.betnbed.interfaces.MovingToTab;
import com.betnbed.service.NotificationService;

public class MyGuestFragment extends Fragment {

    private MovingToTab movingToTab;

    public MyHostRecycleViewAdapter adapter;
    public Handler handler;
    private int type;
    public View emptyLayout = null;
    private TextView tv;


    static public MyGuestFragment newInstance(int type) {
        MyGuestFragment fragment = new MyGuestFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("type", type);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void setArguments(Bundle args) {
        super.setArguments(args);
        this.type = args.getInt("type");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        switch (type) {
            case 0:
                return inflater.inflate(R.layout.fragment_myguest_current, container, false);
            case 1:
                return inflater.inflate(R.layout.fragment_myguest_waiting, container, false);
            default:
                return inflater.inflate(R.layout.fragment_myguest_accepted, container, false);
        }
    }

    @Override
    public void onViewCreated(final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        emptyLayout = view.findViewById(R.id.emptyLayout);

        switch (type) {
            case 0:

                handler = new Handler() {
                    public void handleMessage(android.os.Message msg) {

                        adapter.notifyItemRangeChanged(0, adapter.getItemCount());
                        adapter.notifyDataSetChanged();
                    }
                };
                break;
            case 1:
                adapter = new MyGuestWaitingListRecyclerViewAdapter();
                adapter.setContents(Heap.accountHotel.getRequestHostController().withInvite);
                adapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
                    @Override
                    public void onItemRangeChanged(int positionStart, int itemCount) {

                        super.onItemRangeChanged(positionStart, itemCount);
                        emptyLayout.setVisibility(itemCount == 0 ? View.VISIBLE : View.GONE);
                        adapter.notifyDataSetChanged();
                    }

                });
                handler = new Handler() {
                    public void handleMessage(android.os.Message msg) {

                        adapter.notifyItemRangeChanged(0, adapter.getItemCount());
                        adapter.notifyDataSetChanged();
                    }
                };
                break;
            case 2:

                adapter = new MyGuestAcceptedListRecyclerViewAdapter();
                adapter.setContents(Heap.accountHotel.requestHostController.acceptOutInvite);
                adapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
                    @Override
                    public void onItemRangeChanged(int positionStart, int itemCount) {
                        super.onItemRangeChanged(positionStart, itemCount);
                        emptyLayout.setVisibility(itemCount == 0 ? View.VISIBLE : View.GONE);
                        adapter.notifyDataSetChanged();

                    }
                });
                handler = new Handler() {
                    public void handleMessage(android.os.Message msg) {
                        adapter.notifyItemRangeChanged(0, adapter.getItemCount());
                        adapter.notifyDataSetChanged();
                    }
                };
                break;
        }

        if (type == 0)
            tv = (TextView) view.findViewById(R.id.desc);
        else
            view.findViewById(R.id.goToGuest).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (getMovingToTab() != null) movingToTab.moveTo(0);
                }
            });

        handler.sendEmptyMessage(0);

        final RecyclerView lv = (RecyclerView) view.findViewById(R.id.recycleView);
        lv.setLayoutManager(new LinearLayoutManager(getActivity()));
        lv.setAdapter(adapter);


    }


    @Override
    public void onResume() {
        super.onResume();

        NotificationService.setCountUnreadOurInviteAccepted(0);
        NotificationService.hideOurInviteAcceptedNotiications();

        handler.sendEmptyMessage(0);
        if (type == 0) {
            if (!Heap.accountHotel.getFullNameRegion().isEmpty())
                tv.setText("Здесь будут видны гости, которые хотят посетить " + Heap.accountHotel.getFullNameRegion());
            if (!Heap.accountHotel.getFullNameCity().isEmpty())
                tv.setText("Здесь будут видны гости, которые хотят посетить " + Heap.accountHotel.getFullNameCity());
            if (Heap.accountHotel.getFullNameCity().isEmpty() && Heap.accountHotel.getFullNameRegion().isEmpty())
                tv.setText("Здесь будут видны гости, которые хотят посетить Ваш город");
        }

    }

    public MovingToTab getMovingToTab() {
        return movingToTab;
    }

    public void setMovingToTab(MovingToTab movingToTab) {
        this.movingToTab = movingToTab;
    }
}
