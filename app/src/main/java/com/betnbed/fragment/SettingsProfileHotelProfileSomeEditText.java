package com.betnbed.fragment;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.graphics.drawable.VectorDrawableCompat;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.betnbed.Application;
import com.betnbed.Heap;
import com.betnbed.R;
import com.betnbed.activity.profile.ChousePointOnMapActivity;
import com.betnbed.controller.SettingsController;
import com.betnbed.interfaces.ApproveStatusChanged;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.app.Activity.RESULT_OK;

public class SettingsProfileHotelProfileSomeEditText extends Fragment implements PlaceSelectionListener {

    final int GET_REGION = 1;
    final int GET_CITY = 2;
    final int GET_MAP_POINT = 3;

    @BindView(R.id.phoneNumber) EditText phone;
    @BindView(R.id.contacts) EditText contact;
    @BindView(R.id.title) EditText title;
    @BindView(R.id.street) EditText street;
    @BindView(R.id.house) EditText house;
    @BindView(R.id.vat) EditText vat;

    @BindView(R.id.progressRegion) ProgressBar progressRegion;
    @BindView(R.id.progressCity) ProgressBar progressCity;

    @BindView(R.id.city) TextView city;
    @BindView(R.id.region) TextView region;
    @BindView(R.id.LatLng) TextView latlng;

    @BindView(R.id.clearCity) View clearCity;
    @BindView(R.id.clearRegion) View clearRegion;
    @BindView(R.id.clearMapPoint) View clearMapPoint;

    private LatLng latLng = Heap.accountHotel.getLatLng();
    private String regionId = Heap.accountHotel.getRegion();
    private String cityId = Heap.accountHotel.getCity();
    private LatLng regionPlace = Heap.accountHotel.getRegionPlace();
    private LatLng cityPlace = Heap.accountHotel.getCityPlace();

    @OnClick(R.id.city) void setCity() {
        try {
            city.setClickable(false);
            progressCity.setVisibility(View.VISIBLE);
            AutocompleteFilter typeFilter = new AutocompleteFilter.Builder().setTypeFilter(AutocompleteFilter.TYPE_FILTER_CITIES).build();
            Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY).setFilter(typeFilter).build(getActivity());
            startActivityForResult(intent, GET_CITY);
        } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
            Log.v("TAG", "Error" + e.toString());
        }
    }

    @OnClick(R.id.region) void setRegion() {
        try {
            region.setClickable(false);
            progressRegion.setVisibility(View.VISIBLE);
            AutocompleteFilter typeFilter = new AutocompleteFilter.Builder().setTypeFilter(AutocompleteFilter.TYPE_FILTER_REGIONS).build();
            Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY).setFilter(typeFilter).build(getActivity());
            startActivityForResult(intent, GET_REGION);
        } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
            Log.v("TAG", "Error" + e.toString());
        }
    }

    @OnClick(R.id.clearCity) void clearCity() {
        cityId = "";
        city.setText(cityId);
        cityPlace = null;
        Heap.accountHotel.setFullNameCity(cityId);
        clearCity.setVisibility(View.GONE);
    }

    @OnClick(R.id.clearRegion) void clearRegion() {
        regionId = "";
        region.setText(regionId);
        regionPlace = null;
        Heap.accountHotel.setFullNameRegion(regionId);
        clearRegion.setVisibility(View.GONE);
    }

    @OnClick(R.id.clearMapPoint) void clearMapPoint() {
        clearMapPoint.setVisibility(View.GONE);
        latLng = null;
        latlng.setText("Местоположение не указано");
    }

    @OnClick(R.id.LatLng) void openMapWindow() {

        Intent intent = new Intent(getActivity(), ChousePointOnMapActivity.class);

        Bundle bundle = new Bundle();

        if (latLng != null) {
            bundle.putParcelable("latLng", latLng);
            bundle.putInt("type", 1);
            intent.putExtras(bundle);
            startActivityForResult(intent, GET_MAP_POINT);
            //fragmentManager.beginTransaction().addToBackStack("").add(R.id.frame, fragment).commit();
            return;
        }
        try {
            if (!house.getText().toString().isEmpty() && !street.getText().toString().isEmpty() && !city.getText().toString().isEmpty()) {
                Geocoder geocoder = new Geocoder(getActivity());
                List<Address> addresses = geocoder.getFromLocationName(city.getText().toString() + " " + street.getText().toString() + " " + house.getText().toString(), 1);
                if (addresses.size() > 0) {
                    bundle.putParcelable("latLng", new LatLng(addresses.get(0).getLatitude(), addresses.get(0).getLongitude()));
                    bundle.putInt("type", 1);
                    intent.putExtras(bundle);
                    startActivityForResult(intent, GET_MAP_POINT);
                    //fragmentManager.beginTransaction().addToBackStack("").add(R.id.frame, fragment).commit();
                    return;
                }
            }
        } catch (IOException e) {
            Log.v("TAG", e.toString());
        }
        if (cityPlace != null) {
            bundle.putParcelable("latLng", cityPlace);
            bundle.putInt("type", 2);
            intent.putExtras(bundle);
            startActivityForResult(intent, GET_MAP_POINT);
            /*fragment.setArguments(bundle);
            fragmentManager.beginTransaction().addToBackStack("").add(R.id.frame, fragment).commit();*/
            return;
        }
        if (regionPlace != null) {
            bundle.putParcelable("latLng", regionPlace);
            bundle.putInt("type", 3);
            intent.putExtras(bundle);
            startActivityForResult(intent, GET_MAP_POINT);
            /*fragment.setArguments(bundle);
            fragmentManager.beginTransaction().addToBackStack("").add(R.id.frame, fragment).commit();*/
            return;
        }
        startActivityForResult(intent, GET_MAP_POINT);
        //fragmentManager.beginTransaction().addToBackStack("").add(R.id.frame, fragment).commit();
    }

    @Override public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Heap.accountHotel.setApproveStatusChanged(new ApproveStatusChanged() {
            @Override public void onChange(final int approveStatus) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override public void run() {
                        setIdEditTextsEnabled(approveStatus == 0);
                    }
                });
            }
        });
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_settings_hotel_profile_some_edit_text, container, false);

        ButterKnife.bind(this, v);

        Drawable blackClose = VectorDrawableCompat.create(getResources(), R.drawable.ic_close_black_24dp, getActivity().getTheme());
        Drawable whiteClose = VectorDrawableCompat.create(getResources(), R.drawable.ic_close_white_24dp, getActivity().getTheme());

        clearCity.setBackground(blackClose);
        clearRegion.setBackground(blackClose);
        clearMapPoint.setBackground(whiteClose);

        phone.setText(Heap.accountHotel.getPhone());
        contact.setText(Heap.accountHotel.getContacts());
        title.setText(Heap.accountHotel.getTitle());
        street.setText(Heap.accountHotel.getStreet());
        house.setText(Heap.accountHotel.getHouse_number());


        if (Heap.accountHotel.isHotel()) {
            title.setHint("Название отеля");
            vat.setInputType(InputType.TYPE_CLASS_PHONE);
            vat.setHint(getString(R.string.INN));
            vat.setText(Heap.accountHotel.getVat());
        } else {
            title.setHint("Придумайте название вашему хоста");
            v.findViewById(R.id.additionalInfo).setVisibility(View.GONE);
            vat.setHint(getString(R.string.asTouristsCanContactWithYou));
            vat.setText(Heap.accountHotel.getEmail());

        }


        if (latLng != null) {
            latlng.setText("Местоположение указано");
            clearMapPoint.setVisibility(View.VISIBLE);
        }


        if (SettingsController.isHotel()) {
            if (Heap.accountHotel.getApprove() == 0)
                setIdEditTextsEnabled(true);
            else setIdEditTextsEnabled(false);
        }


        if ((Heap.accountHotel.getRegion().length() != 0) && !(Heap.accountHotel.getFullNameRegion().length() > 0))
            Places.GeoDataApi.getPlaceById(Application.mGoogleApiClient, Heap.accountHotel.getRegion())
                    .setResultCallback(new ResultCallback<PlaceBuffer>() {
                        @Override
                        public void onResult(@NonNull PlaceBuffer places) {
                            if (places.getStatus().isSuccess() && places.getCount() > 0) {
                                Heap.accountHotel.setFullNameRegion(places.get(0).getAddress().toString());
                                region.setText(Heap.accountHotel.getFullNameRegion());
                                clearRegion.setVisibility(View.VISIBLE);
                            } else {

                                Heap.accountHotel.setFullNameRegion("");
                            }
                            places.release();
                        }
                    });
        else if ((Heap.accountHotel.getFullNameRegion().length() > 0)) {
            region.setText(Heap.accountHotel.getFullNameRegion());
            clearRegion.setVisibility(View.VISIBLE);
        }

        if ((Heap.accountHotel.getCity().length() != 0) && !(Heap.accountHotel.getFullNameCity().length() > 0))
            Places.GeoDataApi.getPlaceById(Application.mGoogleApiClient, Heap.accountHotel.getCity())
                    .setResultCallback(new ResultCallback<PlaceBuffer>() {
                        @Override
                        public void onResult(@NonNull PlaceBuffer places) {
                            if (places.getStatus().isSuccess() && places.getCount() > 0) {
                                Heap.accountHotel.setFullNameCity(places.get(0).getAddress().toString());
                                city.setText(Heap.accountHotel.getFullNameCity());
                                clearCity.setVisibility(View.VISIBLE);
                            } else {

                                Heap.accountHotel.setFullNameCity("");
                            }
                            places.release();
                        }
                    });
        else if ((Heap.accountHotel.getFullNameCity().length() > 0)) {
            city.setText(Heap.accountHotel.getFullNameCity());
            clearCity.setVisibility(View.VISIBLE);
        }
        return v;
    }

    @Override public void onDestroy() {
        super.onDestroy();
        Heap.accountHotel.setApproveStatusChanged(null);
    }

    public JSONObject getSaveRequestSetting() {

        try {
            JSONObject data = new JSONObject();
            if (!regionId.equals(Heap.accountHotel.getRegion())) data.put("region", regionId);
            if (!cityId.equals(Heap.accountHotel.getCity())) data.put("city", cityId);
            if (!contact.getText().toString().equals(Heap.accountHotel.getContacts())) data.put("contacts", contact.getText().toString());
            if (!phone.getText().toString().equals(Heap.accountHotel.getPhone())) data.put("phone", phone.getText().toString());
            if (!street.getText().toString().equals(Heap.accountHotel.getStreet())) data.put("street", street.getText().toString());
            if (!house.getText().toString().equals(Heap.accountHotel.getHouse_number())) data.put("house_number", house.getText().toString());
            if (!title.getText().toString().equals(Heap.accountHotel.getTitle())) data.put("title", title.getText().toString());

            if (Heap.accountHotel.isHotel()) {
                if (!vat.getText().toString().equals(Heap.accountHotel.getVat())) data.put("vat", vat.getText().toString());
            } else if (!vat.getText().toString().equals(Heap.accountHotel.getEmail())) data.put("email", vat.getText().toString());

            if (this.latLng != null) {

                if (Heap.accountHotel.getLatLng().latitude != this.latLng.latitude) data.put("latitude", String.valueOf(this.latLng.latitude));
                if (Heap.accountHotel.getLatLng().longitude != this.latLng.longitude) data.put("longitude", String.valueOf(this.latLng.longitude));
            } else {
                if (Heap.accountHotel.getLatLng() != null) {
                    data.put("latitude", "");
                    data.put("longitude", "");
                }
            }

            return data;
        } catch (Exception e) {
            Log.v("TAG", e.toString());
        }

        return new JSONObject();
    }

    @Override
    public void onError(Status status) {
        Log.v("TAG", "Google onError()");
    }

    @Override
    public void onPlaceSelected(Place place) {
        progressCity.setVisibility(View.GONE);
        progressRegion.setVisibility(View.GONE);
        region.setClickable(true);
        city.setClickable(true);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == RESULT_OK) {
            if (requestCode == GET_REGION) {
                Place place = PlaceAutocomplete.getPlace(getContext(), data);
                regionPlace = place.getLatLng();
                region.setText(place.getAddress());
                regionId = place.getId();
                clearRegion.setVisibility(View.VISIBLE);
            }
            if (requestCode == GET_CITY) {
                Place place = PlaceAutocomplete.getPlace(getContext(), data);
                cityPlace = place.getLatLng();
                city.setText(place.getAddress());
                cityId = place.getId();
                clearCity.setVisibility(View.VISIBLE);
            }
            if (requestCode == GET_MAP_POINT) {
                latLng = data.getParcelableExtra("latLng");
                latlng.setText("Местоположение указано");
                clearMapPoint.setVisibility(View.VISIBLE);
            }
        }
        if (requestCode == GET_REGION) {
            region.setClickable(true);
            progressRegion.setVisibility(View.GONE);
        }

        if (requestCode == GET_CITY) {
            city.setClickable(true);
            progressCity.setVisibility(View.GONE);
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    public void setIdEditTextsEnabled(boolean isEnabled) {
        title.setEnabled(isEnabled);
        latlng.setEnabled(isEnabled);
        region.setEnabled(isEnabled);
        city.setEnabled(isEnabled);
        street.setEnabled(isEnabled);
        house.setEnabled(isEnabled);
        contact.setEnabled(isEnabled);
        vat.setEnabled(isEnabled);
        phone.setEnabled(isEnabled);
        clearCity.setEnabled(isEnabled);
        clearRegion.setEnabled(isEnabled);
        clearMapPoint.setEnabled(isEnabled);
    }
}
