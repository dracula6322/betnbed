package com.betnbed.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.betnbed.R;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class ShowHotelInMapFragment extends Fragment {

    public static Fragment getInstance(int position) {

        Fragment showHotelFragment = new ShowHotelInMapFragment();

        return showHotelFragment;

    }

    View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        Log.v("TAG", "ShowHotelInMapFragment null check = " + (getView() == null) + "");

        if (view == null) {
            view = inflater.inflate(R.layout.fragment_invites_in_map, container, false);
            MapFragment mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map);

            mapFragment.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap googleMap) {

                    googleMap.getUiSettings().setMapToolbarEnabled(false);
                    googleMap.getUiSettings().setCompassEnabled(false);

                    googleMap.addMarker(new MarkerOptions().position(new LatLng(52.73169, 41.44326)));
                }
            });
        }


        return view;
    }

}
