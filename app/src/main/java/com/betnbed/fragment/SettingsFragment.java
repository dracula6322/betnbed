package com.betnbed.fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceFragmentCompat;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.betnbed.Heap;
import com.betnbed.R;
import com.betnbed.RequestConstructor;
import com.betnbed.RequestSender;
import com.betnbed.activity.NavigationActivity;
import com.betnbed.activity.login.AgreementActivity;
import com.betnbed.activity.tutorial.TutorialSettingsActivity;
import com.betnbed.controller.SettingsController;
import com.betnbed.interfaces.SocketInterface;
import com.betnbed.service.NetworkService;
import com.betnbed.service.NotificationService;

public class SettingsFragment extends PreferenceFragmentCompat {

    static public String LOGOUT_FROM_SETTINGS = "LOGOUT_FROM_SETTINGS";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addPreferencesFromResource(R.xml.settins);

        findPreference("touristHelp").setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                Intent intent = new Intent(getActivity(), TutorialSettingsActivity.class);
                intent.putExtra("delta", 0);
                startActivity(intent);
                //TutorialSettingsActivity fragment = TutorialSettingsFragment.newInstance(0);
                //getFragmentManager().beginTransaction().addToBackStack("").add(R.id.frame, fragment).commit();
                return false;
            }
        });

        findPreference("hotelHelp").setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                Intent intent = new Intent(getActivity(), TutorialSettingsActivity.class);
                intent.putExtra("delta", 5);
                startActivity(intent);
                //TutorialSettingsFragment fragment = TutorialSettingsFragment.newInstance(5);
                //getFragmentManager().beginTransaction().addToBackStack("").add(R.id.frame, fragment).commit();
                return false;
            }
        });


        findPreference("agreement").setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                Intent intent = new Intent(getActivity(), AgreementActivity.class);
                intent.putExtra("type", AgreementActivity.SHOW_TYPE);
                startActivity(intent);
                //Fragment settins = getFragmentManager().findFragmentByTag("settings");
                //AgreementFragment fragment = AgreementFragment.newInstance(AgreementFragment.SHOW_TYPE);
                //getFragmentManager().beginTransaction().hide(settins).addToBackStack("").add(R.id.frame, fragment).commit();
                return false;
            }
        });


        findPreference("informationAboutCompany").setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("Контактная информация")
                        .setMessage("Для обращений, связанных с работой сервиса Betnbed, пожалуйста, используйте следующие контактные данные:\n\npartner@betnbed.com\nfacebook.com/betnbed\n\nBetnbed© 2017  betnbed.com")
                        .setCancelable(true)
                        .setIcon(R.mipmap.ic_launcher);
                builder.create().show();
                return false;
            }
        });

        findPreference("changePassword").setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {

                if (SettingsController.getAccountLogin().isEmpty()) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage("При входе через социальные сети нельзя поменять пароль")
                            .setCancelable(true)
                            .setTitle("Смена пароля")
                            .setIcon(R.drawable.png_change_user_gray_orinal)
                            .setPositiveButton("Ok", null);

                    builder.create().show();
                } else {

                    LinearLayout linearLayout = new LinearLayout(getActivity());
                    linearLayout.setPadding(16, 32, 32, 16);
                    linearLayout.setFocusableInTouchMode(true);
                    linearLayout.setOrientation(LinearLayout.VERTICAL);
                    linearLayout.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));


                    final TextInputLayout oldTextInputLayout = new TextInputLayout(getActivity());
                    final TextInputLayout firstTextInputLayout = new TextInputLayout(getActivity());
                    final TextInputLayout secondTextInputLayout = new TextInputLayout(getActivity());


                    final EditText old = new EditText(getActivity());
                    old.setHint("Введите старый пароль");
                    old.setHintTextColor(Color.parseColor("#61000000"));
                    old.setTextColor(Color.BLACK);
                    old.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                    old.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    old.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                        }

                        @Override
                        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                            oldTextInputLayout.setError("");

                        }

                        @Override
                        public void afterTextChanged(Editable editable) {

                        }
                    });
                    oldTextInputLayout.addView(old);
                    oldTextInputLayout.setErrorEnabled(true);
                    oldTextInputLayout.setPasswordVisibilityToggleEnabled(true);
                    linearLayout.addView(oldTextInputLayout);

                    EditText firstNew = new EditText(getActivity());
                    firstNew.setHint("Введите новый пароль");
                    firstNew.setHintTextColor(Color.parseColor("#61000000"));
                    firstNew.setTextColor(Color.BLACK);
                    firstNew.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                    firstNew.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    firstNew.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                        }

                        @Override
                        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                            firstTextInputLayout.setError("");
                        }

                        @Override
                        public void afterTextChanged(Editable editable) {

                        }
                    });

                    firstTextInputLayout.addView(firstNew);
                    firstTextInputLayout.setPasswordVisibilityToggleEnabled(true);
                    firstTextInputLayout.setErrorEnabled(true);
                    linearLayout.addView(firstTextInputLayout);

                    EditText secondNew = new EditText(getActivity());
                    secondNew.setHint("Повторите новый пароль");
                    secondNew.setHintTextColor(Color.parseColor("#61000000"));
                    secondNew.setTextColor(Color.BLACK);
                    secondNew.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                    secondNew.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    secondNew.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                        }

                        @Override
                        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                            secondTextInputLayout.setError("");
                        }

                        @Override
                        public void afterTextChanged(Editable editable) {

                        }
                    });

                    secondTextInputLayout.addView(secondNew);
                    secondTextInputLayout.setPasswordVisibilityToggleEnabled(true);
                    secondTextInputLayout.setErrorEnabled(true);
                    linearLayout.addView(secondTextInputLayout);

                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                            .setTitle("Смена пароля")
                            .setIcon(R.drawable.png_change_password_gray)
                            .setPositiveButton("Поменять пароль", null)
                            .setNeutralButton("Отмена", null)
                            .setCancelable(false)
                            .setView(linearLayout);
                    final AlertDialog ad = builder.create();
                    ad.setOnShowListener(new DialogInterface.OnShowListener() {
                        @Override
                        public void onShow(final DialogInterface dialogInterface) {
                            ad.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    if (oldTextInputLayout.getEditText() != null)
                                        if (!oldTextInputLayout.getEditText().getText().toString().equals(SettingsController.getAccountPassword())) {
                                            oldTextInputLayout.setError("Старый пароль не верен");
                                            return;
                                        }


                                    if (firstTextInputLayout.getEditText() != null)
                                        if (firstTextInputLayout.getEditText().getText().toString().equals(SettingsController.getAccountPassword())) {
                                            firstTextInputLayout.setError("Новый пароль не может тем же самым");
                                            return;
                                        }

                                    if (firstTextInputLayout.getEditText().getText().toString().isEmpty()) {
                                        firstTextInputLayout.setError("Пароль не может быть пустым");
                                        return;
                                    }

                                    if (secondTextInputLayout.getEditText() != null)
                                        if (!firstTextInputLayout.getEditText().getText().toString().equals(secondTextInputLayout.getEditText().getText().toString())) {
                                            secondTextInputLayout.setError("Пароли разные");
                                            return;
                                        }

                                    NetworkService.requestController.sendRequest(RequestConstructor.CPassChange(SettingsController.getAccountPassword(), firstTextInputLayout.getEditText().getText().toString()), "SPassChanged", new SocketInterface() {
                                        @Override
                                        public void onSuccess(String request) {
                                            Toast.makeText(getActivity(), "Пароль изменен", Toast.LENGTH_SHORT).show();
                                            SettingsController.setTagAccountPassword(firstTextInputLayout.getEditText().getText().toString());
                                            dialogInterface.dismiss();
                                        }

                                        @Override
                                        public void onError(int error) {
                                            switch (error) {
                                                case 78:
                                                    Toast.makeText(getActivity(), "Пароль изменен", Toast.LENGTH_SHORT).show();
                                                    SettingsController.setTagAccountPassword(firstTextInputLayout.getEditText().getText().toString());
                                                    dialogInterface.dismiss();
                                                    break;
                                                default:
                                                    Toast.makeText(getActivity(), "Произошла ошибка " + error, Toast.LENGTH_SHORT).show();
                                                    break;
                                            }
                                        }
                                    });
                                }
                            });
                            ad.getButton(AlertDialog.BUTTON_NEUTRAL).setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    dialogInterface.dismiss();
                                }
                            });

                        }
                    });
                    ad.show();
                }
                return false;
            }
        });

        findPreference("logout").setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("Выход из учетной записи")
                        .setMessage("Вы уверены, что хотите сменить пользователя?")
                        .setCancelable(true)
                        .setIcon(R.drawable.png_change_user_gray_orinal)
                        .setPositiveButton("Да",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        NetworkService.requestController.sendRequest(RequestConstructor.CLogout(), "SLogout", new SocketInterface() {
                                            @Override
                                            public void onSuccess(String request) {
                                                NotificationService.hideNotifications();
                                                NetworkService.requestController.setState(RequestSender.WITH_OUT_LOGIN);
                                                SettingsController.setTagAccountUserId(0);
                                                SettingsController.clearLoginPasswordId();
                                                Heap.accountHotel.requestHostController.clearRequests();
                                                //if (!SettingsController.isHotel())
                                                    //Heap.accountUser.getRequestTouristController();
                                                Intent intent = new Intent(getActivity(), NavigationActivity.class);
                                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                                intent.setAction(LOGOUT_FROM_SETTINGS);
                                                startActivity(intent);
                                            }

                                            @Override
                                            public void onError(int error) {
                                                switch (error) {
                                                    case 101:
                                                        Toast.makeText(getActivity().getApplicationContext(), getResources().getString(R.string.errorIsEthernetNotAvailable), Toast.LENGTH_SHORT).show();
                                                        break;
                                                }
                                            }
                                        });
                                    }
                                })
                        .setNegativeButton("Нет", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.cancel();
                            }
                        });
                builder.create().show();

                return false;
            }
        });
    }

    @Override public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {

    }
}