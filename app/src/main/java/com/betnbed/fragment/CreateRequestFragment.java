package com.betnbed.fragment;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.graphics.drawable.VectorDrawableCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.betnbed.Heap;
import com.betnbed.R;
import com.betnbed.RequestConstructor;
import com.betnbed.SC;
import com.betnbed.interfaces.MovingToTab;
import com.betnbed.interfaces.SocketInterface;
import com.betnbed.model.BaseModel;
import com.betnbed.model.Hotel;
import com.betnbed.model.Request;
import com.betnbed.service.NetworkService;
import com.bumptech.glide.Glide;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.Calendar;
import java.util.EmptyStackException;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.betnbed.activity.login.LoginCreateAccountActivity.HOTEL_TYPE;
import static com.betnbed.model.Hotel.HOSTEL_TYPE;
import static com.betnbed.model.Hotel.HOST_TYPE;

public class CreateRequestFragment extends Fragment implements View.OnClickListener, PlaceSelectionListener {

    private String destinationId = "";
    private int currentCurrency = -1;
    private int currentMoney = -1;
    private String departure = "";
    private String arrival = "";

    private MovingToTab movingToTab;

    @BindView(R.id.clear_view) View clear;
    @BindView(R.id.additionalSettings) EditText additionalSettings;
    @BindView(R.id.googlePlaceId) TextView cityAndCountry;
    @BindView(R.id.howManyPeople) EditText howManyPeople;
    @BindView(R.id.inDate) TextView inDate;
    @BindView(R.id.inTime) TextView inTime;
    @BindView(R.id.outDate) TextView outDate;
    @BindView(R.id.outTime) TextView outTime;
    @BindView(R.id.howManyMoney) TextView howManyMoney;
    @BindView(R.id.progressBar) ProgressBar progressBar;
    @BindView(R.id.progress_layout) View progressLayout;
    @BindView(R.id.progress) ProgressBar progress;

    @BindViews({R.id.homeHouseLayout, R.id.internetLayout, R.id.breakfastLayout, R.id.parkingLayout, R.id.mediaLayout, R.id.bathLayout, R.id.kitchenLayout, R.id.animalLayout, R.id.serviceLayout, R.id.coinLayout})
    List<LinearLayout> layoutMass;

    @OnClick(R.id.createRequest) void createRequest() {
        sendRequest();
    }

    @OnClick(R.id.clearRequest) void clearRequest() {
        clearUI();
    }

    @OnClick(R.id.clear_view) void clearCityAndCountry() {
        cityAndCountry.setText("");
        destinationId = "";
        clear.setVisibility(View.GONE);
    }

    static public CreateRequestFragment newInstance() {

        CreateRequestFragment fragment = new CreateRequestFragment();

        return fragment;
    }

    @Override public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_create_request, container, false);

        ButterKnife.bind(this, view);


        Drawable closeImage = VectorDrawableCompat.create(getResources(), R.drawable.ic_close_black_24dp, getActivity().getTheme());
        clear.setBackground(closeImage);

        progressBar.getIndeterminateDrawable().setColorFilter(Color.parseColor("#00a0f0"), PorterDuff.Mode.MULTIPLY);

        for (View layout : layoutMass)
            layout.setOnClickListener(this);

        inDate.setOnClickListener(this);
        inTime.setOnClickListener(this);
        outDate.setOnClickListener(this);
        outTime.setOnClickListener(this);
        howManyMoney.setOnClickListener(this);
        howManyPeople.setOnClickListener(this);


        cityAndCountry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progress.setVisibility(View.VISIBLE);
                try {
                    AutocompleteFilter typeFilter = new AutocompleteFilter.Builder()
                            .setTypeFilter(AutocompleteFilter.TYPE_FILTER_CITIES)
                            .setTypeFilter(AutocompleteFilter.TYPE_FILTER_REGIONS)
                            .build();
                    startActivityForResult(new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY).setFilter(typeFilter).build(getActivity()), 1);
                } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
                    Log.v("TAG", "Error" + e.toString());
                }
            }
        });

        return view;
    }

    public void onClick(final View v) {

        if (v.getId() == R.id.homeHouseLayout) {

            final View layout = getActivity().getLayoutInflater().inflate(R.layout.house_hotel_hostel_popup, null);
            final int[] currentState = {imageMass[0][1]};

            final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                    .setView(layout)
                    .setTitle("Варианты размещения");

            final AlertDialog dialog = builder.create();
            dialog.show();

            final TextView first_text = (TextView) layout.findViewById(R.id.first_text);
            final TextView second_text = (TextView) layout.findViewById(R.id.second_text);
            final TextView third_text = (TextView) layout.findViewById(R.id.third_text);
            final ImageView first_icon = (ImageView) layout.findViewById(R.id.first_icon);
            final ImageView second_icon = (ImageView) layout.findViewById(R.id.second_icon);
            final ImageView third_icon = (ImageView) layout.findViewById(R.id.third_icon);


            //final LinearLayout first = (LinearLayout) layout.findViewById(R.id.first);
            //final LinearLayout second = (LinearLayout) layout.findViewById(R.id.second);
            //final LinearLayout third = (LinearLayout) layout.findViewById(R.id.third);

            final int defaultColor = first_text.getCurrentTextColor();


            if((currentState[0] & HOTEL_TYPE) == HOTEL_TYPE)
                first_text.setTextColor(Color.parseColor("#00a0f0"));
            if((currentState[0] & HOST_TYPE) == HOST_TYPE)
                second_text.setTextColor(Color.parseColor("#00a0f0"));
            if((currentState[0] & HOSTEL_TYPE) == HOSTEL_TYPE)
                third_text.setTextColor(Color.parseColor("#00a0f0"));

            Glide.with(getActivity()).load((currentState[0] & HOTEL_TYPE) == HOTEL_TYPE ? R.drawable.png_hotel_blue : R.drawable.png_hotel_gray).into(first_icon);
            Glide.with(getActivity()).load((currentState[0] & HOST_TYPE) == HOST_TYPE ? R.drawable.png_home_blue : R.drawable.png_home_gray).into(second_icon);
            Glide.with(getActivity()).load((currentState[0] & HOSTEL_TYPE) == HOSTEL_TYPE ? R.drawable.png_hostel_blue : R.drawable.png_hostel_gray).into(third_icon);

            layout.findViewById(R.id.button).setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View ignore) {
                    imageMass[0][1] = currentState[0];

                    ImageView imageView = (ImageView) ((LinearLayout) v).getChildAt(1);
                    TextView textView = (TextView) ((LinearLayout) v).getChildAt(0);

                    if (imageMass[0][1] == (HOSTEL_TYPE | HOST_TYPE | Hotel.HOTEL_TYPE)) {
                        Glide.with(getActivity()).load(R.drawable.png_three).into(imageView);
                        textView.setText("Любой вариант");
                    } else if (imageMass[0][1] == HOSTEL_TYPE) {
                        Glide.with(getActivity()).load(R.drawable.png_hostel_blue).into(imageView);
                        textView.setText("В хостел");
                    } else if (imageMass[0][1] == HOST_TYPE) {
                        Glide.with(getActivity()).load(R.drawable.png_home_blue).into(imageView);
                        textView.setText("В дом");
                    } else if (imageMass[0][1] == Hotel.HOTEL_TYPE) {
                        Glide.with(getActivity()).load(R.drawable.png_hotel_blue).into(imageView);
                        textView.setText("В номер");
                    } else {
                        Glide.with(getActivity()).load(R.drawable.png_two).into(imageView);
                        if (imageMass[0][1] == (HOTEL_TYPE | HOST_TYPE))
                            textView.setText("В номер или дом");
                        if (imageMass[0][1] == (HOTEL_TYPE | HOSTEL_TYPE))
                            textView.setText("В номер или хостел");
                        if (imageMass[0][1] == (HOSTEL_TYPE | HOST_TYPE))
                            textView.setText("В хостел или дом");
                    }
                    dialog.dismiss();

                }
            });

            View.OnClickListener chouseHotel = new View.OnClickListener() {
                @Override public void onClick(View v) {
                    currentState[0] ^= HOTEL_TYPE;
                    first_text.setTextColor((currentState[0] & HOTEL_TYPE) == HOTEL_TYPE ? Color.parseColor("#00a0f0") : defaultColor);
                    Glide.with(getActivity()).load((currentState[0] & HOTEL_TYPE) == HOTEL_TYPE ? R.drawable.png_hotel_blue : R.drawable.png_hotel_gray).into(first_icon);
                    layout.findViewById(R.id.button).setEnabled(currentState[0] != 0);
                }
            };
            View.OnClickListener chouseHost = new View.OnClickListener() {
                @Override public void onClick(View v) {
                    currentState[0] ^= HOST_TYPE;
                    second_text.setTextColor((currentState[0] & HOST_TYPE) == HOST_TYPE ? Color.parseColor("#00a0f0") : defaultColor);
                    Glide.with(getActivity()).load((currentState[0] & HOST_TYPE) == HOST_TYPE ? R.drawable.png_home_blue : R.drawable.png_home_gray).into(second_icon);
                    layout.findViewById(R.id.button).setEnabled(currentState[0] != 0);
                }
            };
            View.OnClickListener chouseHostel = new View.OnClickListener() {
                @Override public void onClick(View v) {
                    currentState[0] ^= HOSTEL_TYPE;

                    third_text.setTextColor((currentState[0] & HOSTEL_TYPE) == HOSTEL_TYPE ? Color.parseColor("#00a0f0") : defaultColor);
                    Glide.with(getActivity()).load((currentState[0] & HOSTEL_TYPE) == HOSTEL_TYPE ? R.drawable.png_hostel_blue : R.drawable.png_hostel_gray).into(third_icon);
                    layout.findViewById(R.id.button).setEnabled(currentState[0] != 0);

                }
            };

            layout.findViewById(R.id.first_text).setOnClickListener(chouseHotel);
            layout.findViewById(R.id.second_text).setOnClickListener(chouseHost);
            layout.findViewById(R.id.third_text).setOnClickListener(chouseHostel);

            layout.findViewById(R.id.first_icon).setOnClickListener(chouseHotel);
            layout.findViewById(R.id.second_icon).setOnClickListener(chouseHost);
            layout.findViewById(R.id.third_icon).setOnClickListener(chouseHostel);


        }

        for (int i = 1; i < imageMass.length; i++)
            if (imageMass[i][0] == v.getId()) {
                if (imageMass[i][1] != imageMass[i].length - 1)
                    imageMass[i][1]++;
                else imageMass[i][1] = 2;

                if (v.getId() != R.id.coinLayout && v.getId() != R.id.homeHouseLayout)
                    ((TextView) ((LinearLayout) v).getChildAt(0)).setTextColor(massColor[imageMass[i][1] - 2]);
                ((TextView) ((LinearLayout) v).getChildAt(0)).setText(titleMass[i][imageMass[i][1] - 2]);
                ((ImageView) ((LinearLayout) v).getChildAt(1)).setImageResource(imageMass[i][imageMass[i][1]]);

            }
        if (v.getId() == R.id.inDate)
            getDialog(1).show();
        if (v.getId() == R.id.inTime)
            getDialog(3).show();
        if (v.getId() == R.id.outDate)
            getDialog(2).show();
        if (v.getId() == R.id.outTime)
            getDialog(4).show();
        if (v.getId() == R.id.howManyMoney) {

            final View view = getActivity().getLayoutInflater().inflate(R.layout.dialog_money, null);

            final Spinner spinner = (Spinner) view.findViewById(R.id.currency);
            spinner.setAdapter(new ArrayAdapter<>(getContext(), R.layout.spinner_gender_textview, getResources().getStringArray(R.array.currency)));
            final EditText price = (EditText) view.findViewById(R.id.price);
            if (currentCurrency != -1 && currentMoney != -1) {
                spinner.setSelection(currentCurrency, false);
                price.setText(String.valueOf(currentMoney));
            } else {
                spinner.setSelection(0, false);
                price.setText("");
            }
            final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                    .setView(view)
                    .setTitle("Укажите стоимость*")
                    .setIcon(R.drawable.png_credit_card_blue)
                    .setPositiveButton("ОК", null)
                    .setNeutralButton("Отмена", null);

            final AlertDialog dialog = builder.create();
            dialog.setOnShowListener(new DialogInterface.OnShowListener() {
                @Override
                public void onShow(DialogInterface dialog_) {
                    Button ok = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
                    ok.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (price.getText().length() == 0) {
                                price.setHintTextColor(Color.RED);

                            } else {
                                currentCurrency = spinner.getSelectedItemPosition();
                                currentMoney = Integer.parseInt(price.getText().toString());
                                howManyMoney.setText(price.getText() + " " + getResources().getStringArray(R.array.currency)[currentCurrency]);
                                price.setHintTextColor(Color.parseColor("#ff00aff0"));
                                dialog.dismiss();
                            }
                        }
                    });

                    dialog.getButton(AlertDialog.BUTTON_NEUTRAL).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });
                }
            });

            dialog.show();
        }

    }

    private Dialog getDialog(int id) {
        switch (id) {
            case 1:
                return new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        Calendar calendar = new GregorianCalendar(year, monthOfYear, dayOfMonth);
                        inDate.setText(SC.dfShort.format(calendar.getTime()));
                        arrival = inDate.getText() + " " + inTime.getText();
                    }
                }, Calendar.getInstance().get(Calendar.YEAR), Calendar.getInstance().get(Calendar.MONTH), Calendar.getInstance().get(Calendar.DAY_OF_MONTH));
            case 2:
                return new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        Calendar calendar = new GregorianCalendar(year, monthOfYear, dayOfMonth);
                        outDate.setText(SC.dfShort.format(calendar.getTime()));
                        departure = outDate.getText() + " " + outTime.getText();
                    }
                }, Calendar.getInstance().get(Calendar.YEAR), Calendar.getInstance().get(Calendar.MONTH), Calendar.getInstance().get(Calendar.DAY_OF_MONTH));
            case 3:

                return new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        inTime.setText(hourOfDay + ":" + (minute < 10 ? '0' + String.valueOf(minute) : minute));
                        arrival = inDate.getText() + " " + inTime.getText();
                    }
                }, Calendar.getInstance().get(Calendar.HOUR_OF_DAY), Calendar.getInstance().get(Calendar.MINUTE), true);
            case 4:
                return new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        outTime.setText(hourOfDay + ":" + (minute < 10 ? '0' + String.valueOf(minute) : minute));
                        departure = outDate.getText() + " " + outTime.getText();
                    }
                }, Calendar.getInstance().get(Calendar.HOUR_OF_DAY), Calendar.getInstance().get(Calendar.MINUTE), true);
            default:
                throw new EmptyStackException();
        }
    }

    @Override
    public void onPlaceSelected(Place place) {
        clear.setVisibility(View.VISIBLE);
        progress.setVisibility(View.GONE);
        cityAndCountry.setText(place.getAddress());
    }

    @Override
    public void onError(Status status) {
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        progress.setVisibility(View.GONE);
        if (requestCode == 1) {

            if (resultCode == Activity.RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(getContext(), data);
                destinationId = place.getId();

                this.onPlaceSelected(place);
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {

                Status status = PlaceAutocomplete.getStatus(getContext(), data);
                this.onError(status);
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public class sendCNewRequest extends AsyncTask<Request, Void, Void> {

        int result;

        public sendCNewRequest() {
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressLayout.setVisibility(View.VISIBLE);
        }

        @Override
        protected Void doInBackground(final Request... params) {

            NetworkService.requestController.sendRequest(RequestConstructor.CNewRequest(params[0]), "SRequestCreated", new SocketInterface() {
                @Override
                public void onSuccess(String request) {
                    try {
                        int requestId = new JSONObject(request).getJSONArray("data").getInt(0);
                        params[0].setRequestId(requestId);
                        params[0].setFullPlaceName(cityAndCountry.getText().toString());
                        Heap.accountUser.getRequestTouristController().addRequest(params[0]);
                        result = 0;
                    } catch (JSONException e) {
                        Log.v("TAG", e.toString());
                    }
                }

                @Override
                public void onError(int error) {
                    result = error;
                }
            });
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (result == 0) {
                Toast.makeText(getActivity(), "Запрос был успешно создан", Toast.LENGTH_SHORT).show();
                progressLayout.setVisibility(View.GONE);
                movingToTab.moveTo(0);
                //LocalBroadcastManager.getInstance(getContext()).sendBroadcast(new Intent(TouristRequestListActivity.tagNewRequest));
            } else {
                progressLayout.setVisibility(View.GONE);

                switch (result) {
                    case 20:
                        Toast.makeText(getActivity(), "Вы превысили лимит запросов", Toast.LENGTH_SHORT).show();
                        break;
                    case 101:
                        Toast.makeText(getActivity(), "Возникла ошибка с интернетом", Toast.LENGTH_LONG).show();
                        break;
                }
            }
        }

    }

    private final int[][] imageMass = {
            {-1, HOTEL_TYPE, -1, -1, -1},
            {R.id.internetLayout, 2, R.drawable.png_wifi_gray, R.drawable.png_wifi_blue},
            {R.id.breakfastLayout, 2, R.drawable.png_breakfast_gray, R.drawable.png_breakfast_blue},
            {R.id.parkingLayout, 2, R.drawable.png_parking_gray, R.drawable.png_parking_blue},
            {R.id.mediaLayout, 2, R.drawable.png_tv_gray, R.drawable.png_tv_blue},
            {R.id.bathLayout, 2, R.drawable.png_wash_grey, R.drawable.png_wash_blue},
            {R.id.kitchenLayout, 2, R.drawable.png_kitchen_gray, R.drawable.png_kitchen_blue},
            {R.id.animalLayout, 2, R.drawable.png_animal_gray, R.drawable.png_animal_blue},
            {R.id.serviceLayout, 2, R.drawable.png_service_gray, R.drawable.png_service_blue},
            {R.id.coinLayout, 4, R.drawable.png_both_blue, R.drawable.png_credit_card_blue, R.drawable.png_cash_blue}
    };

    private final int[] massColor = {Color.GRAY, Color.parseColor("#ff009ff0")};

    private final String[][] titleMass = {
            {null, null, null},
            {"Интернет", "Интернет"},
            {"Завтрак", "Завтрак"},
            {"Парковка", "Парковка"},
            {"Медиа", "Медиа"},
            {"Ванная", "Ванная"},
            {"Кухня", "Кухня"},
            {"Животные", "Животные"},
            {"Сервисы", "Сервисы"},
            {"На выбор отеля", "Оплачу картой", "Оплачу наличными"}
    };

    private void clearUI() {

        imageMass[0][1] = 2;
        LinearLayout homeHouseLayout = layoutMass.get(0);
        ((ImageView) homeHouseLayout.getChildAt(1)).setImageResource(R.drawable.png_hotel_blue);
        ((TextView) homeHouseLayout.getChildAt(0)).setText("В номер");



        imageMass[imageMass.length - 1][1] = 4;
        for (int i = 1; i < imageMass.length - 2; i++)
            imageMass[i][1] = 2;


        for (int i = 1; i < imageMass.length; i++) {
            LinearLayout imageLayout = layoutMass.get(i);
            if (imageLayout.getId() != R.id.coinLayout && imageLayout.getId() != R.id.homeHouseLayout)
                ((TextView) imageLayout.getChildAt(0)).setTextColor(massColor[imageMass[i][1] - 2]);
            ((ImageView) imageLayout.getChildAt(1)).setImageResource(imageMass[i][imageMass[i][1]]);
            ((TextView) imageLayout.getChildAt(0)).setText(titleMass[i][imageMass[i][1] - 2]);
        }

        currentMoney = -1;
        currentCurrency = -1;
        clear.setVisibility(View.GONE);
        destinationId = "";
        inDate.setText("");
        inTime.setText("");
        outDate.setText("");
        outTime.setText("");
        howManyMoney.setText("");
        howManyPeople.setText("");
        cityAndCountry.setText("");
        additionalSettings.setText("");
        cityAndCountry.setHintTextColor(Color.parseColor("#00aff0"));
        inDate.setHintTextColor(Color.parseColor("#00aff0"));
        outDate.setHintTextColor(Color.parseColor("#00aff0"));
        howManyPeople.setHintTextColor(Color.parseColor("#00aff0"));
        howManyMoney.setHintTextColor(Color.WHITE);
        inTime.setHintTextColor(Color.parseColor("#00aff0"));
        outTime.setHintTextColor(Color.parseColor("#00aff0"));
    }

    private void sendRequest() {

        boolean error = false;
        if (destinationId.length() == 0) {
            cityAndCountry.setHintTextColor(Color.RED);
            error = true;
        }
        if (inDate.length() == 0) {
            inDate.setHintTextColor(Color.RED);
            error = true;
        }
        if (outDate.length() == 0) {
            outDate.setHintTextColor(Color.RED);
            error = true;
        }
        if (howManyPeople.length() == 0) {
            howManyPeople.setHintTextColor(Color.RED);
            error = true;
        }
        if (howManyMoney.length() == 0) {
            howManyMoney.setHintTextColor(Color.RED);
            error = true;
        }
        if (inTime.length() == 0) {
            inTime.setHintTextColor(Color.RED);
            error = true;
        }
        if (outTime.length() == 0) {
            outTime.setHintTextColor(Color.RED);
            error = true;
        }
        long arrivalTime = 0;
        long departureTime = 0;
        try {
            arrivalTime = (SC.df.parse(arrival).getTime() / 1000);
            departureTime = (SC.df.parse(departure).getTime() / 1000);
            if (arrivalTime * 1000 < System.currentTimeMillis() + TimeZone.getDefault().getRawOffset()) {
                Toast.makeText(getContext(), "Дата приезда должна быть позже текущего времени", Toast.LENGTH_SHORT).show();
                error = true;
            }

            if (departureTime <= arrivalTime && !error) {
                Toast.makeText(getContext(), "Дата отъезда должна быть позже даты приезда", Toast.LENGTH_SHORT).show();
                error = true;
            }
        } catch (ParseException e) {
            Log.v("TAG", e.toString());
            error = true;
        }

        int howManyPeopleInt = 0;
        if (howManyPeople.getEditableText().toString().length() > 0) {
            howManyPeopleInt = Integer.parseInt(howManyPeople.getEditableText().toString());
            if (howManyPeopleInt == 0) {
                Toast.makeText(getContext(), "Количество человек не может быть нулевым", Toast.LENGTH_SHORT).show();
                error = true;
            }
        } else error = true;

        if (error) return;

        BaseModel.BaseBuilder baseBuilder = new BaseModel.BaseBuilder()
                .hotelType(imageMass[0][1])
                .internet(imageMass[1][1] - 2)
                .breakfast(imageMass[2][1] - 2)
                .parking(imageMass[3][1] - 2)
                .media(imageMass[4][1] - 2)
                .shower(imageMass[5][1] - 2)
                .kitchen(imageMass[6][1] - 2)
                .pets(imageMass[7][1] - 2)
                .services(imageMass[8][1] - 2)
                .moneyType(imageMass[9][1] - 2);

        Request request = new Request.Builder()
                .catsAndDogs(baseBuilder)
                .place(destinationId)
                .arrival(arrivalTime)
                .departure(departureTime)
                .money(currentMoney)
                .currency(getResources().getStringArray(R.array.currency)[currentCurrency])
                .people(howManyPeopleInt)
                .special(additionalSettings.getEditableText().toString())
                .isActive(false)
                .build();

        new sendCNewRequest().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, request);

    }

    public MovingToTab getMovingToTab() {
        return movingToTab;
    }

    public void setMovingToTab(MovingToTab movingToTab) {
        this.movingToTab = movingToTab;
    }
}
