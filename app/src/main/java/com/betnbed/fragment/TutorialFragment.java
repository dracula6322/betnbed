package com.betnbed.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.betnbed.R;
import com.bumptech.glide.Glide;

public class TutorialFragment extends Fragment {

    private int type;
    private int textType;

    public TutorialFragment(){

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_tutorial, container, false);

        ImageView imageView = (ImageView)v.findViewById(R.id.image);
        switch (type) {
            case 1 : Glide.with(container.getContext()).load(R.drawable.help_tourist_1).into(imageView);break;
            case 2 : Glide.with(container.getContext()).load(R.drawable.help_tourist_2).into(imageView);break;
            case 3 : Glide.with(container.getContext()).load(R.drawable.help_tourist_3).into(imageView);break;
            case 4 : Glide.with(container.getContext()).load(R.drawable.help_tourist_4).into(imageView);break;
            case 5 : Glide.with(container.getContext()).load(R.drawable.help_tourist_5).into(imageView);break;
            case 11 : Glide.with(container.getContext()).load(R.drawable.help_hotel_1).into(imageView);break;
            case 12 : Glide.with(container.getContext()).load(R.drawable.help_hotel_2).into(imageView);break;
            case 13 : Glide.with(container.getContext()).load(R.drawable.help_hotel_3).into(imageView);break;
            case 14 : Glide.with(container.getContext()).load(R.drawable.help_hotel_4).into(imageView);break;
            case 15 : Glide.with(container.getContext()).load(R.drawable.help_hotel_5).into(imageView);break;

        }

        ((TextView)v.findViewById(R.id.text)).setText(getResources().getStringArray(R.array.tutorialTouristString)[textType]);

        return v;
    }

    public void setType(int type, int textType) {
        this.type = type;
        this.textType = textType;
    }
}
