package com.betnbed.service;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.BitmapFactory;
import android.preference.PreferenceManager;
import android.util.Log;

import com.betnbed.R;
import com.betnbed.activity.NavigationActivity;
import com.betnbed.controller.MessageController;
import com.betnbed.model.Message;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;

public class NotificationService extends FirebaseMessagingService {

    static private NotificationManager notificationManager;
    static private SharedPreferences sharedPref;

    public static final int NEW_INVITE = 1;
    public static final int OUR_INVITE_ACCEPT = 2;
    public static final int NEW_MESSAGE = 3;

    private static int countUnreadMessage = 0;
    private static int countUnreadInvites = 0;
    private static int countUnreadOurInviteAccepted = 0;

    public NotificationService() {
        Log.v("TAG", "NotificationService constructor");
    }

    static public void init(final Context context) {
        Log.v("TAG", "NotificationService init");
        sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        String type = remoteMessage.getData().get("type");

        if (type != null) {
            Log.v("TAG", type);
            for(String string : remoteMessage.getData().values())
                Log.v("TAG", string);

            Map<String, String> map = remoteMessage.getData();
            if (type.equals("PMsg") || type.equals("PPrivateMsg")) {

                String time;
                String id;
                String text;
                if((time = map.get("time")) == null || (id = map.get("id")) == null || (text = map.get("text")) == null)
                    return;


                Message message = new Message(Integer.parseInt(time), Integer.parseInt(id), text, Message.STATE_UNREADED_MESSAGE);
                MessageController.addMessage(message);

                showNewMessage();

            }

            if (type.equals("PNewInvite")) {
                incCountUnreadInvites();
                showNewInvite();
            }

            if (type.equals("PInviteStatus")) {

                String status;
                if ((status = map.get("status")) == null)
                    return;

                if (status.equals("2")) {
                    incCountUnreadOurInviteAccepted();
                    showOutInviteAccept();
                }
            }

            if (type.equals("PApproveStatus")) {

                String status;
                String time;
                String id;
                String reason;

                if((time = map.get("time")) == null || (id = map.get("id")) == null || (reason = map.get("reason")) == null || (status = map.get("status")) == null)
                    return;

                String text;
                switch (Integer.valueOf(status)) {
                    case 0:
                        text = "Для успешного прохождения проверки и получения доступа ко всем возможностям Betnbed, пожалуйста, уточните следующую информацию в Вашем профиле:\n" + reason;
                        break;
                    case 1:
                        text = "Дайте нам, пожалуйста, несколько минут для того, чтобы мы могли проверить Ваш профиль. Персональная проверка профилей наших партнеров позволяет делать услуги Betnbed еще более качественными.";
                        break;
                    default:
                        text = "Мы рады Вам сообщить, что модерация Вашего профиля прошла успешно. Теперь Вам доступны все возможности Betnbed.\nПожалуйста, перейдите в меню, нажмите кнопку «Пригласить гостей» и получайте Ваш дополнительный доход!";
                        break;
                }
                Message message = new Message(Integer.parseInt(time), Integer.parseInt(id), text, Message.STATE_UNREADED_PUSH);
                MessageController.addMessage(message);
                showApproveStatus(Integer.valueOf(status));
            }
        }
    }


    private void showNewMessage() {
        if (sharedPref.getBoolean("isShowNotification", true)) {

            Intent intent = new Intent(this, NavigationActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra("openPage", NEW_MESSAGE);


            PendingIntent contentIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);

            Resources res = getApplicationContext().getResources();
            Notification.Builder builder = new Notification.Builder(this);

            builder.setContentIntent(contentIntent)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setLargeIcon(BitmapFactory.decodeResource(res, R.mipmap.ic_launcher))
                    .setTicker("Вам пришло сообщение")
                    .setWhen(System.currentTimeMillis())
                    .setAutoCancel(true)
                    .setContentTitle("Betnbed")
                    .setContentText("Вам пришло новое сообщение");

            setNotificationSignal(builder);

            Notification notification = builder.build();
            notificationManager.notify(NEW_MESSAGE, notification);
        }
    }

    private void showApproveStatus(int type) {
        if (sharedPref.getBoolean("isShowNotification", true)) {

            Intent intent = new Intent(this, NavigationActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra("openPage", NEW_MESSAGE);

            PendingIntent contentIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);

            Resources res = getApplicationContext().getResources();
            Notification.Builder builder = new Notification.Builder(this);

            builder.setContentIntent(contentIntent)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setLargeIcon(BitmapFactory.decodeResource(res, R.mipmap.ic_launcher))
                    .setWhen(System.currentTimeMillis())
                    .setAutoCancel(true)
                    .setContentTitle("Betnbed");

            switch (type) {
                case 2:
                    builder.setTicker("Всё здорово! Проверка прошла успешно");
                    builder.setContentText("Всё здорово! Проверка прошла успешно");
                    break;
                case 0:
                    builder.setTicker("Пожалуйста, уточните информацию в профиле");
                    builder.setContentText("Пожалуйста, уточните информацию в профиле");
                    break;
                default:
                    builder.setTicker("Мы проверяем ваш профиль");
                    builder.setContentText("Мы проверяем ваш профиль");
            }

            setNotificationSignal(builder);

            Notification notification = builder.build();
            notificationManager.notify(NEW_MESSAGE, notification);


        }
    }

    private void showOutInviteAccept() {
        if (sharedPref.getBoolean("isShowNotification", true)) {

            Intent intent = new Intent(this, NavigationActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra("openPage", OUR_INVITE_ACCEPT);


            PendingIntent contentIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);

            Resources res = getApplicationContext().getResources();
            Notification.Builder builder = new Notification.Builder(this);

            builder.setContentIntent(contentIntent)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setLargeIcon(BitmapFactory.decodeResource(res, R.mipmap.ic_launcher))
                    .setTicker("Ваше приглашение принято")
                    .setWhen(System.currentTimeMillis())
                    .setAutoCancel(true)
                    .setContentTitle("Betnbed")
                    .setContentText("Ваше приглашение принятно"); // Текст уведомления

            setNotificationSignal(builder);

            Notification notification = builder.build();
            notificationManager.notify(OUR_INVITE_ACCEPT, notification);
        }
    }

    private void showNewInvite() {
        if (sharedPref.getBoolean("isShowNotification", true)) {

            Intent intent = new Intent(this, NavigationActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra("openPage", NEW_INVITE);

            Notification.Builder builder = new Notification.Builder(this);
            builder.setContentIntent(PendingIntent.getActivity(this, 0,intent, PendingIntent.FLAG_CANCEL_CURRENT))
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher))
                    .setTicker("Вам пришло новое приглашение")
                    .setWhen(System.currentTimeMillis())
                    .setAutoCancel(true)
                    .setContentTitle("Betnbed")
                    .setContentText(countUnreadInvites == 1 ? "Вам пришло новое приглашение" : ("Вам пришло " + String.valueOf(countUnreadInvites) + " новых приглшений"));

            setNotificationSignal(builder);

            Notification notification = builder.build();
            notificationManager.notify(NEW_INVITE, notification);
        }
    }

    static private void setNotificationSignal(Notification.Builder builder) {
        switch (sharedPref.getString("typeNotification", "")) {
            case "1":
                builder.setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE);
                break;
            case "2":
                builder.setDefaults(Notification.DEFAULT_SOUND);
                break;
            case "3":
                builder.setDefaults(Notification.DEFAULT_VIBRATE);
                break;
        }
    }

    static public void hideNotifications() {
        notificationManager.cancelAll();
        setCountUnreadInvites(0);
        setCountUnreadMessage(0);
        setCountUnreadOurInviteAccepted(0);
    }

    static public void hideNewMessageNotiications() {
        notificationManager.cancel(NEW_MESSAGE);
    }

    static public void hideNewInviteNotiications() {
        notificationManager.cancel(NEW_INVITE);
    }

    static public void hideOurInviteAcceptedNotiications() {
        notificationManager.cancel(OUR_INVITE_ACCEPT);
    }

    /////////////   Getter and Setter Area  //////////////////////////////////////////////////

    public static int getCountUnreadOurInviteAccepted() {
        return countUnreadOurInviteAccepted;
    }

    public static void setCountUnreadOurInviteAccepted(int countUnreadOurInviteAccepted) {
        NotificationService.countUnreadOurInviteAccepted = countUnreadOurInviteAccepted;
    }

    private static void incCountUnreadOurInviteAccepted() {
        NotificationService.countUnreadOurInviteAccepted++;
    }

    public static int getCountUnreadInvites() {
        return countUnreadInvites;
    }

    public static void setCountUnreadInvites(int countUnreadInvites) {
        NotificationService.countUnreadInvites = countUnreadInvites;
    }

    public static void incCountUnreadInvites() {
        NotificationService.countUnreadInvites++;
    }

    public static int getCountUnreadMessage() {
        return countUnreadMessage;
    }

    public static void setCountUnreadMessage(int countUnreadMessage) {
        NotificationService.countUnreadMessage = countUnreadMessage;
    }

    public static void incCountUnreadMessage() {NotificationService.countUnreadMessage++;}
}
