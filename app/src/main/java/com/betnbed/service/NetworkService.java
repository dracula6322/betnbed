package com.betnbed.service;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.util.Log;

import com.betnbed.RequestSender;

public class NetworkService extends IntentService {

    public static RequestSender requestController;


    public NetworkService() {
        super("");
        Log.v("TAG", "NetworkSetvice constructor");
    }

    public NetworkService(String name) {
        super(name);
        Log.v("TAG", "NetworkSetvice constructor");
    }

    @Override public int onStartCommand(@Nullable Intent intent, int flags, int startId) {
        Log.v("TAG", "NetworkSetvice onStartCommand");
        requestController = new RequestSender(this);
        return START_STICKY;
    }

    @Override protected void onHandleIntent(@Nullable Intent intent) {
        Log.v("TAG", "NetworkSetvice onHandleIntent");
    }

    @Override public void onDestroy() {
        super.onDestroy();
        Log.v("TAG", "NetworkSetvice onDestroy");
        requestController.cancel(true);
    }
}
