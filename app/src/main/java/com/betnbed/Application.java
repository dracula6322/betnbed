package com.betnbed;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import com.betnbed.controller.MessageController;
import com.betnbed.controller.SettingsController;
import com.betnbed.service.NotificationService;
import com.google.android.gms.common.api.GoogleApiClient;
import com.vk.sdk.VKSdk;

import java.util.TimeZone;

public class Application extends android.app.Application {

    static public GoogleApiClient mGoogleApiClient;

    public void onCreate() {
        super.onCreate();
        VKSdk.initialize(this);
        SettingsController.initAccountSetting(getSharedPreferences(SettingsController.tagSettings, Context.MODE_PRIVATE));
        NotificationService.init(this);
        MessageController.init(this);

        SC.df.setTimeZone(TimeZone.getTimeZone("UTC"));

        registerActivityLifecycleCallbacks(new ActivityLifecycleCallbacks() {
            @Override
            public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
                Log.v("LC", activity.getLocalClassName() + " onCreated");
            }

            @Override
            public void onActivityStarted(Activity activity) {
                Log.v("LC", activity.getLocalClassName() + " onStarted");
            }

            @Override
            public void onActivityResumed(Activity activity) {
                Log.v("LC", activity.getLocalClassName() + " onResumed");
            }

            @Override
            public void onActivityPaused(Activity activity) {
                Log.v("LC", activity.getLocalClassName() + " onPaused");
            }

            @Override
            public void onActivityStopped(Activity activity) {
                Log.v("LC", activity.getLocalClassName() + " onStopped");
            }

            @Override
            public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
            }

            @Override
            public void onActivityDestroyed(Activity activity) {
                Log.v("LC", activity.getLocalClassName() + " onDestroyed");
            }
        });

    }
}
