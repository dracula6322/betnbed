package com.betnbed;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.betnbed.service.NetworkService;

public class StartUpReceiver extends BroadcastReceiver {
    public StartUpReceiver() {
    }

    private static final int TYPE_WIFI = 1;
    private static final int TYPE_MOBILE = 2;
    private static final int TYPE_NOT_CONNECTED = 0;


    private static int getConnectivityStatus(Context context) {

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

        if (null != activeNetwork) {
            if(activeNetwork.getType() == ConnectivityManager.TYPE_WIFI)
                return TYPE_WIFI;

            if(activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE)
                return TYPE_MOBILE;
        }
        return TYPE_NOT_CONNECTED;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.v("TAG", "StartUpReceiver is started " + intent.getAction());
        Log.v("TAG", "requestController is null = " + (NetworkService.requestController == null));

        /*if(getConnectivityStatus(context) != TYPE_NOT_CONNECTED){
            context.startService(new Intent(context, NetworkService.class));
        } else {
            if(NavigationActivity.requestController!= null)
                NavigationActivity.requestController.cancel(true);
        }*/
    }
}
