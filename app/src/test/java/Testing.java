import android.content.Context;
import android.util.Log;

import com.betnbed.BuildConfig;
import com.betnbed.controller.MessageController;
import com.betnbed.controller.SettingsController;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowLog;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
public class Testing {


    private Context context;

    @Before
    public void setup() throws Exception {

        ShadowLog.stream = System.out;

        context = RuntimeEnvironment.application;

        SettingsController.initAccountSetting(context.getSharedPreferences(SettingsController.tagSettings, Context.MODE_PRIVATE));
        SettingsController.setTagAccountLogin("dracula6322@gmail.com");
        SettingsController.setTagAccountPassword("q\"");

    }

    @Test
    public void message_activity_test() throws Exception {
        Log.v("TAG", "We start message_activity_test");


        /*while (NetworkService.requestController.getState() != RequestSender.WITH_LOGIN_TOURIST) {
            Log.v("TAG", NetworkService.requestController.getState() + "");
        }*/

        MessageController.downloadMessages();
        Log.v("TAG", MessageController.getMessages().size() + "");


    }


}
